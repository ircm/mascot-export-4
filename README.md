# mascot-export

Web application that simplifies exportation of multiple Mascot files.


## Build and installation

1. Clone the project: `git clone https://bitbucket.org/ircm/mascot-export.git`.
2. Build the project: `mvn package`. You may add `-DskipTests` parameter to skip tests for faster build.
3. Run the jar or install as a service.


## Configuration

Before running the jar, create an application.yml file in the same folder as the jar file.

**You must set the location of the Mascot Server directory under the mascot section.**
```
mascot:
  home: ${user.home}/mascot
```

*You can set the port of the application, defaults to 8080.*
```
server:
  port: 8080
```

### Database
*You can change the database settings in the application.yml file.*
For MySQL, the configuration will look like this.
```
spring:
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost/test
    username: dbuser
    password: dbpass
```
See [http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-connect-to-production-database](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-connect-to-production-database)

### Debug Vaadin / GWT
If you modify the code and need to debug Vaadin or GWT, you can set the following value in application.yml file
```
vaadin:
  servlet:
    productionMode: false
```

### Other
Other configuration properties have reasonable default values that should work in most cases.


## Running

You can start the jar directly or by using the java command line.
```
java -jar mascot-export.jar
```

### As a service or cloud
See [http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#deployment](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#deployment)

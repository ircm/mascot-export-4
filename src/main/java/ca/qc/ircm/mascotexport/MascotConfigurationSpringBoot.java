/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = MascotConfigurationSpringBoot.PREFIX)
public class MascotConfigurationSpringBoot implements MascotConfiguration {
  public static final String PREFIX = "mascot";
  private static final String MINIMUM_PEPTIDE_LENGHT_SUMMARY_PARAMETER = "MinPepLenInPepSummary";
  private static final Logger logger = LoggerFactory.getLogger(MascotConfigurationSpringBoot.class);
  private Path home;
  private Path data;
  private Path configuration;

  @Override
  public Path getHome() {
    return home;
  }

  public void setHome(Path home) {
    this.home = home;
  }

  @Override
  public Path getData() {
    return data;
  }

  public void setData(Path data) {
    this.data = data;
  }

  @Override
  public Path getConfiguration() {
    return configuration;
  }

  public void setConfiguration(Path configuration) {
    this.configuration = configuration;
  }

  @Override
  public int getMinimumPeptideLenghtSummary() {
    Path mascotConfiguration = getConfiguration();
    if (Files.exists(mascotConfiguration)) {
      try (BufferedReader reader = Files.newBufferedReader(mascotConfiguration)) {
        String line;
        while ((line = reader.readLine()) != null) {
          if (line.startsWith(MINIMUM_PEPTIDE_LENGHT_SUMMARY_PARAMETER)) {
            String rawValue =
                line.substring(MINIMUM_PEPTIDE_LENGHT_SUMMARY_PARAMETER.length()).trim();
            return Integer.parseInt(rawValue);
          }
        }
        logger.warn("Missing {} in Mascot configuration file {}",
            MINIMUM_PEPTIDE_LENGHT_SUMMARY_PARAMETER, mascotConfiguration);
      } catch (IOException exception) {
        logger.warn("Could not read Mascot configuration file {}", mascotConfiguration);
      } catch (NumberFormatException exception) {
        logger.warn("{} is not a number in Mascot configuration file {}",
            MINIMUM_PEPTIDE_LENGHT_SUMMARY_PARAMETER, mascotConfiguration);
      }
    } else {
      logger.warn("Mascot configuration file {} not found", mascotConfiguration);
    }
    return 6;
  }
}

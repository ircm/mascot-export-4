/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = MascotExportConfigurationSpringBoot.PREFIX)
public class MascotExportConfigurationSpringBoot implements MascotExportConfiguration {
  public static final String PREFIX = "mascot.export";
  private Path executable;
  private List<String> args;
  private Path working;
  private Path output;

  /**
   * Validates configuration.
   */
  @PostConstruct
  public void init() {
    if (args == null) {
      args = new ArrayList<>();
    }
  }

  @Override
  public Path getExecutable() {
    return executable;
  }

  public void setExecutable(Path executable) {
    this.executable = executable;
  }

  @Override
  public List<String> getArgs() {
    return args;
  }

  public void setArgs(List<String> args) {
    this.args = args;
  }

  @Override
  public Path getWorking() {
    return working;
  }

  public void setWorking(Path working) {
    this.working = working;
  }

  @Override
  public Path getOutput() {
    return output;
  }

  public void setOutput(Path output) {
    this.output = output;
  }
}

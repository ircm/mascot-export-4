/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.concurrent.web;

import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Forces shutdown on all executors when application shutdown.
 */
public class ExecutorShutdownListener implements ServletContextListener {
  private static final Logger logger = LoggerFactory.getLogger(ExecutorShutdownListener.class);
  @Autowired
  private List<ExecutorService> executorServices;

  public ExecutorShutdownListener() {
  }

  public ExecutorShutdownListener(List<ExecutorService> executorServices) {
    this.executorServices = executorServices;
  }

  @Override
  public void contextInitialized(ServletContextEvent event) {
    WebApplicationContext context =
        WebApplicationContextUtils.getRequiredWebApplicationContext(event.getServletContext());
    context.getAutowireCapableBeanFactory().autowireBean(this);
  }

  @Override
  public void contextDestroyed(ServletContextEvent event) {
    logger.info("Shutdown {} executors", executorServices.size());
    for (ExecutorService executor : executorServices) {
      try {
        executor.shutdownNow();
      } catch (Throwable exception) {
        // Ignore.
        logger.error("Could not shutdown executor {}", executor, exception);
      }
    }
  }
}

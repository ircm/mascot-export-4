/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import java.io.IOException;

/**
 * Thrown when a file is already locked.
 */
public class FileLockException extends IOException {
  private static final long serialVersionUID = 5789118929383770448L;

  public FileLockException() {
    super();
  }

  public FileLockException(String message, Throwable cause) {
    super(message, cause);
  }

  public FileLockException(String message) {
    super(message);
  }

  public FileLockException(Throwable cause) {
    super(cause);
  }
}

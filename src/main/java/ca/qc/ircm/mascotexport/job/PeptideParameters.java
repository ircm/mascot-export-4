/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.processing.GeneratePropertyNames;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Export job parameters.
 */
@GeneratePropertyNames
public class PeptideParameters {
  private boolean master;
  private boolean expMr;
  private boolean expZ;
  private boolean calcMr;
  private boolean delta;
  private boolean start;
  private boolean end;
  private boolean miss;
  private boolean score;
  private boolean homol;
  private boolean ident;
  private boolean expect;
  private boolean seq;
  private boolean frame;
  private boolean varMod;
  private boolean numMatch;
  private boolean scanTitle;
  private boolean showUnassigned;
  private boolean showPepDupes;

  /**
   * Returns command line parameters for Mascot.
   *
   * @param proteinMaster
   *          protein master value
   * @return command line parameters for Mascot
   */
  public Map<String, List<String>> commandLineParameters(boolean proteinMaster) {
    Map<String, List<String>> parameters = new HashMap<>();
    addParameterIfTrue(parameters, proteinMaster, master, "peptide_master");
    addParameterIfTrue(parameters, proteinMaster, expMr, "pep_exp_mr");
    addParameterIfTrue(parameters, proteinMaster, expZ, "pep_exp_z");
    addParameterIfTrue(parameters, proteinMaster, calcMr, "pep_calc_mr");
    addParameterIfTrue(parameters, proteinMaster, delta, "pep_delta");
    addParameterIfTrue(parameters, proteinMaster, start, "pep_start");
    addParameterIfTrue(parameters, proteinMaster, end, "pep_end");
    addParameterIfTrue(parameters, proteinMaster, miss, "pep_miss");
    addParameterIfTrue(parameters, proteinMaster, score, "pep_score");
    addParameterIfTrue(parameters, proteinMaster, homol, "pep_homol");
    addParameterIfTrue(parameters, proteinMaster, ident, "pep_ident");
    addParameterIfTrue(parameters, proteinMaster, expect, "pep_expect");
    addParameterIfTrue(parameters, proteinMaster, seq, "pep_seq");
    addParameterIfTrue(parameters, proteinMaster, frame, "pep_frame");
    addParameterIfTrue(parameters, proteinMaster, varMod, "pep_var_mod");
    addParameterIfTrue(parameters, proteinMaster, numMatch, "pep_num_match");
    addParameterIfTrue(parameters, proteinMaster, scanTitle, "pep_scan_title");
    addParameterIfTrue(parameters, proteinMaster, showUnassigned, "show_unassigned");
    addParameterIfTrue(parameters, proteinMaster, showPepDupes, "show_pep_dupes");
    return parameters;
  }

  private void addParameterIfTrue(Map<String, List<String>> parameters, boolean proteinMaster,
      boolean value, String name) {
    if (proteinMaster && master && value) {
      parameters.put(name, parameter(value));
    }
  }

  private List<String> parameter(boolean value) {
    return Arrays.asList(value ? "1" : "0");
  }

  public boolean isMaster() {
    return master;
  }

  public void setMaster(boolean master) {
    this.master = master;
  }

  public boolean isExpMr() {
    return expMr;
  }

  public void setExpMr(boolean expMr) {
    this.expMr = expMr;
  }

  public boolean isExpZ() {
    return expZ;
  }

  public void setExpZ(boolean expZ) {
    this.expZ = expZ;
  }

  public boolean isCalcMr() {
    return calcMr;
  }

  public void setCalcMr(boolean calcMr) {
    this.calcMr = calcMr;
  }

  public boolean isDelta() {
    return delta;
  }

  public void setDelta(boolean delta) {
    this.delta = delta;
  }

  public boolean isStart() {
    return start;
  }

  public void setStart(boolean start) {
    this.start = start;
  }

  public boolean isEnd() {
    return end;
  }

  public void setEnd(boolean end) {
    this.end = end;
  }

  public boolean isMiss() {
    return miss;
  }

  public void setMiss(boolean miss) {
    this.miss = miss;
  }

  public boolean isScore() {
    return score;
  }

  public void setScore(boolean score) {
    this.score = score;
  }

  public boolean isHomol() {
    return homol;
  }

  public void setHomol(boolean homol) {
    this.homol = homol;
  }

  public boolean isIdent() {
    return ident;
  }

  public void setIdent(boolean ident) {
    this.ident = ident;
  }

  public boolean isExpect() {
    return expect;
  }

  public void setExpect(boolean expect) {
    this.expect = expect;
  }

  public boolean isSeq() {
    return seq;
  }

  public void setSeq(boolean seq) {
    this.seq = seq;
  }

  public boolean isFrame() {
    return frame;
  }

  public void setFrame(boolean frame) {
    this.frame = frame;
  }

  public boolean isVarMod() {
    return varMod;
  }

  public void setVarMod(boolean varMod) {
    this.varMod = varMod;
  }

  public boolean isNumMatch() {
    return numMatch;
  }

  public void setNumMatch(boolean numMatch) {
    this.numMatch = numMatch;
  }

  public boolean isScanTitle() {
    return scanTitle;
  }

  public void setScanTitle(boolean scanTitle) {
    this.scanTitle = scanTitle;
  }

  public boolean isShowUnassigned() {
    return showUnassigned;
  }

  public void setShowUnassigned(boolean showUnassigned) {
    this.showUnassigned = showUnassigned;
  }

  public boolean isShowPepDupes() {
    return showPepDupes;
  }

  public void setShowPepDupes(boolean showPepDupes) {
    this.showPepDupes = showPepDupes;
  }
}

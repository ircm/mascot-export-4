/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.task.Task;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;

/**
 * Contains jobs that are running.
 */
@Component
public class RunningExportJobs {
  private Map<ExportJob, Task> jobs = new ConcurrentHashMap<>();

  public int size() {
    return jobs.size();
  }

  public Collection<ExportJob> all() {
    return jobs.keySet();
  }

  public boolean contains(ExportJob job) {
    return jobs.containsKey(job);
  }

  public Task get(ExportJob job) {
    return jobs.get(job);
  }

  public void add(ExportJob job, Task task) {
    jobs.put(job, task);
  }

  public void remove(ExportJob job) {
    jobs.remove(job);
  }
}

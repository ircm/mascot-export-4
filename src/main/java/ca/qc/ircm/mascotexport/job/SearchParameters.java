/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import ca.qc.ircm.processing.GeneratePropertyNames;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Export job parameters.
 */
@GeneratePropertyNames
public class SearchParameters {
  private boolean master;
  private boolean showHeader;
  private boolean showMods;
  private boolean showParams;
  private boolean showFormat;
  private boolean showMasses;

  /**
   * Returns command line parameters for Mascot.
   *
   * @return command line parameters for Mascot
   */
  public Map<String, List<String>> commandLineParameters() {
    Map<String, List<String>> parameters = new HashMap<>();
    addParameterIfTrue(parameters, master, "search_master");
    addParameterIfTrue(parameters, showHeader, "show_header");
    addParameterIfTrue(parameters, showMods, "show_mods");
    addParameterIfTrue(parameters, showParams, "show_params");
    addParameterIfTrue(parameters, showFormat, "show_format");
    addParameterIfTrue(parameters, showMasses, "show_masses");
    return parameters;
  }

  private void addParameterIfTrue(Map<String, List<String>> parameters, boolean value,
      String name) {
    if (master && value) {
      parameters.put(name, parameter(value));
    }
  }

  private List<String> parameter(boolean value) {
    return Arrays.asList(value ? "1" : "0");
  }

  public boolean isMaster() {
    return master;
  }

  public void setMaster(boolean master) {
    this.master = master;
  }

  public boolean isShowHeader() {
    return showHeader;
  }

  public void setShowHeader(boolean showHeader) {
    this.showHeader = showHeader;
  }

  public boolean isShowMods() {
    return showMods;
  }

  public void setShowMods(boolean showMods) {
    this.showMods = showMods;
  }

  public boolean isShowParams() {
    return showParams;
  }

  public void setShowParams(boolean showParams) {
    this.showParams = showParams;
  }

  public boolean isShowFormat() {
    return showFormat;
  }

  public void setShowFormat(boolean showFormat) {
    this.showFormat = showFormat;
  }

  public boolean isShowMasses() {
    return showMasses;
  }

  public void setShowMasses(boolean showMasses) {
    this.showMasses = showMasses;
  }
}

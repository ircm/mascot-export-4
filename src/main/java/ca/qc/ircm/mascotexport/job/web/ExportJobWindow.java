/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.ui.Window;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export job window.
 */
@Component
@Scope("prototype")
public class ExportJobWindow extends Window implements BaseComponent {
  private static final long serialVersionUID = -699567102080714918L;
  protected ExportJobWindowDesign content = new ExportJobWindowDesign();
  @Inject
  protected ExportSearchResultsForm exportSearchResultsForm;
  @Inject
  protected SearchInformationForm searchInformationForm;
  @Inject
  protected ProteinHitInformationForm proteinHitInformationForm;
  @Inject
  protected PeptideMatchInformationForm peptideMatchInformationForm;
  @Inject
  protected QueryLevelInformationForm queryLevelInformationForm;
  private transient ExportJob job;
  @Inject
  private transient ExportJobWindowPresenter presenter;

  protected ExportJobWindow() {

  }

  protected ExportJobWindow(ExportJobWindowPresenter presenter) {
    this.presenter = presenter;
  }

  /**
   * Initializes window to view job parameters.
   */
  @PostConstruct
  public void init() {
    setContent(content);
    setHeight("500px");
    setWidth("700px");
    content.fileForm.setMargin(false);
    content.exportSearchResultsFormLayout.addComponent(exportSearchResultsForm);
    content.searchInformationFormLayout.addComponent(searchInformationForm);
    content.proteinHitInformationFormLayout.addComponent(proteinHitInformationForm);
    content.peptideMatchInformationFormLayout.addComponent(peptideMatchInformationForm);
    content.queryLevelInformationFormLayout.addComponent(queryLevelInformationForm);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
    presenter.setValue(job);
  }

  /**
   * Sets job.
   *
   * @param job
   *          job
   */
  public void setValue(ExportJob job) {
    this.job = job;
    if (isAttached()) {
      presenter.setValue(job);
    }
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobProperties;
import ca.qc.ircm.utils.MessageResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export job window.
 */
@Component
@Scope("prototype")
public class ExportJobWindowPresenter {
  public static final String STYLE = "export-job-window";
  public static final String TITLE = "title";
  public static final String FILE = ExportJobProperties.FILE_PATH;
  private static final Logger logger = LoggerFactory.getLogger(ExportJobWindowPresenter.class);
  private ExportJobWindow view;

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(ExportJobWindow view) {
    logger.debug("View job");
    this.view = view;
    prepareComponents();
  }

  private void prepareComponents() {
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    view.setCaption(resources.message(TITLE, ""));
    view.content.fileLabel.addStyleName(FILE);
    view.content.fileLabel.setCaption(resources.message(FILE));
    view.exportSearchResultsForm.setReadOnly(true);
    view.searchInformationForm.setReadOnly(true);
    view.proteinHitInformationForm.setReadOnly(true);
    view.proteinHitInformationForm.masterProperty()
        .addValueChangeListener(e -> view.peptideMatchInformationForm.proteinMasterProperty()
            .setValue(view.proteinHitInformationForm.masterProperty().getValue()));
    view.peptideMatchInformationForm.setReadOnly(true);
    view.peptideMatchInformationForm.proteinMasterProperty()
        .setValue(view.proteinHitInformationForm.masterProperty().getValue());
    view.queryLevelInformationForm.setReadOnly(true);
  }

  void setValue(ExportJob job) {
    MessageResource resources = view.getResources();
    view.setCaption(resources.message(TITLE, job.getFilePath()));
    view.content.fileLabel.setValue(job.getFilePath());
    view.exportSearchResultsForm.setBean(job.getExportParameters());
    view.searchInformationForm.setBean(job.getSearchParameters());
    view.proteinHitInformationForm.setBean(job.getProteinParameters());
    view.peptideMatchInformationForm.setBean(job.getPeptideParameters());
    view.queryLevelInformationForm.setBean(job.getQueryParameters());
  }
}

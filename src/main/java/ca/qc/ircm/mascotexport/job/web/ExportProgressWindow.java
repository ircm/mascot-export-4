/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export progress window.
 */
@Component
@Scope("prototype")
public class ExportProgressWindow extends Window implements BaseComponent {
  private static final long serialVersionUID = -8798214453957694250L;
  protected Label titleLabel = new Label();
  protected Label messageLabel = new Label();
  protected Button cancelButton = new Button();
  @Inject
  private transient ExportProgressWindowPresenter presenter;

  protected ExportProgressWindow() {
  }

  protected ExportProgressWindow(ExportProgressWindowPresenter presenter) {
    this();
    this.presenter = presenter;
  }

  /**
   * Initializes progress window for exportation.
   */
  @PostConstruct
  public void init() {
    VerticalLayout layout = new VerticalLayout();
    layout.setMargin(true);
    layout.setSpacing(true);
    setContent(layout);
    layout.addComponent(titleLabel);
    layout.addComponent(messageLabel);
    layout.addComponent(cancelButton);
    setWidth("400");
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  public Registration addCancelledClickListener(ClickListener listener) {
    return presenter.addCancelledClickListener(listener);
  }

  public String getTitle() {
    return presenter.getTitle();
  }

  public void setTitle(String title) {
    presenter.setTitle(title);
  }

  public String getMessage() {
    return presenter.getMessage();
  }

  public void setMessage(String message) {
    presenter.setMessage(message);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.utils.MessageResource;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button.ClickListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export progress window.
 */
@Component
@Scope("prototype")
public class ExportProgressWindowPresenter {
  public static final String STYLE = "export-progress-window";
  public static final String TITLE = "title";
  public static final String CANCEL = "cancel";
  private static final Logger logger = LoggerFactory.getLogger(ExportProgressWindowPresenter.class);
  private ExportProgressWindow view;

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(ExportProgressWindow view) {
    logger.debug("Export progress window");
    this.view = view;
    view.addStyleName(STYLE);
    MessageResource resources = view.getResources();
    view.setCaption(resources.message(TITLE));
    view.cancelButton.addStyleName(CANCEL);
    view.cancelButton.setCaption(resources.message(CANCEL));
    view.cancelButton.focus();
  }

  public void cancel() {
    view.cancelButton.click();
  }

  public Registration addCancelledClickListener(ClickListener listener) {
    return view.cancelButton.addClickListener(listener);
  }

  public String getTitle() {
    return view.titleLabel.getCaption();
  }

  public void setTitle(String title) {
    view.titleLabel.setCaption(title);
  }

  public String getMessage() {
    return view.messageLabel.getCaption();
  }

  public void setMessage(String message) {
    view.messageLabel.setCaption(message);
  }
}

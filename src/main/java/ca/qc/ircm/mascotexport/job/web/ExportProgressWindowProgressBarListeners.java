/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.progressbar.ProgressBarWithListeners;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ExportProgressWindowProgressBarListeners {
  private static final Logger logger =
      LoggerFactory.getLogger(ExportProgressWindowProgressBarListeners.class);
  private BiConsumer<String, String> titleListener;
  private BiConsumer<String, String> messageListener;
  private BiConsumer<Double, Double> progressListener;

  public ExportProgressWindowProgressBarListeners(UI ui, ExportProgressWindow window) {
    titleListener = (oldValue, newValue) -> updateWindow(ui, () -> window.setTitle(newValue));
    messageListener = (oldValue, newValue) -> updateWindow(ui, () -> window.setMessage(newValue));
    progressListener = (oldValue, newValue) -> {
    };
  }

  private void updateWindow(UI ui, Runnable callable) {
    try {
      if (ui.isAttached()) {
        ui.access(new Runnable() {
          @Override
          public void run() {
            callable.run();
          }
        });
      }
    } catch (UIDetachedException exception) {
      // Ignore, user disconnected.
    } catch (Throwable exception) {
      // Ignore, user probably disconnected.
      logger.trace("Exception thrown when updating title on window", exception);
    }
  }

  public void addValueChangeListeners(ProgressBarWithListeners progressBar) {
    progressBar.title().addListener(titleListener);
    progressBar.message().addListener(messageListener);
    progressBar.progress().addListener(progressListener);
  }

  public void removeValueChangeListeners(ProgressBarWithListeners progressBar) {
    progressBar.title().removeListener(titleListener);
    progressBar.message().removeListener(messageListener);
    progressBar.progress().removeListener(progressListener);
  }
}

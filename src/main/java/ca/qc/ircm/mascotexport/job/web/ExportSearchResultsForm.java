/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CustomComponent;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export search results form.
 */
@Component
@Scope("prototype")
public class ExportSearchResultsForm extends CustomComponent implements BaseComponent {
  private static final long serialVersionUID = -4446637300842336670L;
  protected ExportSearchResultsFormDesign design = new ExportSearchResultsFormDesign();
  @Inject
  private transient ExportSearchResultsFormPresenter presenter;

  protected ExportSearchResultsForm() {
  }

  protected ExportSearchResultsForm(ExportSearchResultsFormPresenter presenter) {
    this.presenter = presenter;
  }

  @PostConstruct
  public void init() {
    setCompositionRoot(design);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  @Override
  public boolean isReadOnly() {
    return presenter.isReadOnly();
  }

  @Override
  public void setReadOnly(boolean readOnly) {
    presenter.setReadOnly(readOnly);
  }

  public ExportParameters getBean() {
    return presenter.getBean();
  }

  public void setBean(ExportParameters bean) {
    presenter.setBean(bean);
  }

  public void writeBean(ExportParameters bean) throws ValidationException {
    presenter.writeBean(bean);
  }

  public BinderValidationStatus<ExportParameters> validate() {
    return presenter.validate();
  }
}

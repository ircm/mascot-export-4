/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportFormat.XML;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.ExportParametersProperties;
import ca.qc.ircm.mascotexport.web.converter.StringToNullableDoubleConverter;
import ca.qc.ircm.mascotexport.web.converter.StringToNullableIntegerConverter;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.ValidationException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export search results presenter.
 */
@Component
@Scope("prototype")
public class ExportSearchResultsFormPresenter {
  public static final String STYLE = "general-parameters";
  public static final String EXPORT_FORMAT = ExportParametersProperties.EXPORT_FORMAT;
  public static final String SIG_THRESHOLD = ExportParametersProperties.SIG_THRESHOLD;
  public static final String IGNORE_ION_SCORE_BELOW =
      ExportParametersProperties.IGNORE_IONS_SCORE_BELOW;
  public static final String USE_HOMOLOGY = ExportParametersProperties.USE_HOMOLOGY;
  public static final String REPORT = ExportParametersProperties.REPORT;
  public static final String SERVER_MUDPIT_SWITCH = ExportParametersProperties.SERVER_MUDPIT_SWITCH;
  public static final String SHOW_SAME_SETS = ExportParametersProperties.SHOW_SAME_SETS;
  public static final String SHOW_SUBSETS = ExportParametersProperties.SHOW_SUBSETS;
  public static final String GROUP_FAMILY = ExportParametersProperties.GROUP_FAMILY;
  public static final String REQUIRE_BOLD_RED = ExportParametersProperties.REQUIRE_BOLD_RED;
  public static final String PREFER_TAXONOMY = ExportParametersProperties.PREFER_TAXONOMY;
  private ExportSearchResultsForm view;
  private ExportSearchResultsFormDesign design;
  private boolean readOnly;
  private Binder<ExportParameters> binder = new Binder<>(ExportParameters.class);

  protected ExportSearchResultsFormPresenter() {
    defaultValues();
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(ExportSearchResultsForm view) {
    this.view = view;
    design = view.design;
    prepareFields();

    disableFields();
  }

  private void prepareFields() {
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    design.exportFormat.addStyleName(EXPORT_FORMAT);
    design.exportFormat.setCaption(resources.message(EXPORT_FORMAT));
    design.exportFormat.setEmptySelectionAllowed(false);
    design.exportFormat.setTextInputAllowed(false);
    design.exportFormat.setItems(CSV, XML);
    binder.forField(design.exportFormat).asRequired(resources.message(EXPORT_FORMAT + ".empty"))
        .bind(EXPORT_FORMAT);
    design.sigThreshold.addStyleName(SIG_THRESHOLD);
    design.sigThreshold.setCaption(resources.message(SIG_THRESHOLD));
    binder.forField(design.sigThreshold)
        .withConverter(
            new StringToNullableDoubleConverter(resources.message(SIG_THRESHOLD + ".invalid")))
        .bind(SIG_THRESHOLD);
    design.ignoreIonsScoreBelow.addStyleName(IGNORE_ION_SCORE_BELOW);
    design.ignoreIonsScoreBelow.setCaption(resources.message(IGNORE_ION_SCORE_BELOW));
    binder.forField(design.ignoreIonsScoreBelow).withConverter(new StringToNullableIntegerConverter(
        resources.message(IGNORE_ION_SCORE_BELOW + ".invalid"))).bind(IGNORE_ION_SCORE_BELOW);
    design.useHomology.addStyleName(USE_HOMOLOGY);
    design.useHomology.setCaption(resources.message(USE_HOMOLOGY));
    design.useHomology.setItems(false, true);
    design.useHomology
        .setItemCaptionGenerator(item -> resources.message(USE_HOMOLOGY + "." + item));
    binder.bind(design.useHomology, USE_HOMOLOGY);
    design.report.addStyleName(REPORT);
    design.report.setCaption(resources.message(REPORT));
    binder.forField(design.report)
        .withConverter(new StringToNullableIntegerConverter(resources.message(REPORT + ".invalid")))
        .bind(REPORT);
    design.serverMudpitSwitch.addStyleName(SERVER_MUDPIT_SWITCH);
    design.serverMudpitSwitch.setCaption(resources.message(SERVER_MUDPIT_SWITCH));
    design.serverMudpitSwitch.setItems(ProteinScoring.values());
    design.serverMudpitSwitch.setItemCaptionGenerator(
        item -> resources.message(SERVER_MUDPIT_SWITCH + "." + item.name()));
    binder.bind(design.serverMudpitSwitch, SERVER_MUDPIT_SWITCH);
    design.showSameSets.addStyleName(SHOW_SAME_SETS);
    design.showSameSets.setCaption(resources.message(SHOW_SAME_SETS));
    design.showSameSets.setDescription(resources.message(SHOW_SAME_SETS + ".tooltip"));
    binder.bind(design.showSameSets, SHOW_SAME_SETS);
    design.showSubsets.addStyleName(SHOW_SUBSETS);
    design.showSubsets.setCaption(resources.message(SHOW_SUBSETS));
    design.showSubsets.setDescription(resources.message(SHOW_SUBSETS + ".tooltip"));
    binder.forField(design.showSubsets)
        .withConverter(
            new StringToNullableIntegerConverter(resources.message(SHOW_SUBSETS + ".invalid")))
        .bind(SHOW_SUBSETS);
    design.groupFamily.addStyleName(GROUP_FAMILY);
    design.groupFamily.setCaption(resources.message(GROUP_FAMILY));
    design.groupFamily.addValueChangeListener(e -> {
      design.requireBoldRed.setValue(false);
      disableFields();
    });
    binder.bind(design.groupFamily, GROUP_FAMILY);
    design.requireBoldRed.addStyleName(REQUIRE_BOLD_RED);
    design.requireBoldRed.setCaption(resources.message(REQUIRE_BOLD_RED));
    binder.bind(design.requireBoldRed, REQUIRE_BOLD_RED);
    design.preferTaxonomy.addStyleName(PREFER_TAXONOMY);
    design.preferTaxonomy.setCaption(resources.message(PREFER_TAXONOMY));
    design.preferTaxonomy.setEmptySelectionAllowed(false);
    design.preferTaxonomy.setTextInputAllowed(false);
    design.preferTaxonomy.setEnabled(false);
    design.preferTaxonomy.setItemCaptionGenerator(value -> {
      if (value == null) {
        return resources.message(PREFER_TAXONOMY + ".null");
      } else {
        return String.valueOf(value);
      }
    });
    binder.forField(design.preferTaxonomy)
        .withConverter(
            new StringToNullableIntegerConverter(resources.message(PREFER_TAXONOMY + ".invalid")))
        .bind(PREFER_TAXONOMY);
    binder.setReadOnly(readOnly);
  }

  private void defaultValues() {
    ExportParameters parameters = new ExportParameters();
    parameters.setExportFormat(CSV);
    parameters.setSigThreshold(0.05);
    parameters.setIgnoreIonsScoreBelow(15);
    parameters.setUseHomology(false);
    parameters.setReport(200);
    parameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    parameters.setShowSameSets(true);
    parameters.setShowSubsets(1);
    parameters.setGroupFamily(false);
    parameters.setRequireBoldRed(true);
    binder.setBean(parameters);
  }

  private void disableFields() {
    design.requireBoldRed.setEnabled(!design.groupFamily.getValue());
  }

  boolean isReadOnly() {
    return readOnly;
  }

  void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  BinderValidationStatus<ExportParameters> validate() {
    return binder.validate();
  }

  boolean isValid() {
    return binder.isValid();
  }

  ExportParameters getBean() {
    return binder.getBean();
  }

  void setBean(ExportParameters bean) {
    binder.setBean(bean);
  }

  void writeBean(ExportParameters bean) throws ValidationException {
    binder.writeBean(bean);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.web.view.BaseView;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CustomComponent;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Export Mascot search results view.
 */
@SpringView(name = ExportView.VIEW_NAME)
public class ExportView extends CustomComponent implements BaseView {
  private static final long serialVersionUID = 5660365415506963687L;
  public static final String VIEW_NAME = "export";
  protected ExportViewDesign design = new ExportViewDesign();
  @Inject
  protected ExportSearchResultsForm exportSearchResultsForm;
  @Inject
  protected SearchInformationForm searchInformationForm;
  @Inject
  protected ProteinHitInformationForm proteinHitInformationForm;
  @Inject
  protected PeptideMatchInformationForm peptideMatchInformationForm;
  @Inject
  protected QueryLevelInformationForm queryLevelInformationForm;
  @Inject
  private transient ExportViewPresenter presenter;

  /**
   * Initializes view.
   */
  @PostConstruct
  public void init() {
    setCompositionRoot(design);
    design.exportSearchResultsFormLayout.addComponent(exportSearchResultsForm);
    design.searchInformationFormLayout.addComponent(searchInformationForm);
    design.proteinHitInformationFormLayout.addComponent(proteinHitInformationForm);
    design.peptideMatchInformationFormLayout.addComponent(peptideMatchInformationForm);
    design.queryLevelInformationFormLayout.addComponent(queryLevelInformationForm);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }
}

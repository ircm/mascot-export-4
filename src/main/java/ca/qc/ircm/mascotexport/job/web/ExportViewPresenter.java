/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.web.WebConstants.FIELD_NOTIFICATION;
import static ca.qc.ircm.mascotexport.web.WebConstants.GENERAL_MESSAGES;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionWindow;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.ValidationException;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.UserError;
import com.vaadin.ui.Grid.SelectionMode;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Export Mascot search results presenter.
 */
@Component
@Scope("prototype")
public class ExportViewPresenter {
  public static final String VIEW_NAME = ExportView.VIEW_NAME;
  public static final String ADD_FILES_STYLE = VIEW_NAME + "-addFiles";
  public static final String FILES_STYLE = VIEW_NAME + "-files";
  public static final String FILE = "this";
  public static final String SAVE_STYLE = VIEW_NAME + "-save";
  public static final String SAVE_RUN_STYLE = VIEW_NAME + "-save-run";
  private static final Logger logger = LoggerFactory.getLogger(ExportViewPresenter.class);
  private ExportView view;
  private ExportViewDesign design;
  private List<Path> files = new ArrayList<>();
  private ListDataProvider<Path> filesProvider = DataProvider.ofCollection(files);
  @Inject
  private ExportJobService jobService;
  @Inject
  private ApplicationEventPublisher publisher;
  @Inject
  private Provider<MascotFilesSelectionWindow> mascotFilesSelectionWindowProvider;

  protected ExportViewPresenter() {
  }

  protected ExportViewPresenter(ExportJobService jobService, ApplicationEventPublisher publisher,
      Provider<MascotFilesSelectionWindow> mascotFilesSelectionWindowProvider) {
    this.jobService = jobService;
    this.publisher = publisher;
    this.mascotFilesSelectionWindowProvider = mascotFilesSelectionWindowProvider;
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(ExportView view) {
    this.view = view;
    design = view.design;
    view.setId(VIEW_NAME);
    prepareFields();
    addListeners();
    view.peptideMatchInformationForm.proteinMasterProperty()
        .setValue(view.proteinHitInformationForm.masterProperty().getValue());
  }

  private void prepareFields() {
    MessageResource resources = view.getResources();
    view.setTitle(resources.message("title"));
    design.filesLabel.setValue(resources.message("files"));
    design.addFilesButton.setStyleName(ADD_FILES_STYLE);
    design.addFilesButton.setCaption(resources.message("addFiles"));
    design.filesGrid.setStyleName(FILES_STYLE);
    design.filesGrid.setSelectionMode(SelectionMode.MULTI);
    design.filesGrid.setDataProvider(filesProvider);
    design.filesGrid.addColumn(item -> item.getFileName()).setId(FILE)
        .setCaption(resources.message("file.header"))
        .setDescriptionGenerator(file -> file.toString());
    design.removeFilesButton.setCaption(resources.message("removeFiles"));
    design.parametersLabel.setValue(resources.message("parameters"));
    design.saveButton.setStyleName(SAVE_STYLE);
    design.saveButton.setCaption(resources.message("save"));
    design.saveAndRunButton.setStyleName(SAVE_RUN_STYLE);
    design.saveAndRunButton.setCaption(resources.message("saveAndRun"));
  }

  private void addListeners() {
    view.proteinHitInformationForm.masterProperty()
        .addValueChangeListener(e -> view.peptideMatchInformationForm.proteinMasterProperty()
            .setValue(view.proteinHitInformationForm.masterProperty().getValue()));
    design.addFilesButton.addClickListener(e -> addFiles());
    design.removeFilesButton.addClickListener(e -> removeFiles());
    design.saveButton.addClickListener(e -> saveJobs());
    design.saveAndRunButton.addClickListener(e -> saveAndRunJobs());
  }

  private void addFiles() {
    logger.debug("Open mascot file selection window");
    MascotFilesSelectionWindow window = mascotFilesSelectionWindowProvider.get();
    window.setModal(true);
    window.center();
    window.addSaveListener(event -> {
      List<Path> values = event.getSavedObject();
      addFiles(values);
      window.close();
    });
    view.addWindow(window);
  }

  protected void addFiles(Collection<Path> values) {
    files.clear();
    files.addAll(values);
    filesProvider.refreshAll();
  }

  private void removeFiles() {
    Collection<Path> fileIds = design.filesGrid.getSelectedItems();
    logger.debug("Remove mascot files {}", fileIds);
    for (Path fileId : fileIds) {
      design.filesGrid.deselect(fileId);
      files.remove(fileId);
    }
    filesProvider.refreshAll();
  }

  private boolean validate(MessageResource resources) {
    logger.trace("Validate jobs");
    boolean valid = true;
    valid &= validateFiles(resources);
    valid &= view.exportSearchResultsForm.validate().isOk();
    valid &= view.searchInformationForm.validate().isOk();
    valid &= view.proteinHitInformationForm.validate().isOk();
    valid &= view.peptideMatchInformationForm.validate().isOk();
    valid &= view.queryLevelInformationForm.validate().isOk();
    return valid;
  }

  private boolean validateFiles(MessageResource resources) {
    if (files.size() == 0) {
      design.filesGrid.setComponentError(new UserError(resources.message("files.empty")));
      return false;
    }
    return true;
  }

  private void saveJobs() {
    MessageResource resources = view.getResources();
    if (validate(resources)) {
      List<ExportJob> jobs = getJobs();
      logger.debug("save jobs {}", jobs);
      jobService.insert(jobs);
      view.navigateTo(JobsView.VIEW_NAME);
    } else {
      logger.debug("Validation failed");
      final MessageResource generalResources =
          new MessageResource(GENERAL_MESSAGES, view.getLocale());
      view.showError(generalResources.message(FIELD_NOTIFICATION));
    }
  }

  private void saveAndRunJobs() {
    MessageResource resources = view.getResources();
    if (validate(resources)) {
      List<ExportJob> jobs = getJobs();
      logger.debug("save and run jobs {}", jobs);
      jobService.insert(jobs);
      view.navigateTo(JobsView.VIEW_NAME);
      publisher.publishEvent(new StartProcessExportJobsEvent(design.saveAndRunButton, jobs));
    } else {
      logger.debug("Validation failed");
      final MessageResource generalResources =
          new MessageResource(GENERAL_MESSAGES, view.getLocale());
      view.showError(generalResources.message(FIELD_NOTIFICATION));
    }
  }

  private List<ExportJob> getJobs() {
    List<ExportJob> jobs = new ArrayList<>();
    for (Path file : files) {
      try {
        ExportJob job = new ExportJob();
        ExportParameters exportParameters = new ExportParameters();
        view.exportSearchResultsForm.writeBean(exportParameters);
        job.setExportParameters(exportParameters);
        SearchParameters searchParameters = new SearchParameters();
        view.searchInformationForm.writeBean(searchParameters);
        job.setSearchParameters(searchParameters);
        ProteinParameters proteinParameters = new ProteinParameters();
        view.proteinHitInformationForm.writeBean(proteinParameters);
        job.setProteinParameters(proteinParameters);
        PeptideParameters peptideParameters = new PeptideParameters();
        view.peptideMatchInformationForm.writeBean(peptideParameters);
        job.setPeptideParameters(peptideParameters);
        QueryParameters queryParameters = new QueryParameters();
        view.queryLevelInformationForm.writeBean(queryParameters);
        job.setQueryParameters(queryParameters);
        job.setFilePath(file.toString());
        jobs.add(job);
      } catch (ValidationException exception) {
        final MessageResource generalResources =
            new MessageResource(GENERAL_MESSAGES, view.getLocale());
        view.showError(generalResources.message(FIELD_NOTIFICATION));
      }
    }
    return jobs;
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobProperties;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.ButtonRenderer;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * View jobs presenter.
 */
@Component
@Scope("prototype")
public class JobsViewPresenter {
  public static final String TITLE = "title";
  public static final String JOBS_ID = "jobsGrid";
  public static final String EXECUTE_JOBS_ID = "executeJobs";
  public static final String REMOVE_JOBS_ID = "removeJobs";
  public static final String REMOVE_DONE_JOBS_ID = "removeDoneJobs";
  public static final String REFRESH_ID = "refresh";
  public static final String FILE = ExportJobProperties.FILE_PATH;
  public static final String INPUT = "input";
  public static final String DATE = ExportJobProperties.DATE;
  public static final String DONE_DATE = ExportJobProperties.DONE_DATE;
  public static final String VIEW_PARAMETERS = "viewParameters";
  public static final String REMOVE_JOB = "removeJob";
  private static final Logger logger = LoggerFactory.getLogger(JobsViewPresenter.class);
  private JobsView view;
  private JobsViewDesign design;
  @Inject
  private ExportJobService jobService;
  @Inject
  private ApplicationEventPublisher publisher;
  @Inject
  private Provider<ExportJobWindow> exportJobWindowProvider;

  protected JobsViewPresenter() {
  }

  protected JobsViewPresenter(ExportJobService jobService, ApplicationEventPublisher publisher,
      Provider<ExportJobWindow> exportJobWindowProvider) {
    this.jobService = jobService;
    this.publisher = publisher;
    this.exportJobWindowProvider = exportJobWindowProvider;
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void attach(JobsView view) {
    logger.debug("View jobs");
    this.view = view;
    design = view.design;
    prepareComponents();
  }

  private void prepareComponents() {
    MessageResource resources = view.getResources();
    view.setTitle(resources.message(TITLE));
    design.jobsGrid.setId(JOBS_ID);
    initJobsGrid();
    design.executeJobsButton.setId(EXECUTE_JOBS_ID);
    design.executeJobsButton.setCaption(resources.message(EXECUTE_JOBS_ID));
    design.executeJobsButton.addClickListener(e -> executeJobs());
    design.removeJobsButton.setId(REMOVE_JOBS_ID);
    design.removeJobsButton.setCaption(resources.message(REMOVE_JOBS_ID));
    design.removeJobsButton.addClickListener(e -> removeJobs());
    design.removeDoneJobsButton.setId(REMOVE_DONE_JOBS_ID);
    design.removeDoneJobsButton.setCaption(resources.message(REMOVE_DONE_JOBS_ID));
    design.removeDoneJobsButton.addClickListener(e -> removeDoneJobs());
    design.refreshButton.setId(REFRESH_ID);
    design.refreshButton.setCaption(resources.message(REFRESH_ID));
    design.refreshButton.addClickListener(e -> refreshJobs());
  }

  private void initJobsGrid() {
    final MessageResource resources = view.getResources();
    final DateTimeFormatter dateFormatter =
        DateTimeFormatter.ISO_LOCAL_DATE.withLocale(view.getLocale());
    design.jobsGrid.setItems(jobService.all());
    design.jobsGrid.addColumn(ExportJob::getFilePath).setId(FILE)
        .setCaption(resources.message(FILE));
    design.jobsGrid.addColumn(job -> jobService.inputFilename(job).orElse("")).setId(INPUT)
        .setCaption(resources.message(INPUT));
    design.jobsGrid
        .addColumn(job -> job.getDate() != null ? dateFormatter.format(job.getDate()) : null)
        .setId(DATE).setCaption(resources.message(DATE));
    design.jobsGrid
        .addColumn(job -> job.getDoneDate() != null ? dateFormatter.format(job.getDoneDate()) : "")
        .setId(DONE_DATE).setCaption(resources.message(DONE_DATE));
    design.jobsGrid
        .addColumn(job -> resources.message(VIEW_PARAMETERS),
            new ButtonRenderer<>(e -> viewParameters(e.getItem())))
        .setId(VIEW_PARAMETERS).setCaption(resources.message(VIEW_PARAMETERS));
    design.jobsGrid
        .addColumn(job -> resources.message(REMOVE_JOB),
            new ButtonRenderer<>(e -> removeJob(e.getItem())))
        .setId(REMOVE_JOB).setCaption(resources.message(REMOVE_JOB));
    design.jobsGrid.setSelectionMode(SelectionMode.MULTI);
    design.jobsGrid.sort(DATE, SortDirection.ASCENDING);
  }

  private void refreshJobs() {
    design.jobsGrid.deselectAll();
    design.jobsGrid.setDataProvider(DataProvider.ofCollection(jobService.all()));
    design.jobsGrid.setSortOrder(new ArrayList<>(design.jobsGrid.getSortOrder()));
  }

  private void viewParameters(ExportJob job) {
    ExportJobWindow window = exportJobWindowProvider.get();
    window.setValue(job);
    window.center();
    view.addWindow(window);
  }

  private void removeJob(ExportJob job) {
    logger.debug("remove job {}", job);
    design.jobsGrid.deselect(job);
    jobService.delete(job);
    refreshJobs();
  }

  private List<ExportJob> getSelectedJobs() {
    return new ArrayList<>(design.jobsGrid.getSelectedItems());
  }

  private void executeJobs() {
    List<ExportJob> jobs = getSelectedJobs();
    logger.debug("execute jobs {}", jobs);
    publisher.publishEvent(new StartProcessExportJobsEvent(design.executeJobsButton, jobs));
  }

  private void removeJobs() {
    List<ExportJob> jobs = getSelectedJobs();
    logger.debug("remove jobs {}", jobs);
    jobService.delete(jobs);
    refreshJobs();
  }

  @SuppressWarnings("unchecked")
  private List<ExportJob> getDoneJobs() {
    Collection<ExportJob> jobs =
        ((ListDataProvider<ExportJob>) design.jobsGrid.getDataProvider()).getItems();
    return jobs.stream().filter(job -> job.getDoneDate() != null).collect(Collectors.toList());
  }

  private void removeDoneJobs() {
    List<ExportJob> jobs = getDoneJobs();
    logger.debug("remove done jobs {}", jobs);
    jobService.delete(jobs);
    refreshJobs();
  }
}

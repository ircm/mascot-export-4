package ca.qc.ircm.mascotexport.job.web;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

/** 
 * !! DO NOT EDIT THIS FILE !!
 * 
 * This class is generated by Vaadin Designer and will be overwritten.
 * 
 * Please make a subclass with logic and additional interfaces as needed,
 * e.g class LoginView extends LoginDesign implements View { }
 */
@DesignRoot
@AutoGenerated
@SuppressWarnings("serial")
public class PeptideMatchInformationFormDesign extends VerticalLayout {
  protected CheckBox master;
  protected CheckBox expMr;
  protected CheckBox expZ;
  protected CheckBox calcMr;
  protected CheckBox delta;
  protected CheckBox start;
  protected CheckBox end;
  protected CheckBox miss;
  protected CheckBox score;
  protected CheckBox homol;
  protected CheckBox ident;
  protected CheckBox expect;
  protected CheckBox seq;
  protected CheckBox frame;
  protected CheckBox varMod;
  protected CheckBox numMatch;
  protected CheckBox scanTitle;
  protected CheckBox showUnassigned;
  protected CheckBox showPepDupes;

  public PeptideMatchInformationFormDesign() {
    Design.read(this);
  }
}

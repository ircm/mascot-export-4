/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.PeptideParametersProperties;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.CheckBox;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Peptide match information presenter.
 */
@Component
@Scope("prototype")
public class PeptideMatchInformationFormPresenter {
  public static final String STYLE = "peptide-parameters";
  public static final String PEPTIDE_MASTER = PeptideParametersProperties.MASTER;
  public static final String PEP_EXP_MR = PeptideParametersProperties.EXP_MR;
  public static final String PEP_EXP_Z = PeptideParametersProperties.EXP_Z;
  public static final String PEP_CALC_MR = PeptideParametersProperties.CALC_MR;
  public static final String PEP_DELTA = PeptideParametersProperties.DELTA;
  public static final String PEP_START = PeptideParametersProperties.START;
  public static final String PEP_END = PeptideParametersProperties.END;
  public static final String PEP_MISS = PeptideParametersProperties.MISS;
  public static final String PEP_SCORE = PeptideParametersProperties.SCORE;
  public static final String PEP_HOMOL = PeptideParametersProperties.HOMOL;
  public static final String PEP_IDENT = PeptideParametersProperties.IDENT;
  public static final String PEP_EXPECT = PeptideParametersProperties.EXPECT;
  public static final String PEP_SEQ = PeptideParametersProperties.SEQ;
  public static final String PEP_FRAME = PeptideParametersProperties.FRAME;
  public static final String PEP_VAR_MOD = PeptideParametersProperties.VAR_MOD;
  public static final String PEP_NUM_MATCH = PeptideParametersProperties.NUM_MATCH;
  public static final String PEP_SCAN_TITLE = PeptideParametersProperties.SCAN_TITLE;
  public static final String SHOW_UNASSIGNED = PeptideParametersProperties.SHOW_UNASSIGNED;
  public static final String SHOW_PEP_DUPES = PeptideParametersProperties.SHOW_PEP_DUPES;
  private PeptideMatchInformationForm view;
  private PeptideMatchInformationFormDesign design;
  private boolean readOnly;
  private HasValue<Boolean> proteinMasterProperty = new CheckBox();
  private Binder<PeptideParameters> binder = new Binder<>(PeptideParameters.class);
  private List<AbstractField<?>> fields = new ArrayList<>();

  protected PeptideMatchInformationFormPresenter() {
    defaultValues();
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(PeptideMatchInformationForm view) {
    this.view = view;
    design = view.design;
    proteinMasterProperty.addValueChangeListener(e -> disableFields());
    prepareFields();

    disableFields();
  }

  private void prepareFields() {
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    design.master.addStyleName(PEPTIDE_MASTER);
    design.master.setCaption(resources.message(PEPTIDE_MASTER));
    design.master.addValueChangeListener(e -> disableFields());
    binder.bind(design.master, PEPTIDE_MASTER);
    fields.add(design.master);
    design.expMr.addStyleName(PEP_EXP_MR);
    design.expMr.setCaption(resources.message(PEP_EXP_MR));
    binder.bind(design.expMr, PEP_EXP_MR);
    fields.add(design.expMr);
    design.expZ.addStyleName(PEP_EXP_Z);
    design.expZ.setCaption(resources.message(PEP_EXP_Z));
    binder.bind(design.expZ, PEP_EXP_Z);
    fields.add(design.expZ);
    design.calcMr.addStyleName(PEP_CALC_MR);
    design.calcMr.setCaption(resources.message(PEP_CALC_MR));
    binder.bind(design.calcMr, PEP_CALC_MR);
    fields.add(design.calcMr);
    design.delta.addStyleName(PEP_DELTA);
    design.delta.setCaption(resources.message(PEP_DELTA));
    binder.bind(design.delta, PEP_DELTA);
    fields.add(design.delta);
    design.start.addStyleName(PEP_START);
    design.start.setCaption(resources.message(PEP_START));
    binder.bind(design.start, PEP_START);
    fields.add(design.start);
    design.end.addStyleName(PEP_END);
    design.end.setCaption(resources.message(PEP_END));
    binder.bind(design.end, PEP_END);
    fields.add(design.end);
    design.miss.addStyleName(PEP_MISS);
    design.miss.setCaption(resources.message(PEP_MISS));
    binder.bind(design.miss, PEP_MISS);
    fields.add(design.miss);
    design.score.addStyleName(PEP_SCORE);
    design.score.setCaption(resources.message(PEP_SCORE));
    binder.bind(design.score, PEP_SCORE);
    fields.add(design.score);
    design.homol.addStyleName(PEP_HOMOL);
    design.homol.setCaption(resources.message(PEP_HOMOL));
    binder.bind(design.homol, PEP_HOMOL);
    fields.add(design.homol);
    design.ident.addStyleName(PEP_IDENT);
    design.ident.setCaption(resources.message(PEP_IDENT));
    binder.bind(design.ident, PEP_IDENT);
    fields.add(design.ident);
    design.expect.addStyleName(PEP_EXPECT);
    design.expect.setCaption(resources.message(PEP_EXPECT));
    binder.bind(design.expect, PEP_EXPECT);
    fields.add(design.expect);
    design.seq.addStyleName(PEP_SEQ);
    design.seq.setCaption(resources.message(PEP_SEQ));
    binder.bind(design.seq, PEP_SEQ);
    fields.add(design.seq);
    design.frame.addStyleName(PEP_FRAME);
    design.frame.setCaption(resources.message(PEP_FRAME));
    binder.bind(design.frame, PEP_FRAME);
    fields.add(design.frame);
    design.varMod.addStyleName(PEP_VAR_MOD);
    design.varMod.setCaption(resources.message(PEP_VAR_MOD));
    binder.bind(design.varMod, PEP_VAR_MOD);
    fields.add(design.varMod);
    design.numMatch.addStyleName(PEP_NUM_MATCH);
    design.numMatch.setCaption(resources.message(PEP_NUM_MATCH));
    binder.bind(design.numMatch, PEP_NUM_MATCH);
    fields.add(design.numMatch);
    design.scanTitle.addStyleName(PEP_SCAN_TITLE);
    design.scanTitle.setCaption(resources.message(PEP_SCAN_TITLE));
    binder.bind(design.scanTitle, PEP_SCAN_TITLE);
    fields.add(design.scanTitle);
    design.showUnassigned.addStyleName(SHOW_UNASSIGNED);
    design.showUnassigned.setCaption(resources.message(SHOW_UNASSIGNED));
    binder.bind(design.showUnassigned, SHOW_UNASSIGNED);
    fields.add(design.showUnassigned);
    design.showPepDupes.addStyleName(SHOW_PEP_DUPES);
    design.showPepDupes.setCaption(resources.message(SHOW_PEP_DUPES));
    binder.bind(design.showPepDupes, SHOW_PEP_DUPES);
    fields.add(design.showPepDupes);
    binder.setReadOnly(readOnly);
  }

  private void defaultValues() {
    PeptideParameters parameters = new PeptideParameters();
    parameters.setMaster(true);
    parameters.setExpMr(true);
    parameters.setExpZ(true);
    parameters.setCalcMr(true);
    parameters.setDelta(true);
    parameters.setStart(true);
    parameters.setEnd(true);
    parameters.setMiss(true);
    parameters.setScore(true);
    parameters.setHomol(false);
    parameters.setIdent(false);
    parameters.setExpect(true);
    parameters.setSeq(true);
    parameters.setFrame(false);
    parameters.setVarMod(true);
    parameters.setNumMatch(true);
    parameters.setScanTitle(true);
    parameters.setShowUnassigned(false);
    parameters.setShowPepDupes(false);
    binder.setBean(parameters);
  }

  private void disableFields() {
    boolean proteinMasterValue = proteinMasterProperty.getValue();
    boolean masterValue = design.master.getValue();
    design.master.setEnabled(proteinMasterValue);
    for (AbstractField<?> field : fields) {
      if (field != design.master) {
        field.setEnabled(masterValue && proteinMasterValue);
      }
    }
  }

  HasValue<Boolean> proteinMasterProperty() {
    return proteinMasterProperty;
  }

  boolean isReadOnly() {
    return readOnly;
  }

  void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  BinderValidationStatus<PeptideParameters> validate() {
    return binder.validate();
  }

  boolean isValid() {
    return binder.isValid();
  }

  PeptideParameters getBean() {
    return binder.getBean();
  }

  void setBean(PeptideParameters bean) {
    binder.setBean(bean);
  }

  void writeBean(PeptideParameters bean) throws ValidationException {
    binder.writeBean(bean);
  }
}

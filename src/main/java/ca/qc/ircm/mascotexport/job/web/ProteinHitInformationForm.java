/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CustomComponent;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Protein hit information form.
 */
@Component
@Scope("prototype")
public class ProteinHitInformationForm extends CustomComponent implements BaseComponent {
  private static final long serialVersionUID = -6009760702574476484L;
  protected ProteinHitInformationFormDesign design = new ProteinHitInformationFormDesign();
  @Inject
  private transient ProteinHitInformationFormPresenter presenter;

  protected ProteinHitInformationForm() {
  }

  protected ProteinHitInformationForm(ProteinHitInformationFormPresenter presenter) {
    this.presenter = presenter;
  }

  @PostConstruct
  public void init() {
    setCompositionRoot(design);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  @Override
  public boolean isReadOnly() {
    return presenter.isReadOnly();
  }

  @Override
  public void setReadOnly(boolean readOnly) {
    presenter.setReadOnly(readOnly);
  }

  public HasValue<Boolean> masterProperty() {
    return presenter.masterProperty();
  }

  public ProteinParameters getBean() {
    return presenter.getBean();
  }

  public void setBean(ProteinParameters bean) {
    presenter.setBean(bean);
  }

  public void writeBean(ProteinParameters bean) throws ValidationException {
    presenter.writeBean(bean);
  }

  public BinderValidationStatus<ProteinParameters> validate() {
    return presenter.validate();
  }
}

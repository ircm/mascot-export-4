/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.ProteinParametersProperties;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.AbstractField;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Protein hit information form presenter.
 */
@Component
@Scope("prototype")
public class ProteinHitInformationFormPresenter {
  public static final String STYLE = "protein-parameters";
  public static final String PROTEIN_MASTER = ProteinParametersProperties.MASTER;
  public static final String PROT_SCORE = ProteinParametersProperties.SCORE;
  public static final String PROT_DESC = ProteinParametersProperties.DESC;
  public static final String PROT_MASS = ProteinParametersProperties.MASS;
  public static final String PROT_MATCHES = ProteinParametersProperties.MATCHES;
  public static final String PROT_COVER = ProteinParametersProperties.COVER;
  public static final String PROT_LEN = ProteinParametersProperties.LEN;
  public static final String PROT_PI = ProteinParametersProperties.PI;
  public static final String PROT_TAX_STR = ProteinParametersProperties.TAX_STR;
  public static final String PROT_TAX_ID = ProteinParametersProperties.TAX_ID;
  public static final String PROT_SEQ = ProteinParametersProperties.SEQ;
  public static final String PROT_EMPAI = ProteinParametersProperties.EMPAI;
  public static final String SLOW = "slow";
  public static final String VERY_SLOW = "verySlow";
  private ProteinHitInformationForm view;
  private ProteinHitInformationFormDesign design;
  private boolean readOnly;
  private Binder<ProteinParameters> binder = new Binder<>(ProteinParameters.class);
  private List<AbstractField<?>> fields = new ArrayList<>();

  protected ProteinHitInformationFormPresenter() {
    defaultValues();
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(ProteinHitInformationForm view) {
    this.view = view;
    design = view.design;
    prepareFields();

    disableFields();
  }

  private void prepareFields() {
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    design.master.addStyleName(PROTEIN_MASTER);
    design.master.setCaption(resources.message(PROTEIN_MASTER));
    design.master.addValueChangeListener(e -> disableFields());
    binder.bind(design.master, PROTEIN_MASTER);
    fields.add(design.master);
    design.score.addStyleName(PROT_SCORE);
    design.score.setCaption(resources.message(PROT_SCORE));
    binder.bind(design.score, PROT_SCORE);
    fields.add(design.score);
    design.desc.addStyleName(PROT_DESC);
    design.desc.setCaption(resources.message(PROT_DESC));
    binder.bind(design.desc, PROT_DESC);
    fields.add(design.desc);
    design.mass.addStyleName(PROT_MASS);
    design.mass.setCaption(resources.message(PROT_MASS));
    binder.bind(design.mass, PROT_MASS);
    fields.add(design.mass);
    design.matches.addStyleName(PROT_MATCHES);
    design.matches.setCaption(resources.message(PROT_MATCHES));
    binder.bind(design.matches, PROT_MATCHES);
    fields.add(design.matches);
    design.cover.addStyleName(PROT_COVER);
    design.cover.setCaption(resources.message(PROT_COVER));
    binder.bind(design.cover, PROT_COVER);
    fields.add(design.cover);
    design.len.addStyleName(PROT_LEN);
    design.len.setCaption(resources.message(PROT_LEN));
    binder.bind(design.len, PROT_LEN);
    fields.add(design.len);
    design.pi.addStyleName(PROT_PI);
    design.pi.setCaption(resources.message(PROT_PI));
    binder.bind(design.pi, PROT_PI);
    fields.add(design.pi);
    design.taxStr.addStyleName(PROT_TAX_STR);
    design.taxStr.setCaption(resources.message(PROT_TAX_STR));
    binder.bind(design.taxStr, PROT_TAX_STR);
    fields.add(design.taxStr);
    design.taxId.addStyleName(PROT_TAX_ID);
    design.taxId.setCaption(resources.message(PROT_TAX_ID));
    binder.bind(design.taxId, PROT_TAX_ID);
    fields.add(design.taxId);
    design.seq.addStyleName(PROT_SEQ);
    design.seq.setCaption(resources.message(PROT_SEQ));
    binder.bind(design.seq, PROT_SEQ);
    fields.add(design.seq);
    design.empai.addStyleName(PROT_EMPAI);
    design.empai.setCaption(resources.message(PROT_EMPAI));
    binder.bind(design.empai, PROT_EMPAI);
    fields.add(design.empai);
    design.slow.setValue(resources.message(SLOW));
    design.verySlow.setValue(resources.message(VERY_SLOW));
    binder.setReadOnly(readOnly);
  }

  private void defaultValues() {
    ProteinParameters parameters = new ProteinParameters();
    parameters.setMaster(true);
    parameters.setScore(true);
    parameters.setDesc(true);
    parameters.setMass(true);
    parameters.setMatches(true);
    parameters.setCover(true);
    parameters.setLen(true);
    parameters.setPi(true);
    parameters.setTaxStr(true);
    parameters.setTaxId(true);
    parameters.setSeq(false);
    parameters.setEmpai(true);
    binder.setBean(parameters);
  }

  private void disableFields() {
    boolean masterValue = design.master.getValue();
    for (AbstractField<?> field : fields) {
      if (field != design.master) {
        field.setEnabled(masterValue);
      }
    }
  }

  HasValue<Boolean> masterProperty() {
    return design.master;
  }

  boolean isReadOnly() {
    return readOnly;
  }

  void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  BinderValidationStatus<ProteinParameters> validate() {
    return binder.validate();
  }

  boolean isValid() {
    return binder.isValid();
  }

  ProteinParameters getBean() {
    return binder.getBean();
  }

  void setBean(ProteinParameters bean) {
    binder.setBean(bean);
  }

  void writeBean(ProteinParameters bean) throws ValidationException {
    binder.writeBean(bean);
  }
}

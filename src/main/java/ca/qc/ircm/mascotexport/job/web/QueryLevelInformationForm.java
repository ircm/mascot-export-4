/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CustomComponent;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Query level information form.
 */
@Component
@Scope("prototype")
public class QueryLevelInformationForm extends CustomComponent implements BaseComponent {
  private static final long serialVersionUID = 5653629517213116754L;
  protected QueryLevelInformationFormDesign design = new QueryLevelInformationFormDesign();
  @Inject
  private transient QueryLevelInformationFormPresenter presenter;

  protected QueryLevelInformationForm() {
  }

  protected QueryLevelInformationForm(QueryLevelInformationFormPresenter presenter) {
    this.presenter = presenter;
  }

  @PostConstruct
  public void init() {
    setCompositionRoot(design);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  @Override
  public boolean isReadOnly() {
    return presenter.isReadOnly();
  }

  @Override
  public void setReadOnly(boolean readOnly) {
    presenter.setReadOnly(readOnly);
  }

  public QueryParameters getBean() {
    return presenter.getBean();
  }

  public void setBean(QueryParameters bean) {
    presenter.setBean(bean);
  }

  public void writeBean(QueryParameters bean) throws ValidationException {
    presenter.writeBean(bean);
  }

  public BinderValidationStatus<QueryParameters> validate() {
    return presenter.validate();
  }
}

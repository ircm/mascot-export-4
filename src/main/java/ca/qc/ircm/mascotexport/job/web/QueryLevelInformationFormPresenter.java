/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.QueryParametersProperties;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.AbstractField;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Query level information form presenter.
 */
@Component
@Scope("prototype")
public class QueryLevelInformationFormPresenter {
  public static final String STYLE = "query-parameters";
  public static final String QUERY_MASTER = QueryParametersProperties.MASTER;
  public static final String QUERY_TITLE = QueryParametersProperties.TITLE;
  public static final String QUERY_QUALIFIERS = QueryParametersProperties.QUALIFIERS;
  public static final String QUERY_PARAMS = QueryParametersProperties.PARAMS;
  public static final String QUERY_PEAKS = QueryParametersProperties.PEAKS;
  public static final String QUERY_RAW = QueryParametersProperties.RAW;
  private QueryLevelInformationForm view;
  private QueryLevelInformationFormDesign design;
  private boolean readOnly;
  private Binder<QueryParameters> binder = new Binder<>(QueryParameters.class);
  private List<AbstractField<?>> fields = new ArrayList<>();

  protected QueryLevelInformationFormPresenter() {
    defaultValues();
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(QueryLevelInformationForm view) {
    this.view = view;
    design = view.design;
    prepareFields();

    disableFields();
  }

  private void prepareFields() {
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    design.master.addStyleName(QUERY_MASTER);
    design.master.setCaption(resources.message(QUERY_MASTER));
    design.master.addValueChangeListener(e -> disableFields());
    binder.bind(design.master, QUERY_MASTER);
    fields.add(design.master);
    design.title.addStyleName(QUERY_TITLE);
    design.title.setCaption(resources.message(QUERY_TITLE));
    binder.bind(design.title, QUERY_TITLE);
    fields.add(design.title);
    design.qualifiers.addStyleName(QUERY_QUALIFIERS);
    design.qualifiers.setCaption(resources.message(QUERY_QUALIFIERS));
    binder.bind(design.qualifiers, QUERY_QUALIFIERS);
    fields.add(design.qualifiers);
    design.params.addStyleName(QUERY_PARAMS);
    design.params.setCaption(resources.message(QUERY_PARAMS));
    binder.bind(design.params, QUERY_PARAMS);
    fields.add(design.params);
    design.peaks.addStyleName(QUERY_PEAKS);
    design.peaks.setCaption(resources.message(QUERY_PEAKS));
    binder.bind(design.peaks, QUERY_PEAKS);
    fields.add(design.peaks);
    design.raw.addStyleName(QUERY_RAW);
    design.raw.setCaption(resources.message(QUERY_RAW));
    binder.bind(design.raw, QUERY_RAW);
    fields.add(design.raw);
    binder.setReadOnly(readOnly);
  }

  private void defaultValues() {
    QueryParameters parameters = new QueryParameters();
    parameters.setMaster(false);
    parameters.setTitle(false);
    parameters.setQualifiers(false);
    parameters.setParams(false);
    parameters.setPeaks(false);
    parameters.setRaw(false);
    binder.setBean(parameters);
  }

  private void disableFields() {
    boolean masterValue = design.master.getValue();
    for (AbstractField<?> field : fields) {
      if (field != design.master) {
        field.setEnabled(masterValue);
      }
    }
  }

  public boolean isReadOnly() {
    return readOnly;
  }

  void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  BinderValidationStatus<QueryParameters> validate() {
    return binder.validate();
  }

  boolean isValid() {
    return binder.isValid();
  }

  QueryParameters getBean() {
    return binder.getBean();
  }

  void setBean(QueryParameters bean) {
    binder.setBean(bean);
  }

  void writeBean(QueryParameters bean) throws ValidationException {
    binder.writeBean(bean);
  }
}

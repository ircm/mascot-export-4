/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CustomComponent;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Search information form.
 */
@Component
@Scope("prototype")
public class SearchInformationForm extends CustomComponent implements BaseComponent {
  private static final long serialVersionUID = 7717332953269604368L;
  protected SearchInformationFormDesign design = new SearchInformationFormDesign();
  @Inject
  private transient SearchInformationFormPresenter presenter;

  protected SearchInformationForm() {
  }

  protected SearchInformationForm(SearchInformationFormPresenter presenter) {
    this.presenter = presenter;
  }

  @PostConstruct
  public void init() {
    setCompositionRoot(design);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  @Override
  public boolean isReadOnly() {
    return presenter.isReadOnly();
  }

  @Override
  public void setReadOnly(boolean readOnly) {
    presenter.setReadOnly(readOnly);
  }

  public SearchParameters getBean() {
    return presenter.getBean();
  }

  public void setBean(SearchParameters bean) {
    presenter.setBean(bean);
  }

  public void writeBean(SearchParameters bean) throws ValidationException {
    presenter.writeBean(bean);
  }

  public BinderValidationStatus<SearchParameters> validate() {
    return presenter.validate();
  }
}

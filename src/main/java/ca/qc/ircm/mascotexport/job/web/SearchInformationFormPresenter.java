/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.job.SearchParametersProperties;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.AbstractField;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Search information form presenter.
 */
@Component
@Scope("prototype")
public class SearchInformationFormPresenter {
  public static final String STYLE = "search-parameters";
  public static final String SEARCH_MASTER = SearchParametersProperties.MASTER;
  public static final String SHOW_HEADER = SearchParametersProperties.SHOW_HEADER;
  public static final String SHOW_MODS = SearchParametersProperties.SHOW_MODS;
  public static final String SHOW_PARAMS = SearchParametersProperties.SHOW_PARAMS;
  public static final String SHOW_FORMAT = SearchParametersProperties.SHOW_FORMAT;
  public static final String SHOW_MASSES = SearchParametersProperties.SHOW_MASSES;
  private SearchInformationForm view;
  private SearchInformationFormDesign design;
  private boolean readOnly;
  private Binder<SearchParameters> binder = new Binder<>(SearchParameters.class);
  private List<AbstractField<?>> fields = new ArrayList<>();

  protected SearchInformationFormPresenter() {
    defaultValues();
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(SearchInformationForm view) {
    this.view = view;
    design = view.design;
    prepareFields();

    disableFields();
  }

  private void prepareFields() {
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    design.master.addStyleName(SEARCH_MASTER);
    design.master.setCaption(resources.message(SEARCH_MASTER));
    design.master.addValueChangeListener(e -> disableFields());
    binder.bind(design.master, SEARCH_MASTER);
    fields.add(design.master);
    design.showHeader.addStyleName(SHOW_HEADER);
    design.showHeader.setCaption(resources.message(SHOW_HEADER));
    binder.bind(design.showHeader, SHOW_HEADER);
    fields.add(design.showHeader);
    design.showMods.addStyleName(SHOW_MODS);
    design.showMods.setCaption(resources.message(SHOW_MODS));
    binder.bind(design.showMods, SHOW_MODS);
    fields.add(design.showMods);
    design.showParams.addStyleName(SHOW_PARAMS);
    design.showParams.setCaption(resources.message(SHOW_PARAMS));
    binder.bind(design.showParams, SHOW_PARAMS);
    fields.add(design.showParams);
    design.showFormat.addStyleName(SHOW_FORMAT);
    design.showFormat.setCaption(resources.message(SHOW_FORMAT));
    binder.bind(design.showFormat, SHOW_FORMAT);
    fields.add(design.showFormat);
    design.showMasses.addStyleName(SHOW_MASSES);
    design.showMasses.setCaption(resources.message(SHOW_MASSES));
    binder.bind(design.showMasses, SHOW_MASSES);
    fields.add(design.showMasses);
    binder.setReadOnly(readOnly);
  }

  private void defaultValues() {
    SearchParameters parameters = new SearchParameters();
    parameters.setMaster(true);
    parameters.setShowHeader(true);
    parameters.setShowMods(true);
    parameters.setShowParams(true);
    parameters.setShowFormat(true);
    parameters.setShowMasses(false);
    binder.setBean(parameters);
  }

  private void disableFields() {
    boolean masterValue = design.master.getValue();
    for (AbstractField<?> field : fields) {
      if (field != design.master) {
        field.setEnabled(masterValue);
      }
    }
  }

  boolean isReadOnly() {
    return readOnly;
  }

  void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
    binder.setReadOnly(readOnly);
  }

  BinderValidationStatus<SearchParameters> validate() {
    return binder.validate();
  }

  boolean isValid() {
    return binder.isValid();
  }

  SearchParameters getBean() {
    return binder.getBean();
  }

  void setBean(SearchParameters bean) {
    binder.setBean(bean);
  }

  void writeBean(SearchParameters bean) throws ValidationException {
    binder.writeBean(bean);
  }
}

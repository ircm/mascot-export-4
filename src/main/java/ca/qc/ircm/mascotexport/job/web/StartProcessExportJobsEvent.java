/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.FindbugsExplanations.SE_BAD_FIELD;

import ca.qc.ircm.mascotexport.job.ExportJob;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Collection;
import org.springframework.context.ApplicationEvent;

@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = SE_BAD_FIELD)
public class StartProcessExportJobsEvent extends ApplicationEvent {
  private static final long serialVersionUID = 5328738844411080551L;
  private final Collection<ExportJob> jobs;

  public StartProcessExportJobsEvent(Object source, Collection<ExportJob> jobs) {
    super(source);
    this.jobs = jobs;
  }

  public Collection<ExportJob> getJobs() {
    return jobs;
  }
}

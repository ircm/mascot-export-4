/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import ca.qc.ircm.mascotexport.job.ProcessExportJobService;
import ca.qc.ircm.mascotexport.task.web.CancelClickListener;
import ca.qc.ircm.mascotexport.task.web.ShowTaskProgressEvent;
import ca.qc.ircm.progressbar.ProgressBarWithListeners;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import java.util.concurrent.CancellationException;
import javax.inject.Inject;
import javax.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@UIScope
public class StartProcessExportJobsService {
  private static class Notifier implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(Notifier.class);
    private final Task task;
    private final ExportProgressWindow window;

    private Notifier(Task task, ExportProgressWindow window) {
      this.task = task;
      this.window = window;
    }

    @Override
    public void run() {
      try {
        logger.trace("Join task {}", task);
        task.join();
      } catch (InterruptedException | CancellationException exception) {
        // Ignore.
      }
      logger.trace("Task {} finished", task);
      try {
        final UI ui = window.getUI();
        if (ui.isAttached()) {
          ui.access(new Runnable() {
            @Override
            public void run() {
              logger.trace("Closing window {}", window);
              window.close();
              MessageResource resources =
                  new MessageResource(StartProcessExportJobsService.class, ui.getLocale());
              if (task.getState().getException() != null) {
                Throwable exception = task.getState().getException();
                logger.debug("Task {} failed with exception {}", task, exception, exception);
                new Notification(resources.message("processJob.error.title"),
                    resources.message("processJob.error.message"), Notification.Type.ERROR_MESSAGE)
                        .show(ui.getPage());
              } else if (!task.getState().isCancelled()) {
                logger.trace("Task {} succeded", task);
                new Notification(resources.message("processJob.done.title"),
                    resources.message("processJob.done.message"),
                    Notification.Type.TRAY_NOTIFICATION).show(ui.getPage());
              }
            }
          });
        }
      } catch (UIDetachedException exception) {
        // Ignore, user disconnected.
      } catch (Throwable exception) {
        // Ignore, user probably disconnected.
        logger.trace("Exception thrown when closing window and notifying", exception);
      }
    }
  }

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(StartProcessExportJobsService.class);
  @Inject
  private UI ui;
  @Inject
  private ProcessExportJobService processJobService;
  @Inject
  private Provider<ExportProgressWindow> exportProgressWindowProvider;

  protected StartProcessExportJobsService() {
  }

  protected StartProcessExportJobsService(UI ui, ProcessExportJobService processJobService,
      Provider<ExportProgressWindow> exportProgressWindowProvider) {
    this.ui = ui;
    this.processJobService = processJobService;
    this.exportProgressWindowProvider = exportProgressWindowProvider;
  }

  /**
   * Starts jobs specified in event.
   *
   * @param event
   *          contains jobs to start
   */
  @EventListener
  public void onStartProcessJobsEvent(StartProcessExportJobsEvent event) {
    ProgressBarWithListeners progressBar = new ProgressBarWithListeners();
    final Task task = processJobService.process(event.getJobs(), progressBar, ui.getLocale());
    openWindowForTask(task);
  }

  @EventListener
  public void onShowTaskProgressEvent(ShowTaskProgressEvent event) {
    openWindowForTask(event.getTask());
  }

  private void openWindowForTask(final Task task) {
    ProgressBarWithListeners progressBar =
        (ProgressBarWithListeners) task.getContext().getProgressBar();
    ExportProgressWindow window = exportProgressWindowProvider.get();
    ui.addWindow(window);
    ExportProgressWindowProgressBarListeners listeners =
        new ExportProgressWindowProgressBarListeners(ui, window);
    listeners.addValueChangeListeners(progressBar);
    window.addCloseListener(e -> listeners.removeValueChangeListeners(progressBar));
    window.setTitle(progressBar.getTitle());
    window.setMessage(progressBar.getMessage());
    window.addCancelledClickListener(new CancelClickListener(task));
    window.center();
    new Thread(new Notifier(task, window)).start();
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot;

import static ca.qc.ircm.mascotexport.text.Strings.normalize;

import com.vaadin.server.SerializablePredicate;
import java.nio.file.Path;

/**
 * Filters Mascot search result files.
 */
public class MascotFileFilter implements SerializablePredicate<MascotFile> {
  private static final long serialVersionUID = -7816126402936585047L;
  public String nameContains;
  public String parentContains;
  public String inputFileContains;

  @Override
  public boolean test(MascotFile mfile) {
    boolean test = true;
    if (nameContains != null) {
      if (mfile.file != null) {
        Path filenameAsPath = mfile.file.getFileName();
        if (filenameAsPath != null) {
          String nameContainsNormalized = normalize(nameContains).toLowerCase();
          String name = normalize(filenameAsPath.toString()).toLowerCase();
          test &= name.contains(nameContainsNormalized);
        } else {
          test = false;
        }
      } else {
        test = false;
      }
    }
    if (parentContains != null) {
      if (mfile.file != null && mfile.file.getParent() != null) {
        Path filenameAsPath = mfile.file.getParent().getFileName();
        if (filenameAsPath != null) {
          String parentContainsNormalized = normalize(parentContains).toLowerCase();
          String parent = normalize(filenameAsPath.toString()).toLowerCase();
          test &= parent.contains(parentContainsNormalized);
        } else {
          test = false;
        }
      } else {
        test = false;
      }
    }
    if (inputFileContains != null) {
      if (mfile.inputFile != null) {
        String inputFileContainsNormalized = normalize(inputFileContains).toLowerCase();
        String inputFile = normalize(mfile.inputFile).toLowerCase();
        test &= inputFile.contains(inputFileContainsNormalized);
      } else {
        test = false;
      }
    }
    return test;
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * Parser for Mascot dat files.
 */
@Component
public class MascotParser {
  private static final String SECTION_PATTERN =
      "Content-Type: application/x-Mascot; name=\"(\\w+)\"";
  private static final String PARAMETER_SECTION_NAME = "parameters";
  private static final int PARAMETER_MAX_LINE_NUMBER = 100;
  private static final String FILE_LINE_START = "FILE=";
  private static final String FILENAME_PATTERN = "(\\w+\\.\\w+)";

  /**
   * Parses input filename from Mascot's search file.
   *
   * @param mascot
   *          search file
   * @return input filename
   * @throws IOException
   *           could not parse Mascot file
   */
  @Cacheable("mascot-input")
  public String parseFilename(Path mascot) throws IOException {
    if (mascot == null) {
      throw new NullPointerException("mascot cannot be null.");
    } else if (!Files.exists(mascot)) {
      throw new FileNotFoundException(mascot + " does not exists");
    }

    Pattern sectionPattern = Pattern.compile(SECTION_PATTERN);
    Pattern filenamePattern = Pattern.compile(FILENAME_PATTERN);
    try (LineNumberReader reader =
        new LineNumberReader(Files.newBufferedReader(mascot, Charset.defaultCharset()))) {
      String line;
      Matcher matcher;
      boolean parameter = false;
      while ((line = reader.readLine()) != null && !parameter
          && reader.getLineNumber() < PARAMETER_MAX_LINE_NUMBER) {
        matcher = sectionPattern.matcher(line);
        if (matcher.matches() && matcher.group(1).equals(PARAMETER_SECTION_NAME)) {
          parameter = true;
        }
      }
      if (!parameter) {
        throw new IOException("Could not find parameters section in Mascot dat file");
      }
      while ((line = reader.readLine()) != null && parameter) {
        if (line.startsWith(FILE_LINE_START)) {
          String rawFilename = line.substring(FILE_LINE_START.length());
          matcher = filenamePattern.matcher(rawFilename);
          if (matcher.find()) {
            return matcher.group(1);
          } else {
            return rawFilename;
          }
        }
        parameter = !sectionPattern.matcher(line).matches();
      }
      throw new IOException("Could not find filename in parameters section of Mascot dat file");
    }
  }
}

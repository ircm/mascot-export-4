/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.web;

import ca.qc.ircm.mascotexport.web.SaveEvent;
import ca.qc.ircm.mascotexport.web.SaveListener;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.shared.Registration;
import com.vaadin.ui.CustomComponent;
import java.nio.file.Path;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Allow selection of Mascot files.
 */
@Component
@Scope("prototype")
public class MascotFilesSelection extends CustomComponent implements BaseComponent {
  public static final String STYLE = "mascot-files-selection";
  public static final String SELECT_STYLE = STYLE + "-select";
  private static final long serialVersionUID = -7879268740508364320L;
  protected MascotFilesSelectionDesign design = new MascotFilesSelectionDesign();
  @Inject
  private transient MascotFilesSelectionPresenter presenter;

  protected MascotFilesSelection() {
  }

  protected MascotFilesSelection(MascotFilesSelectionPresenter presenter) {
    this.presenter = presenter;
  }

  @PostConstruct
  public void init() {
    setCompositionRoot(design);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  public Registration addSaveListener(SaveListener<List<Path>> listener) {
    return addListener(SaveEvent.class, listener, SaveListener.SAVED_METHOD);
  }

  protected void fireSaveEvent(List<Path> files) {
    fireEvent(new SaveEvent<>(this, files));
  }
}

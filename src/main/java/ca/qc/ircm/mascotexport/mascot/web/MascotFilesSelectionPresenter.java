/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.web;

import ca.qc.ircm.mascotexport.mascot.MascotFile;
import ca.qc.ircm.mascotexport.mascot.MascotFileFilter;
import ca.qc.ircm.mascotexport.mascot.MascotService;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.GridSortOrderBuilder;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Mascot files selection presenter.
 */
@Component
@Scope("prototype")
public class MascotFilesSelectionPresenter {
  public static final String FILES = "files";
  public static final String NAME = "name";
  public static final String PARENT = "parent";
  public static final String PATH = "path";
  public static final String INPUT_FILE = "inputFile";
  public static final String SELECT = "select";
  public static final String ALL = "all";
  private static final Logger logger = LoggerFactory.getLogger(MascotFilesSelectionPresenter.class);
  private MascotFilesSelection view;
  private MascotFilesSelectionDesign design;
  private MascotFileFilter filter = new MascotFileFilter();
  @Inject
  private MascotService mascotService;

  protected MascotFilesSelectionPresenter() {
  }

  protected MascotFilesSelectionPresenter(MascotService mascotService) {
    this.mascotService = mascotService;
  }

  /**
   * Initialize presenter.
   *
   * @param view
   *          view
   */
  public void init(MascotFilesSelection view) {
    this.view = view;
    this.design = view.design;
    final MessageResource resources = view.getResources();
    design.files.addStyleName(FILES);
    try {
      ListDataProvider<MascotFile> dataProvider =
          DataProvider.ofCollection(mascotService.allFilesWithInput());
      dataProvider.setFilter(filter);
      design.files.setDataProvider(dataProvider);
    } catch (IOException exception) {
      throw new IllegalStateException("Mascot service threw exception", exception);
    }
    design.files.addColumn(file -> file.file.getFileName().toString()).setId(NAME)
        .setCaption(resources.message(NAME)).setDescriptionGenerator(file -> file.file.toString());
    design.files.addColumn(
        file -> file.file.getParent() != null ? file.file.getParent().getFileName().toString() : "")
        .setId(PARENT).setCaption(resources.message(PARENT));
    design.files.addColumn(file -> file.inputFile).setId(INPUT_FILE)
        .setCaption(resources.message(INPUT_FILE));
    HeaderRow filterRow = design.files.appendHeaderRow();
    filterRow.getCell(NAME).setComponent(textFilter(e -> {
      filter.nameContains = e.getValue();
      design.files.getDataProvider().refreshAll();
    }));
    filterRow.getCell(PARENT).setComponent(textFilter(e -> {
      filter.parentContains = e.getValue();
      design.files.getDataProvider().refreshAll();
    }));
    filterRow.getCell(INPUT_FILE).setComponent(textFilter(e -> {
      filter.inputFileContains = e.getValue();
      design.files.getDataProvider().refreshAll();
    }));
    design.files.setSelectionMode(SelectionMode.MULTI);
    design.files.setSortOrder(new GridSortOrderBuilder<MascotFile>()
        .thenDesc(design.files.getColumn(PARENT)).thenAsc(design.files.getColumn(NAME)));
    design.select.addStyleName(SELECT);
    design.select.setCaption(resources.message(SELECT));
    design.select.addClickListener(e -> select());
  }

  private TextField textFilter(ValueChangeListener<String> listener) {
    MessageResource resources = view.getResources();
    TextField filter = new TextField();
    filter.addValueChangeListener(listener);
    filter.setWidth("100%");
    filter.addStyleName(ValoTheme.TEXTFIELD_TINY);
    filter.setPlaceholder(resources.message(ALL));
    return filter;
  }

  private void select() {
    List<Path> files = design.files.getSelectedItems().stream().map(file -> file.file)
        .collect(Collectors.toList());
    logger.debug("Selected mascot files {}", files);
    view.fireSaveEvent(new ArrayList<>(files));
  }

  protected MascotFileFilter getFilter() {
    return filter;
  }
}

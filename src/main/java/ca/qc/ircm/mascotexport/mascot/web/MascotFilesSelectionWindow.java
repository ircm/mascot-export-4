/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.web;

import ca.qc.ircm.mascotexport.web.SaveListener;
import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Window;
import java.nio.file.Path;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Allow selection of Mascot files.
 */
@Component
@Scope("prototype")
public class MascotFilesSelectionWindow extends Window implements BaseComponent {
  private static final long serialVersionUID = -7305681633802715592L;
  @Inject
  protected MascotFilesSelection mascotFilesSelection;
  @Inject
  private MascotFilesSelectionWindowPresenter presenter;

  protected MascotFilesSelectionWindow() {
  }

  protected MascotFilesSelectionWindow(MascotFilesSelection mascotFilesSelection,
      MascotFilesSelectionWindowPresenter presenter) {
    this.mascotFilesSelection = mascotFilesSelection;
    this.presenter = presenter;
  }

  /**
   * Initializes window.
   */
  @PostConstruct
  public void init() {
    setContent(mascotFilesSelection);
    setWidth("800px");
    setHeight("500px");
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }

  public Registration addSaveListener(SaveListener<List<Path>> listener) {
    return mascotFilesSelection.addSaveListener(listener);
  }
}

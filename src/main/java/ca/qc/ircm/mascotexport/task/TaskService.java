/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task;

import ca.qc.ircm.task.Task;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import org.springframework.stereotype.Component;

/**
 * Services for {@link Task tasks}.
 */
@Component
public class TaskService {
  @Inject
  private Tasks tasks;

  protected TaskService() {
  }

  TaskService(Tasks tasks) {
    this.tasks = tasks;
  }

  /**
   * Selects task.
   *
   * @param id
   *          task's identifier
   * @return task
   */
  public Task get(UUID id) {
    return tasks.get(id);
  }

  /**
   * Selects all tasks.
   *
   * @return all tasks
   */
  public List<Task> tasks() {
    return new ArrayList<>(tasks.values());
  }

  /**
   * Inserts task in user's tasks.
   *
   * @param task
   *          task to insert
   */
  public void add(Task task) {
    if (task == null) {
      return;
    }

    tasks.put(task.getId(), task);
  }

  /**
   * Remove task from user's task.
   *
   * @param task
   *          task to remove
   */
  public void remove(Task task) {
    if (task == null) {
      return;
    }

    tasks.remove(task.getId());
  }

  /**
   * Remove all task from signed user that are done.
   */
  public void removeAllDone() {
    for (Task task : tasks.values()) {
      if (task.getState().isDone()) {
        tasks.remove(task.getId());
      }
    }
  }
}

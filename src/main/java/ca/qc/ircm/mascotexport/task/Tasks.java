/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task;

import ca.qc.ircm.task.Task;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;

/**
 * All tasks.
 */
@Component
public class Tasks implements Map<UUID, Task> {
  private ConcurrentHashMap<UUID, Task> map;

  public Tasks() {
    map = new ConcurrentHashMap<>();
  }

  @Override
  public int size() {
    return map.size();
  }

  @Override
  public boolean isEmpty() {
    return map.isEmpty();
  }

  @Override
  public boolean containsKey(Object key) {
    return map.containsKey(key);
  }

  @Override
  public boolean containsValue(Object value) {
    return map.containsValue(value);
  }

  @Override
  public Task get(Object key) {
    return map.get(key);
  }

  @Override
  public Task put(UUID key, Task value) {
    return map.put(key, value);
  }

  @Override
  public Task remove(Object key) {
    return map.remove(key);
  }

  @Override
  public void putAll(Map<? extends UUID, ? extends Task> tasks) {
    map.putAll(tasks);
  }

  @Override
  public void clear() {
    map.clear();
  }

  @Override
  public Set<UUID> keySet() {
    return map.keySet();
  }

  @Override
  public Collection<Task> values() {
    return map.values();
  }

  @Override
  public Set<java.util.Map.Entry<UUID, Task>> entrySet() {
    return map.entrySet();
  }

  @Override
  public boolean equals(Object other) {
    return map.equals(other);
  }

  @Override
  public int hashCode() {
    return map.hashCode();
  }
}

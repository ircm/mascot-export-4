/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task.web;

import ca.qc.ircm.task.Task;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CancelClickListener implements ClickListener {
  private static final Logger logger = LoggerFactory.getLogger(CancelClickListener.class);
  private static final long serialVersionUID = 7996771418285664165L;
  private final transient Task task;

  public CancelClickListener(Task task) {
    this.task = task;
  }

  @Override
  public void buttonClick(ClickEvent event) {
    logger.debug("Cancelling task {}", task);
    task.cancel();
  }
}
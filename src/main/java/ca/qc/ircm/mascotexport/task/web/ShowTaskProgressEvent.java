/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task.web;

import ca.qc.ircm.task.Task;
import org.springframework.context.ApplicationEvent;

public class ShowTaskProgressEvent extends ApplicationEvent {
  private static final long serialVersionUID = -994868433511294204L;
  private final transient Task task;

  public ShowTaskProgressEvent(Object source, Task task) {
    super(source);
    this.task = task;
  }

  public Task getTask() {
    return task;
  }
}

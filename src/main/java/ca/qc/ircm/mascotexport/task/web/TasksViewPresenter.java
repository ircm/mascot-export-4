/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task.web;

import ca.qc.ircm.mascotexport.job.ExportJobTaskContext;
import ca.qc.ircm.mascotexport.job.RunningExportJobs;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.ui.renderers.ButtonRenderer;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * View tasks.
 */
@Component
@Scope("prototype")
public class TasksViewPresenter {
  public static final String VIEW_NAME = TasksView.VIEW_NAME;
  public static final String ID = "id";
  public static final String JOBS = "jobs";
  public static final String PROGRESS = "progress";
  public static final String CANCEL = "cancel";
  public static final String REFRESH = "refresh";
  private static final Logger logger = LoggerFactory.getLogger(TasksViewPresenter.class);
  private TasksView view;
  private TasksViewDesign design;
  @Inject
  private RunningExportJobs runningJobs;
  @Inject
  private ApplicationEventPublisher publisher;

  protected TasksViewPresenter() {
  }

  protected TasksViewPresenter(RunningExportJobs runningJobs, ApplicationEventPublisher publisher) {
    this.runningJobs = runningJobs;
    this.publisher = publisher;
  }

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void attach(TasksView view) {
    logger.debug("Tasks view");
    this.view = view;
    design = view.design;
    view.setId(VIEW_NAME);
    prepareComponents();

    refreshTasks();
  }

  private void prepareComponents() {
    MessageResource resources = view.getResources();
    view.setTitle(resources.message("title"));
    design.tasksGrid.addColumn(task -> resources.message("task." + ID, task.getId())).setId(ID)
        .setCaption(resources.message(ID));
    design.tasksGrid.addColumn(task -> {
      if (task.getContext() instanceof ExportJobTaskContext) {
        ExportJobTaskContext jobContext = (ExportJobTaskContext) task.getContext();
        return jobContext.getJobs().size();
      } else {
        return null;
      }
    }).setId(JOBS).setCaption(resources.message(JOBS));
    design.tasksGrid
        .addColumn(task -> resources.message(PROGRESS),
            new ButtonRenderer<Task>(e -> progress(e.getItem())))
        .setId(PROGRESS).setCaption(resources.message(PROGRESS));
    design.tasksGrid
        .addColumn(task -> resources.message(CANCEL),
            new ButtonRenderer<Task>(e -> cancel(e.getItem())))
        .setId(CANCEL).setCaption(resources.message(CANCEL));
    design.refreshButton.setCaption(resources.message(REFRESH));
    design.refreshButton.addClickListener(e -> refreshTasks());
  }

  private void refreshTasks() {
    List<Task> runningTasks = runningJobs.all().stream().map(job -> runningJobs.get(job)).distinct()
        .collect(Collectors.toList());
    design.tasksGrid.setItems(runningTasks);
  }

  private void progress(Task task) {
    logger.debug("Show progress for task {}", task);
    publisher.publishEvent(new ShowTaskProgressEvent(view, task));
  }

  private void cancel(Task task) {
    task.cancel();
    refreshTasks();
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import ca.qc.ircm.mascotexport.web.component.BaseComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * About window.
 */
@Component
@Scope("prototype")
public class AboutWindow extends Window implements BaseComponent {
  private static final long serialVersionUID = -5001420256434161336L;
  protected Label label = new Label();
  @Inject
  private AboutWindowPresenter presenter;

  /**
   * Creates about window.
   */
  public AboutWindow() {
    VerticalLayout layout = new VerticalLayout();
    setContent(layout);
    layout.setMargin(true);
    layout.addComponent(label);
  }

  @Override
  public void attach() {
    super.attach();
    presenter.init(this);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import ca.qc.ircm.utils.MessageResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * About window.
 */
@Component
@Scope("prototype")
public class AboutWindowPresenter {
  public static final String STYLE = "about-window";
  public static final String TITLE = "title";
  public static final String LABEL = "label";
  private static final Logger logger = LoggerFactory.getLogger(AboutWindowPresenter.class);

  /**
   * Initializes presenter.
   *
   * @param view
   *          view
   */
  public void init(AboutWindow view) {
    logger.debug("About window");
    MessageResource resources = view.getResources();
    view.addStyleName(STYLE);
    view.setCaption(resources.message(TITLE));
    view.label.addStyleName(LABEL);
    view.label.setValue(resources.message(LABEL));
  }
}

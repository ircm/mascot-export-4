/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.UI;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main Vaadin UI.
 */
@Theme("valo")
@SpringUI
@Push(value = PushMode.AUTOMATIC, transport = Transport.LONG_POLLING)
@Widgetset("com.vaadin.DefaultWidgetSet")
public class MainUi extends UI {
  private static final Logger logger = LoggerFactory.getLogger(MainUi.class);
  private static final long serialVersionUID = -7400782917472488086L;
  @Inject
  private MainLayout layout;
  @Inject
  private Provider<SpringNavigator> springNavigatorProvider;

  /**
   * Initialize navigator.
   */
  @PostConstruct
  public void initialize() {
    SpringNavigator navigator = springNavigatorProvider.get();
    navigator.init(this, layout.design.content);
    getNavigator().setErrorView(ErrorView.class);
    setContent(layout);
    setErrorHandler(new DefaultErrorHandler() {
      private static final long serialVersionUID = -7130867541890981144L;

      @Override
      public void error(com.vaadin.server.ErrorEvent event) {
        logger.warn("Uncaught exception", event.getThrowable());
        doDefault(event);
      }
    });
  }

  @Override
  protected void init(VaadinRequest vaadinRequest) {
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import ca.qc.ircm.mascotexport.job.web.ExportView;
import ca.qc.ircm.mascotexport.web.view.BaseView;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CustomComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main view.
 */
@SpringView(name = MainView.VIEW_NAME)
public class MainView extends CustomComponent implements BaseView {
  public static final String VIEW_NAME = "";
  public static final String TITLE = "title";
  private static final long serialVersionUID = -2537732272999926530L;
  private static final Logger logger = LoggerFactory.getLogger(MainView.class);

  @Override
  public void attach() {
    logger.debug("Main view");
    super.attach();
    MessageResource resources = getResources();
    setCaption(resources.message(TITLE));
    setTitle(getCaption());
  }

  @Override
  public void enter(ViewChangeEvent event) {
    navigateTo(ExportView.VIEW_NAME);
  }
}

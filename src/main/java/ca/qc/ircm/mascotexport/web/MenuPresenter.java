/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import ca.qc.ircm.mascotexport.job.web.ExportView;
import ca.qc.ircm.mascotexport.job.web.JobsView;
import ca.qc.ircm.mascotexport.task.web.TasksView;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import javax.inject.Inject;
import javax.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Menu presenter.
 */
@Component
@Scope("prototype")
public class MenuPresenter {
  public static final String EXPORT = "export";
  public static final String JOBS = "jobs";
  public static final String TASKS = "tasks";
  public static final String HELP = "help";
  public static final String ABOUT = "about";
  private static final Logger logger = LoggerFactory.getLogger(MenuPresenter.class);
  private Menu view;
  @Inject
  private Provider<AboutWindow> aboutWindowProvider;

  protected MenuPresenter() {
  }

  protected MenuPresenter(Provider<AboutWindow> aboutWindowProvider) {
    this.aboutWindowProvider = aboutWindowProvider;
  }

  /**
   * Initialize presenter.
   *
   * @param view
   *          view
   */
  public void init(Menu view) {
    this.view = view;
    MessageResource resources = view.getResources();
    view.menuBar.addItem(resources.message(EXPORT), changeView(ExportView.VIEW_NAME));
    view.menuBar.addItem(resources.message(JOBS), changeView(JobsView.VIEW_NAME));
    view.menuBar.addItem(resources.message(TASKS), changeView(TasksView.VIEW_NAME));
    MenuItem helpItem = view.menuBar.addItem(resources.message(HELP), VaadinIcons.QUESTION, null);
    helpItem.addItem(resources.message(ABOUT), VaadinIcons.TAG, about());
  }

  private MenuBar.Command changeView(String viewName) {
    return e -> {
      logger.debug("Navigate to {}", viewName);
      view.navigateTo(viewName);
    };
  }

  private MenuBar.Command about() {
    return e -> {
      AboutWindow window = aboutWindowProvider.get();
      window.setModal(true);
      window.center();
      view.addWindow(window);
    };
  }
}

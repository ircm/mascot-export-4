/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web.converter;

import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.StringToIntegerConverter;

/**
 * Converts strings to nullable double.
 */
public class StringToNullableIntegerConverter extends StringToIntegerConverter {
  private static final long serialVersionUID = -8930742585043018600L;

  public StringToNullableIntegerConverter(String errorMessage) {
    this(null, errorMessage);
  }

  public StringToNullableIntegerConverter(Integer emptyValue, String errorMessage) {
    super(emptyValue, errorMessage);
  }

  @Override
  public String convertToPresentation(Integer value, ValueContext context) {
    if (value == null) {
      return "";
    }
    return super.convertToPresentation(value, context);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.progressbar;

/**
 * Implementation of {@link ProgressBar} using {@link Property}.
 */
public class ProgressBarWithListeners implements ProgressBar {
  /**
   * Progress value (by default between 0 and 1).
   */
  private Property<Double> progress = new Property<>(0.0);
  /**
   * Progress message.
   */
  private Property<String> message = new Property<>("");
  /**
   * Progress title.
   */
  private Property<String> title = new Property<>("");

  public Property<Double> progress() {
    return progress;
  }

  public Property<String> message() {
    return message;
  }

  public Property<String> title() {
    return title;
  }

  @Override
  public double getProgress() {
    return progress.get();
  }

  @Override
  public void setProgress(double progress) {
    progress = Math.max(0.0, progress);
    progress = Math.min(1.0, progress);
    this.progress.set(progress);
  }

  @Override
  public String getMessage() {
    return message.get();
  }

  @Override
  public void setMessage(String message) {
    this.message.set(message);
  }

  @Override
  public String getTitle() {
    return title.get();
  }

  @Override
  public void setTitle(String title) {
    this.title.set(title);
  }

  @Override
  public ProgressBar step(double step) {
    return new StepProgressBar(this, progress.get(), step);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.progressbar;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Property of a progress bar.
 *
 * @param <T>
 *          type of object in property
 */
public class Property<T> {
  private List<BiConsumer<T, T>> listeners = new ArrayList<>();
  private T value;

  public Property() {
  }

  public Property(T initialValue) {
    value = initialValue;
  }

  public T get() {
    return value;
  }

  /**
   * Sets value and inform listeners.
   *
   * @param value
   *          new value
   */
  public void set(T value) {
    T old = value;
    this.value = value;
    for (BiConsumer<T, T> listener : listeners) {
      listener.accept(old, value);
    }
  }

  public void addListener(BiConsumer<T, T> listener) {
    listeners.add(listener);
  }

  public void removeListener(BiConsumer<T, T> listener) {
    listeners.remove(listener);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import java.util.Locale;

/**
 * Command that is executed by a {@link Task}.
 */
public interface Command {
  /**
   * Execute command.
   *
   * @throws InterruptedException
   *           execution is interrupted
   */
  public void execute() throws InterruptedException, Exception;

  /**
   * Returns true if command can be undone, false otherwise.
   *
   * @return true if command can be undone, false otherwise
   */
  public boolean canUndo();

  /**
   * Undo command.
   *
   * @throws UnsupportedOperationException
   *           if command cannot be undone
   * @throws InterruptedException
   *           execution is interrupted
   */
  public void undo() throws UnsupportedOperationException, InterruptedException, Exception;

  /**
   * Returns the description of command.
   *
   * @param locale
   *          user's locale
   * @return description of command
   */
  public String getDescription(Locale locale);

  /**
   * Returns the description of the error that occurred with command, if any.
   *
   * @param exception
   *          exception
   * @param locale
   *          user's locale
   * @return description of the error that occurred with command, if any
   */
  public String getErrorDescription(Throwable exception, Locale locale);
}

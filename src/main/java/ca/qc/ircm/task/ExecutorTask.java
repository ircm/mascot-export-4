/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * {@link Task} that uses an {@link Executor} to run {@link Command} methods.
 */
public class ExecutorTask extends AbstractTask {
  private final Command command;
  private final ExecutorService executor;
  private Future<?> future;
  private final Lock lock = new ReentrantLock();

  /**
   * Creates a task for the executor.
   *
   * @param command
   *          task's command to execute
   * @param executor
   *          executor
   * @param context
   *          task's context
   */
  public ExecutorTask(Command command, ExecutorService executor, TaskContext context) {
    super(command, context);
    this.command = command;
    this.executor = executor;
  }

  @Override
  public void execute() {
    lock.lock();
    try {
      future = executor.submit(createRunnable(command, false));
    } finally {
      lock.unlock();
    }
  }

  @Override
  public boolean canUndo() {
    return command.canUndo();
  }

  @Override
  public void cancel() {
    lock.lock();
    try {
      if (future != null) {
        future.cancel(true);
      }
      this.wasCancelled();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void join() throws InterruptedException {
    Future<?> future;
    lock.lock();
    try {
      future = this.future;
    } finally {
      lock.unlock();
    }
    if (future != null) {
      try {
        future.get();
      } catch (ExecutionException exception) {
        // Ignore.
      }
    }
  }

  @Override
  public void undo() {
    if (this.canUndo()) {
      // Interrupt original task first.
      this.cancel();
      // Wait for original task completion.
      try {
        this.join();
      } catch (Exception exception) {
        // Ignore.
      }
      // Undo.
      lock.lock();
      try {
        future = executor.submit(createRunnable(command, true));
      } finally {
        lock.unlock();
      }
    } else {
      throw new UnsupportedOperationException("Task cannot be undone");
    }
  }

  @Override
  public String toString() {
    return "ExecutorTask [getId()=" + getId() + "]";
  }
}

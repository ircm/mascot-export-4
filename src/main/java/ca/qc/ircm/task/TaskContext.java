/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import ca.qc.ircm.progressbar.ProgressBar;

/**
 * Context in which task is run.
 */
public class TaskContext {
  private Task task;
  private final ProgressBar progressBar;

  public TaskContext(ProgressBar progressBar) {
    this.progressBar = progressBar;
  }

  /**
   * Called upon task creation.
   *
   * @param task
   *          task
   */
  public void setTask(Task task) {
    this.task = task;
  }

  /**
   * Returns task.
   *
   * @return task
   */
  public Task getTask() {
    return task;
  }

  /**
   * Returns progress bar used to record progression.
   *
   * @return progress bar used to record progression
   */
  public ProgressBar getProgressBar() {
    return progressBar;
  }
}

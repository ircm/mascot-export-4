/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotExportConfigurationTest {
  @Inject
  private MascotExportConfiguration mascotExportConfiguration;

  @Test
  public void defaultConfiguration() {
    Path userHome = Paths.get(System.getProperty("user.home"));
    Path mascotHome = userHome.resolve("mascot");
    assertEquals(mascotHome.resolve("cgi").resolve("export_dat_2.pl"),
        mascotExportConfiguration.getExecutable());
    List<String> arguments = mascotExportConfiguration.getArgs();
    assertNotNull(arguments);
    assertEquals(2, arguments.size());
    assertEquals("argument1", arguments.get(0));
    assertEquals("argument2", arguments.get(1));
    assertEquals(mascotHome.resolve("cgi"), mascotExportConfiguration.getWorking());
    assertEquals(mascotHome.resolve("mascot-export"), mascotExportConfiguration.getOutput());
  }
}

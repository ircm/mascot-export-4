/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.concurrent.web;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExecutorShutdownListenerTest {
  private ExecutorShutdownListener executorShutdownListener;
  @Inject
  private List<ExecutorService> executorServices = new ArrayList<>();
  @Mock
  private ServletContextEvent event;
  @Mock
  private ServletContext servletContext;
  @Mock
  private WebApplicationContext webApplicationContext;
  @Mock
  private AutowireCapableBeanFactory autowireCapableBeanFactory;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    executorShutdownListener = new ExecutorShutdownListener(executorServices);
  }

  @Test
  public void contextInitialized() {
    when(event.getServletContext()).thenReturn(servletContext);
    when(servletContext.getAttribute(any())).thenReturn(webApplicationContext);
    when(webApplicationContext.getAutowireCapableBeanFactory())
        .thenReturn(autowireCapableBeanFactory);

    executorShutdownListener.contextInitialized(event);

    verify(event).getServletContext();
    verify(servletContext)
        .getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
    verify(webApplicationContext).getAutowireCapableBeanFactory();
    verify(autowireCapableBeanFactory).autowireBean(executorShutdownListener);
  }

  @Test
  @DirtiesContext
  public void contextDestroyed() {
    executorShutdownListener.contextDestroyed(event);

    for (ExecutorService executorService : executorServices) {
      assertTrue(executorService.isShutdown());
    }
  }
}

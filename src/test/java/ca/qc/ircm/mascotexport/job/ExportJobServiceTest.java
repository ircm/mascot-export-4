/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.mascot.parser.MascotParser;
import ca.qc.ircm.mascotexport.test.config.ServiceTestAnnotations;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ServiceTestAnnotations
public class ExportJobServiceTest {
  private ExportJobService exportJobService;
  @Inject
  private ExportJobRepository exportJobRepository;
  @Inject
  private MongoTemplate mongoTemplate;
  @Mock
  private MascotParser mascotParser;
  @Mock
  private MascotConfiguration mascotConfiguration;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    exportJobService = new ExportJobService(exportJobRepository, mascotParser, mascotConfiguration);
    when(mascotConfiguration.getData()).thenReturn(Paths.get("myfolder"));
  }

  private ExportJob findJob(Collection<ExportJob> jobs, String id) {
    for (ExportJob job : jobs) {
      if (id.equals(job.getId())) {
        return job;
      }
    }
    return null;
  }

  @Test
  public void get() throws Throwable {
    ExportJob job = exportJobService.get("1");
    assertNotNull(job);

    assertEquals("1", job.getId());
    assertEquals("20110330/F017513.dat", job.getFilePath());
    assertEquals(LocalDateTime.of(2012, 5, 7, 10, 11, 27), job.getDate());
    assertEquals(null, job.getDoneDate());
    assertEquals(CSV, job.getExportParameters().getExportFormat());
    assertEquals(0.05, job.getExportParameters().getSigThreshold(), 0.00001);
    assertEquals((Integer) 15, job.getExportParameters().getIgnoreIonsScoreBelow());
    assertEquals(false, job.getExportParameters().isUseHomology());
    assertEquals((Integer) 200, job.getExportParameters().getReport());
    assertEquals(ProteinScoring.STANDARD, job.getExportParameters().getServerMudpitSwitch());
    assertEquals(true, job.getExportParameters().isShowSameSets());
    assertEquals((Integer) 1, job.getExportParameters().getShowSubsets());
    assertEquals(false, job.getExportParameters().isGroupFamily());
    assertEquals(true, job.getExportParameters().isRequireBoldRed());
    assertEquals(null, job.getExportParameters().getPreferTaxonomy());
    assertEquals(true, job.getSearchParameters().isMaster());
    assertEquals(true, job.getSearchParameters().isShowHeader());
    assertEquals(true, job.getSearchParameters().isShowMods());
    assertEquals(true, job.getSearchParameters().isShowParams());
    assertEquals(true, job.getSearchParameters().isShowFormat());
    assertEquals(false, job.getSearchParameters().isShowMasses());
    assertEquals(true, job.getProteinParameters().isMaster());
    assertEquals(true, job.getProteinParameters().isScore());
    assertEquals(true, job.getProteinParameters().isDesc());
    assertEquals(true, job.getProteinParameters().isMass());
    assertEquals(true, job.getProteinParameters().isMatches());
    assertEquals(true, job.getProteinParameters().isCover());
    assertEquals(false, job.getProteinParameters().isLen());
    assertEquals(false, job.getProteinParameters().isPi());
    assertEquals(false, job.getProteinParameters().isTaxStr());
    assertEquals(false, job.getProteinParameters().isTaxId());
    assertEquals(false, job.getProteinParameters().isSeq());
    assertEquals(true, job.getProteinParameters().isEmpai());
    assertEquals(true, job.getPeptideParameters().isMaster());
    assertEquals(true, job.getPeptideParameters().isExpMr());
    assertEquals(true, job.getPeptideParameters().isExpZ());
    assertEquals(true, job.getPeptideParameters().isCalcMr());
    assertEquals(true, job.getPeptideParameters().isDelta());
    assertEquals(true, job.getPeptideParameters().isStart());
    assertEquals(true, job.getPeptideParameters().isEnd());
    assertEquals(true, job.getPeptideParameters().isMiss());
    assertEquals(true, job.getPeptideParameters().isScore());
    assertEquals(false, job.getPeptideParameters().isHomol());
    assertEquals(false, job.getPeptideParameters().isIdent());
    assertEquals(true, job.getPeptideParameters().isExpect());
    assertEquals(true, job.getPeptideParameters().isSeq());
    assertEquals(false, job.getPeptideParameters().isFrame());
    assertEquals(true, job.getPeptideParameters().isVarMod());
    assertEquals(true, job.getPeptideParameters().isNumMatch());
    assertEquals(true, job.getPeptideParameters().isScanTitle());
    assertEquals(false, job.getPeptideParameters().isShowUnassigned());
    assertEquals(false, job.getPeptideParameters().isShowPepDupes());
    assertEquals(false, job.getQueryParameters().isMaster());
    assertEquals(false, job.getQueryParameters().isTitle());
    assertEquals(false, job.getQueryParameters().isQualifiers());
    assertEquals(false, job.getQueryParameters().isParams());
    assertEquals(false, job.getQueryParameters().isPeaks());
    assertEquals(false, job.getQueryParameters().isRaw());
  }

  @Test
  public void get_Null() throws Throwable {
    ExportJob job = exportJobService.get(null);
    assertNull(job);
  }

  @Test
  public void all() throws Throwable {
    Collection<ExportJob> jobs = exportJobService.all();
    assertEquals(4, jobs.size());
    assertNotNull(findJob(jobs, "1"));
    assertNotNull(findJob(jobs, "2"));
    assertNotNull(findJob(jobs, "3"));
    assertNotNull(findJob(jobs, "4"));
  }

  @Test
  public void allToExecute() {
    Collection<ExportJob> jobs = exportJobService.allToExecute();
    assertFalse(jobs.isEmpty());
    assertEquals(3, jobs.size());
    assertNull(findJob(jobs, "4"));
    assertNotNull(findJob(jobs, "1"));
    assertNotNull(findJob(jobs, "2"));
    assertNotNull(findJob(jobs, "3"));
  }

  @Test
  public void inputFilename() throws Throwable {
    ExportJob job = new ExportJob();
    job.setFilePath("test/myfile.dat");
    String input = "myinputfile.raw";
    when(mascotParser.parseFilename(any())).thenReturn(input);

    Optional<String> optionalInputFilename = exportJobService.inputFilename(job);

    assertTrue(optionalInputFilename.isPresent());
    assertEquals(input, optionalInputFilename.get());
    verify(mascotParser).parseFilename(mascotConfiguration.getData().resolve(job.getFilePath()));
  }

  @Test
  public void inputFilename_IoException() throws Throwable {
    ExportJob job = new ExportJob();
    job.setFilePath("test/myfile.dat");
    when(mascotParser.parseFilename(any())).thenThrow(new IOException());

    Optional<String> optionalInputFilename = exportJobService.inputFilename(job);

    assertFalse(optionalInputFilename.isPresent());
    verify(mascotParser).parseFilename(mascotConfiguration.getData().resolve(job.getFilePath()));
  }

  @Test
  public void insert() throws Throwable {
    String parameterDatFilePath = "20110330/F017513.dat";
    ExportJob job = new ExportJob();
    job.setFilePath(parameterDatFilePath);
    ExportParameters exportParameters = new ExportParameters();
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    job.setExportParameters(exportParameters);
    SearchParameters searchParameters = new SearchParameters();
    job.setSearchParameters(searchParameters);
    ProteinParameters proteinParameters = new ProteinParameters();
    job.setProteinParameters(proteinParameters);
    PeptideParameters peptideParameters = new PeptideParameters();
    job.setPeptideParameters(peptideParameters);
    QueryParameters queryParameters = new QueryParameters();
    job.setQueryParameters(queryParameters);

    exportJobService.insert(job);

    assertNotNull(job.getId());
    job = mongoTemplate.findById(job.getId(), ExportJob.class);
    assertNotNull(job.getDate());
    assertNull(job.getDoneDate());
    assertEquals(parameterDatFilePath, job.getFilePath());
    LocalDateTime beforeInsert = LocalDateTime.now().minusMinutes(1);
    assertTrue(beforeInsert.isBefore(job.getDate()));
    LocalDateTime afterInsert = LocalDateTime.now().plusMinutes(1);
    assertTrue(afterInsert.isAfter(job.getDate()));
    assertEquals(null, job.getDoneDate());
    assertEquals(exportParameters.getExportFormat(), job.getExportParameters().getExportFormat());
    assertEquals(exportParameters.getSigThreshold(), job.getExportParameters().getSigThreshold());
    assertEquals(exportParameters.getIgnoreIonsScoreBelow(),
        job.getExportParameters().getIgnoreIonsScoreBelow());
    assertEquals(exportParameters.isUseHomology(), job.getExportParameters().isUseHomology());
    assertEquals(exportParameters.getReport(), job.getExportParameters().getReport());
    assertEquals(exportParameters.getServerMudpitSwitch(),
        job.getExportParameters().getServerMudpitSwitch());
    assertEquals(exportParameters.isShowSameSets(), job.getExportParameters().isShowSameSets());
    assertEquals(exportParameters.getShowSubsets(), job.getExportParameters().getShowSubsets());
    assertEquals(exportParameters.isGroupFamily(), job.getExportParameters().isGroupFamily());
    assertEquals(exportParameters.isRequireBoldRed(), job.getExportParameters().isRequireBoldRed());
    assertEquals(exportParameters.getPreferTaxonomy(),
        job.getExportParameters().getPreferTaxonomy());
    assertEquals(searchParameters.isMaster(), job.getSearchParameters().isMaster());
    assertEquals(searchParameters.isShowHeader(), job.getSearchParameters().isShowHeader());
    assertEquals(searchParameters.isShowMods(), job.getSearchParameters().isShowMods());
    assertEquals(searchParameters.isShowParams(), job.getSearchParameters().isShowParams());
    assertEquals(searchParameters.isShowFormat(), job.getSearchParameters().isShowFormat());
    assertEquals(searchParameters.isShowMasses(), job.getSearchParameters().isShowMasses());
    assertEquals(proteinParameters.isMaster(), job.getProteinParameters().isMaster());
    assertEquals(proteinParameters.isScore(), job.getProteinParameters().isScore());
    assertEquals(proteinParameters.isDesc(), job.getProteinParameters().isDesc());
    assertEquals(proteinParameters.isMass(), job.getProteinParameters().isMass());
    assertEquals(proteinParameters.isMatches(), job.getProteinParameters().isMatches());
    assertEquals(proteinParameters.isCover(), job.getProteinParameters().isCover());
    assertEquals(proteinParameters.isLen(), job.getProteinParameters().isLen());
    assertEquals(proteinParameters.isPi(), job.getProteinParameters().isPi());
    assertEquals(proteinParameters.isTaxStr(), job.getProteinParameters().isTaxStr());
    assertEquals(proteinParameters.isTaxId(), job.getProteinParameters().isTaxId());
    assertEquals(proteinParameters.isSeq(), job.getProteinParameters().isSeq());
    assertEquals(proteinParameters.isEmpai(), job.getProteinParameters().isEmpai());
    assertEquals(peptideParameters.isMaster(), job.getPeptideParameters().isMaster());
    assertEquals(peptideParameters.isExpMr(), job.getPeptideParameters().isExpMr());
    assertEquals(peptideParameters.isExpZ(), job.getPeptideParameters().isExpZ());
    assertEquals(peptideParameters.isCalcMr(), job.getPeptideParameters().isCalcMr());
    assertEquals(peptideParameters.isDelta(), job.getPeptideParameters().isDelta());
    assertEquals(peptideParameters.isStart(), job.getPeptideParameters().isStart());
    assertEquals(peptideParameters.isEnd(), job.getPeptideParameters().isEnd());
    assertEquals(peptideParameters.isMiss(), job.getPeptideParameters().isMiss());
    assertEquals(peptideParameters.isScore(), job.getPeptideParameters().isScore());
    assertEquals(peptideParameters.isHomol(), job.getPeptideParameters().isHomol());
    assertEquals(peptideParameters.isIdent(), job.getPeptideParameters().isIdent());
    assertEquals(peptideParameters.isExpect(), job.getPeptideParameters().isExpect());
    assertEquals(peptideParameters.isSeq(), job.getPeptideParameters().isSeq());
    assertEquals(peptideParameters.isFrame(), job.getPeptideParameters().isFrame());
    assertEquals(peptideParameters.isVarMod(), job.getPeptideParameters().isVarMod());
    assertEquals(peptideParameters.isNumMatch(), job.getPeptideParameters().isNumMatch());
    assertEquals(peptideParameters.isScanTitle(), job.getPeptideParameters().isScanTitle());
    assertEquals(peptideParameters.isShowUnassigned(),
        job.getPeptideParameters().isShowUnassigned());
    assertEquals(peptideParameters.isShowPepDupes(), job.getPeptideParameters().isShowPepDupes());
    assertEquals(queryParameters.isMaster(), job.getQueryParameters().isMaster());
    assertEquals(queryParameters.isTitle(), job.getQueryParameters().isTitle());
    assertEquals(queryParameters.isQualifiers(), job.getQueryParameters().isQualifiers());
    assertEquals(queryParameters.isParams(), job.getQueryParameters().isParams());
    assertEquals(queryParameters.isPeaks(), job.getQueryParameters().isPeaks());
    assertEquals(queryParameters.isRaw(), job.getQueryParameters().isRaw());
  }

  @Test
  public void insert_Collection() throws Throwable {
    String parameterDatFilePath = "20110330/F017513.dat";
    ExportJob job = new ExportJob();
    job.setFilePath(parameterDatFilePath);
    ExportParameters exportParameters = new ExportParameters();
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    job.setExportParameters(exportParameters);
    SearchParameters searchParameters = new SearchParameters();
    job.setSearchParameters(searchParameters);
    ProteinParameters proteinParameters = new ProteinParameters();
    job.setProteinParameters(proteinParameters);
    PeptideParameters peptideParameters = new PeptideParameters();
    job.setPeptideParameters(peptideParameters);
    QueryParameters queryParameters = new QueryParameters();
    job.setQueryParameters(queryParameters);

    exportJobService.insert(Arrays.asList(job));

    assertNotNull(job.getId());
    job = mongoTemplate.findById(job.getId(), ExportJob.class);
    assertNotNull(job.getDate());
    assertNull(job.getDoneDate());
    assertEquals(parameterDatFilePath, job.getFilePath());
    LocalDateTime beforeInsert = LocalDateTime.now().minusMinutes(1);
    assertTrue(beforeInsert.isBefore(job.getDate()));
    LocalDateTime afterInsert = LocalDateTime.now().plusMinutes(1);
    assertTrue(afterInsert.isAfter(job.getDate()));
    assertEquals(null, job.getDoneDate());
    assertEquals(exportParameters.getExportFormat(), job.getExportParameters().getExportFormat());
    assertEquals(exportParameters.getSigThreshold(), job.getExportParameters().getSigThreshold());
    assertEquals(exportParameters.getIgnoreIonsScoreBelow(),
        job.getExportParameters().getIgnoreIonsScoreBelow());
    assertEquals(exportParameters.isUseHomology(), job.getExportParameters().isUseHomology());
    assertEquals(exportParameters.getReport(), job.getExportParameters().getReport());
    assertEquals(exportParameters.getServerMudpitSwitch(),
        job.getExportParameters().getServerMudpitSwitch());
    assertEquals(exportParameters.isShowSameSets(), job.getExportParameters().isShowSameSets());
    assertEquals(exportParameters.getShowSubsets(), job.getExportParameters().getShowSubsets());
    assertEquals(exportParameters.isGroupFamily(), job.getExportParameters().isGroupFamily());
    assertEquals(exportParameters.isRequireBoldRed(), job.getExportParameters().isRequireBoldRed());
    assertEquals(exportParameters.getPreferTaxonomy(),
        job.getExportParameters().getPreferTaxonomy());
    assertEquals(searchParameters.isMaster(), job.getSearchParameters().isMaster());
    assertEquals(searchParameters.isShowHeader(), job.getSearchParameters().isShowHeader());
    assertEquals(searchParameters.isShowMods(), job.getSearchParameters().isShowMods());
    assertEquals(searchParameters.isShowParams(), job.getSearchParameters().isShowParams());
    assertEquals(searchParameters.isShowFormat(), job.getSearchParameters().isShowFormat());
    assertEquals(searchParameters.isShowMasses(), job.getSearchParameters().isShowMasses());
    assertEquals(proteinParameters.isMaster(), job.getProteinParameters().isMaster());
    assertEquals(proteinParameters.isScore(), job.getProteinParameters().isScore());
    assertEquals(proteinParameters.isDesc(), job.getProteinParameters().isDesc());
    assertEquals(proteinParameters.isMass(), job.getProteinParameters().isMass());
    assertEquals(proteinParameters.isMatches(), job.getProteinParameters().isMatches());
    assertEquals(proteinParameters.isCover(), job.getProteinParameters().isCover());
    assertEquals(proteinParameters.isLen(), job.getProteinParameters().isLen());
    assertEquals(proteinParameters.isPi(), job.getProteinParameters().isPi());
    assertEquals(proteinParameters.isTaxStr(), job.getProteinParameters().isTaxStr());
    assertEquals(proteinParameters.isTaxId(), job.getProteinParameters().isTaxId());
    assertEquals(proteinParameters.isSeq(), job.getProteinParameters().isSeq());
    assertEquals(proteinParameters.isEmpai(), job.getProteinParameters().isEmpai());
    assertEquals(peptideParameters.isMaster(), job.getPeptideParameters().isMaster());
    assertEquals(peptideParameters.isExpMr(), job.getPeptideParameters().isExpMr());
    assertEquals(peptideParameters.isExpZ(), job.getPeptideParameters().isExpZ());
    assertEquals(peptideParameters.isCalcMr(), job.getPeptideParameters().isCalcMr());
    assertEquals(peptideParameters.isDelta(), job.getPeptideParameters().isDelta());
    assertEquals(peptideParameters.isStart(), job.getPeptideParameters().isStart());
    assertEquals(peptideParameters.isEnd(), job.getPeptideParameters().isEnd());
    assertEquals(peptideParameters.isMiss(), job.getPeptideParameters().isMiss());
    assertEquals(peptideParameters.isScore(), job.getPeptideParameters().isScore());
    assertEquals(peptideParameters.isHomol(), job.getPeptideParameters().isHomol());
    assertEquals(peptideParameters.isIdent(), job.getPeptideParameters().isIdent());
    assertEquals(peptideParameters.isExpect(), job.getPeptideParameters().isExpect());
    assertEquals(peptideParameters.isSeq(), job.getPeptideParameters().isSeq());
    assertEquals(peptideParameters.isFrame(), job.getPeptideParameters().isFrame());
    assertEquals(peptideParameters.isVarMod(), job.getPeptideParameters().isVarMod());
    assertEquals(peptideParameters.isNumMatch(), job.getPeptideParameters().isNumMatch());
    assertEquals(peptideParameters.isScanTitle(), job.getPeptideParameters().isScanTitle());
    assertEquals(peptideParameters.isShowUnassigned(),
        job.getPeptideParameters().isShowUnassigned());
    assertEquals(peptideParameters.isShowPepDupes(), job.getPeptideParameters().isShowPepDupes());
    assertEquals(queryParameters.isMaster(), job.getQueryParameters().isMaster());
    assertEquals(queryParameters.isTitle(), job.getQueryParameters().isTitle());
    assertEquals(queryParameters.isQualifiers(), job.getQueryParameters().isQualifiers());
    assertEquals(queryParameters.isParams(), job.getQueryParameters().isParams());
    assertEquals(queryParameters.isPeaks(), job.getQueryParameters().isPeaks());
    assertEquals(queryParameters.isRaw(), job.getQueryParameters().isRaw());
  }

  @Test
  public void jobDone() throws Throwable {
    ExportJob job = exportJobService.get("1");
    assertNull(job.getDoneDate());

    exportJobService.jobDone(job);

    job = exportJobService.get("1");
    assertNotNull(job.getDoneDate());
  }

  @Test
  public void delete() throws Throwable {
    ExportJob job = exportJobService.get("1");
    assertNotNull(job);

    exportJobService.delete(job);

    job = exportJobService.get("1");
    assertNull(job);
  }

  @Test
  public void delete_Many() throws Throwable {
    ExportJob job1 = mongoTemplate.findById("1", ExportJob.class);
    assertNotNull(job1);
    ExportJob job2 = mongoTemplate.findById("2", ExportJob.class);
    assertNotNull(job2);
    Collection<ExportJob> jobs = Arrays.asList(job1, job2);

    exportJobService.delete(jobs);

    job1 = mongoTemplate.findById("1", ExportJob.class);
    assertNull(job1);
    job2 = mongoTemplate.findById("2", ExportJob.class);
    assertNull(job2);
  }
}

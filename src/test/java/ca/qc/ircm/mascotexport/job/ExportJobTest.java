/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportJobTest {
  private ExportJob exportJob;
  @Mock
  private ExportParameters exportParameters;
  @Mock
  private SearchParameters searchParameters;
  @Mock
  private ProteinParameters proteinParameters;
  @Mock
  private PeptideParameters peptideParameters;
  @Mock
  private QueryParameters queryParameters;
  private Random random = new Random();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    exportJob = new ExportJob();
    exportJob.setExportParameters(exportParameters);
    exportJob.setSearchParameters(searchParameters);
    exportJob.setProteinParameters(proteinParameters);
    exportJob.setPeptideParameters(peptideParameters);
    exportJob.setQueryParameters(queryParameters);
  }

  private Map<String, List<String>> randomParametersMap() {
    Map<String, List<String>> parameters = new HashMap<>();
    IntStream.range(0, random.nextInt(11) + 1).forEach(i -> {
      parameters.put(RandomStringUtils.randomAlphanumeric(10),
          Arrays.asList(RandomStringUtils.randomAlphanumeric(8)));
    });
    return parameters;
  }

  @Test
  public void commandLineParameters() {
    Map<String, List<String>> exportParametersMap = randomParametersMap();
    Map<String, List<String>> searchParametersMap = randomParametersMap();
    Map<String, List<String>> proteinParametersMap = randomParametersMap();
    Map<String, List<String>> peptideParametersMap = randomParametersMap();
    Map<String, List<String>> queryParametersMap = randomParametersMap();
    when(exportParameters.commandLineParameters()).thenReturn(exportParametersMap);
    when(searchParameters.commandLineParameters()).thenReturn(searchParametersMap);
    when(proteinParameters.commandLineParameters()).thenReturn(proteinParametersMap);
    when(peptideParameters.commandLineParameters(anyBoolean())).thenReturn(peptideParametersMap);
    when(queryParameters.commandLineParameters()).thenReturn(queryParametersMap);

    final Map<String, List<String>> parameters = exportJob.commandLineParameters(6);

    verify(exportParameters).commandLineParameters();
    verify(searchParameters).commandLineParameters();
    verify(proteinParameters).commandLineParameters();
    verify(proteinParameters, atLeastOnce()).isMaster();
    verify(peptideParameters).commandLineParameters(false);
    verify(queryParameters).commandLineParameters();
    Map<String, List<String>> expectedParameters = new HashMap<>();
    expectedParameters.putAll(exportParametersMap);
    expectedParameters.putAll(searchParametersMap);
    expectedParameters.putAll(proteinParametersMap);
    expectedParameters.putAll(peptideParametersMap);
    expectedParameters.putAll(queryParametersMap);
    assertEquals(expectedParameters.size() + 10, parameters.size());
    for (Map.Entry<String, List<String>> entry : expectedParameters.entrySet()) {
      assertTrue(parameters.containsKey(entry.getKey()));
      assertEquals(1, parameters.get(entry.getKey()).size());
      assertEquals(entry.getValue().get(0), parameters.get(entry.getKey()).get(0));
    }
    assertTrue(parameters.containsKey("_minpeplen"));
    assertEquals(1, parameters.get("_minpeplen").size());
    assertEquals("6", parameters.get("_minpeplen").get(0));
    assertTrue(parameters.containsKey("do_export"));
    assertEquals(1, parameters.get("do_export").size());
    assertEquals("1", parameters.get("do_export").get(0));
    assertTrue(parameters.containsKey("pep_exp_mz"));
    assertEquals(1, parameters.get("pep_exp_mz").size());
    assertEquals("1", parameters.get("pep_exp_mz").get(0));
    assertTrue(parameters.containsKey("pep_isbold"));
    assertEquals(1, parameters.get("pep_isbold").size());
    assertEquals("1", parameters.get("pep_isbold").get(0));
    assertTrue(parameters.containsKey("pep_isunique"));
    assertEquals(1, parameters.get("pep_isunique").size());
    assertEquals("1", parameters.get("pep_isunique").get(0));
    assertTrue(parameters.containsKey("pep_query"));
    assertEquals(1, parameters.get("pep_query").size());
    assertEquals("1", parameters.get("pep_query").get(0));
    assertTrue(parameters.containsKey("pep_rank"));
    assertEquals(1, parameters.get("pep_rank").size());
    assertEquals("1", parameters.get("pep_rank").get(0));
    assertTrue(parameters.containsKey("prot_acc"));
    assertEquals(1, parameters.get("prot_acc").size());
    assertEquals("1", parameters.get("prot_acc").get(0));
    assertTrue(parameters.containsKey("prot_hit_num"));
    assertEquals(1, parameters.get("prot_hit_num").size());
    assertEquals("1", parameters.get("prot_hit_num").get(0));
    assertTrue(parameters.containsKey("sessionid"));
    assertEquals(1, parameters.get("sessionid").size());
    assertEquals("all_secdisabledsession", parameters.get("sessionid").get(0));
  }

  @Test
  public void commandLineParameters_AllEmpty() {
    Map<String, List<String>> parameters = exportJob.commandLineParameters(6);

    assertEquals(10, parameters.size());
    verify(exportParameters).commandLineParameters();
    verify(searchParameters).commandLineParameters();
    verify(proteinParameters).commandLineParameters();
    verify(proteinParameters, atLeastOnce()).isMaster();
    verify(peptideParameters).commandLineParameters(false);
    verify(queryParameters).commandLineParameters();
    assertTrue(parameters.containsKey("_minpeplen"));
    assertEquals(1, parameters.get("_minpeplen").size());
    assertEquals("6", parameters.get("_minpeplen").get(0));
    assertTrue(parameters.containsKey("do_export"));
    assertEquals(1, parameters.get("do_export").size());
    assertEquals("1", parameters.get("do_export").get(0));
    assertTrue(parameters.containsKey("pep_exp_mz"));
    assertEquals(1, parameters.get("pep_exp_mz").size());
    assertEquals("1", parameters.get("pep_exp_mz").get(0));
    assertTrue(parameters.containsKey("pep_isbold"));
    assertEquals(1, parameters.get("pep_isbold").size());
    assertEquals("1", parameters.get("pep_isbold").get(0));
    assertTrue(parameters.containsKey("pep_isunique"));
    assertEquals(1, parameters.get("pep_isunique").size());
    assertEquals("1", parameters.get("pep_isunique").get(0));
    assertTrue(parameters.containsKey("pep_query"));
    assertEquals(1, parameters.get("pep_query").size());
    assertEquals("1", parameters.get("pep_query").get(0));
    assertTrue(parameters.containsKey("pep_rank"));
    assertEquals(1, parameters.get("pep_rank").size());
    assertEquals("1", parameters.get("pep_rank").get(0));
    assertTrue(parameters.containsKey("prot_acc"));
    assertEquals(1, parameters.get("prot_acc").size());
    assertEquals("1", parameters.get("prot_acc").get(0));
    assertTrue(parameters.containsKey("prot_hit_num"));
    assertEquals(1, parameters.get("prot_hit_num").size());
    assertEquals("1", parameters.get("prot_hit_num").get(0));
    assertTrue(parameters.containsKey("sessionid"));
    assertEquals(1, parameters.get("sessionid").size());
    assertEquals("all_secdisabledsession", parameters.get("sessionid").get(0));
  }

  @Test
  public void commandLineParameters_MinimumPeptideLenghtSummary() {
    final Map<String, List<String>> parameters = exportJob.commandLineParameters(2);

    assertTrue(parameters.containsKey("_minpeplen"));
    assertEquals(1, parameters.get("_minpeplen").size());
    assertEquals("2", parameters.get("_minpeplen").get(0));
  }

  @Test
  public void commandLineParameters_ProteinMaster() {
    when(proteinParameters.isMaster()).thenReturn(true);

    exportJob.commandLineParameters(6);

    verify(proteinParameters, atLeastOnce()).isMaster();
    verify(peptideParameters).commandLineParameters(true);
  }
}

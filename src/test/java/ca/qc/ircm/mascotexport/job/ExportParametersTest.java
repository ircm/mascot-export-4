/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static ca.qc.ircm.mascotexport.ProteinScoring.MUDPIT;
import static ca.qc.ircm.mascotexport.ProteinScoring.STANDARD;
import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportFormat.XML;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportParametersTest {
  private ExportParameters exportParameters = new ExportParameters();

  @Test
  public void commandLineParameters_AllNullOrFalse() {
    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_All() {
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(true);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(true);
    exportParameters.setRequireBoldRed(true);
    exportParameters.setPreferTaxonomy(1);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(10, parameters.size());
    assertTrue(parameters.containsKey("export_format"));
    assertEquals(1, parameters.get("export_format").size());
    assertEquals("CSV", parameters.get("export_format").get(0));
    assertTrue(parameters.containsKey("_sigthreshold"));
    assertEquals(1, parameters.get("_sigthreshold").size());
    assertEquals("0.05", parameters.get("_sigthreshold").get(0));
    assertTrue(parameters.containsKey("_ignoreionsscorebelow"));
    assertEquals(1, parameters.get("_ignoreionsscorebelow").size());
    assertEquals("15", parameters.get("_ignoreionsscorebelow").get(0));
    assertTrue(parameters.containsKey("use_homology"));
    assertEquals(1, parameters.get("use_homology").size());
    assertEquals("1", parameters.get("use_homology").get(0));
    assertTrue(parameters.containsKey("report"));
    assertEquals(1, parameters.get("report").size());
    assertEquals("200", parameters.get("report").get(0));
    assertTrue(parameters.containsKey("_server_mudpit_switch"));
    assertEquals(1, parameters.get("_server_mudpit_switch").size());
    assertEquals(STANDARD.value, parameters.get("_server_mudpit_switch").get(0));
    assertTrue(parameters.containsKey("show_same_sets"));
    assertEquals(1, parameters.get("show_same_sets").size());
    assertEquals("1", parameters.get("show_same_sets").get(0));
    assertTrue(parameters.containsKey("_showsubsets"));
    assertEquals(1, parameters.get("_showsubsets").size());
    assertEquals("1", parameters.get("_showsubsets").get(0));
    assertTrue(parameters.containsKey("group_family"));
    assertEquals(1, parameters.get("group_family").size());
    assertEquals("1", parameters.get("group_family").get(0));
    assertFalse(parameters.containsKey("_requireboldred"));
    assertTrue(parameters.containsKey("_prefertaxonomy"));
    assertEquals(1, parameters.get("_prefertaxonomy").size());
    assertEquals("1", parameters.get("_prefertaxonomy").get(0));
  }

  @Test
  public void commandLineParameters_ExportFormat() {
    exportParameters.setExportFormat(XML);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("export_format"));
    assertEquals(1, parameters.get("export_format").size());
    assertEquals("XML", parameters.get("export_format").get(0));
  }

  @Test
  public void commandLineParameters_SigThreshold() {
    exportParameters.setSigThreshold(0.23);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("_sigthreshold"));
    assertEquals(1, parameters.get("_sigthreshold").size());
    assertEquals("0.23", parameters.get("_sigthreshold").get(0));
  }

  @Test
  public void commandLineParameters_IgnoreIonsScoreBelow() {
    exportParameters.setIgnoreIonsScoreBelow(12);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("_ignoreionsscorebelow"));
    assertEquals(1, parameters.get("_ignoreionsscorebelow").size());
    assertEquals("12", parameters.get("_ignoreionsscorebelow").get(0));
  }

  @Test
  public void commandLineParameters_UseHomology() {
    exportParameters.setUseHomology(true);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("use_homology"));
    assertEquals(1, parameters.get("use_homology").size());
    assertEquals("1", parameters.get("use_homology").get(0));
  }

  @Test
  public void commandLineParameters_Report() {
    exportParameters.setReport(72);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("report"));
    assertEquals(1, parameters.get("report").size());
    assertEquals("72", parameters.get("report").get(0));
  }

  @Test
  public void commandLineParameters_ServerMudpitSwitch() {
    exportParameters.setServerMudpitSwitch(MUDPIT);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("_server_mudpit_switch"));
    assertEquals(1, parameters.get("_server_mudpit_switch").size());
    assertEquals(MUDPIT.value, parameters.get("_server_mudpit_switch").get(0));
  }

  @Test
  public void commandLineParameters_ShowSameSets() {
    exportParameters.setShowSameSets(true);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("show_same_sets"));
    assertEquals(1, parameters.get("show_same_sets").size());
    assertEquals("1", parameters.get("show_same_sets").get(0));
  }

  @Test
  public void commandLineParameters_ShowSubsets() {
    exportParameters.setShowSubsets(4);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("_showsubsets"));
    assertEquals(1, parameters.get("_showsubsets").size());
    assertEquals("4", parameters.get("_showsubsets").get(0));
  }

  @Test
  public void commandLineParameters_GroupFamily() {
    exportParameters.setGroupFamily(true);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("group_family"));
    assertEquals(1, parameters.get("group_family").size());
    assertEquals("1", parameters.get("group_family").get(0));
  }

  @Test
  public void commandLineParameters_RequireBoldRed() {
    exportParameters.setRequireBoldRed(true);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("_requireboldred"));
    assertEquals(1, parameters.get("_requireboldred").size());
    assertEquals("1", parameters.get("_requireboldred").get(0));
  }

  @Test
  public void commandLineParameters_GroupFamily_RequireBoldRed() {
    exportParameters.setGroupFamily(true);
    exportParameters.setRequireBoldRed(true);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("group_family"));
    assertEquals(1, parameters.get("group_family").size());
    assertEquals("1", parameters.get("group_family").get(0));
  }

  @Test
  public void commandLineParameters_PreferTaxonomy() {
    exportParameters.setPreferTaxonomy(9606);

    Map<String, List<String>> parameters = exportParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("_prefertaxonomy"));
    assertEquals(1, parameters.get("_prefertaxonomy").size());
    assertEquals("9606", parameters.get("_prefertaxonomy").get(0));
  }
}

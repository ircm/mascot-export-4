/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ProteinParametersTest {
  private ProteinParameters proteinParameters = new ProteinParameters();

  @Test
  public void commandLineParameters_AllFalse() {
    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_AllTrue() {
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(true);
    proteinParameters.setPi(true);
    proteinParameters.setTaxStr(true);
    proteinParameters.setTaxId(true);
    proteinParameters.setSeq(true);
    proteinParameters.setEmpai(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(12, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_score"));
    assertEquals(1, parameters.get("prot_score").size());
    assertEquals("1", parameters.get("prot_score").get(0));
    assertTrue(parameters.containsKey("prot_desc"));
    assertEquals(1, parameters.get("prot_desc").size());
    assertEquals("1", parameters.get("prot_desc").get(0));
    assertTrue(parameters.containsKey("prot_mass"));
    assertEquals(1, parameters.get("prot_mass").size());
    assertEquals("1", parameters.get("prot_mass").get(0));
    assertTrue(parameters.containsKey("prot_matches"));
    assertEquals(1, parameters.get("prot_matches").size());
    assertEquals("1", parameters.get("prot_matches").get(0));
    assertTrue(parameters.containsKey("prot_cover"));
    assertEquals(1, parameters.get("prot_cover").size());
    assertEquals("1", parameters.get("prot_cover").get(0));
    assertTrue(parameters.containsKey("prot_len"));
    assertEquals(1, parameters.get("prot_len").size());
    assertEquals("1", parameters.get("prot_len").get(0));
    assertTrue(parameters.containsKey("prot_pi"));
    assertEquals(1, parameters.get("prot_pi").size());
    assertEquals("1", parameters.get("prot_pi").get(0));
    assertTrue(parameters.containsKey("prot_tax_str"));
    assertEquals(1, parameters.get("prot_tax_str").size());
    assertEquals("1", parameters.get("prot_tax_str").get(0));
    assertTrue(parameters.containsKey("prot_tax_id"));
    assertEquals(1, parameters.get("prot_tax_id").size());
    assertEquals("1", parameters.get("prot_tax_id").get(0));
    assertTrue(parameters.containsKey("prot_seq"));
    assertEquals(1, parameters.get("prot_seq").size());
    assertEquals("1", parameters.get("prot_seq").get(0));
    assertTrue(parameters.containsKey("prot_empai"));
    assertEquals(1, parameters.get("prot_empai").size());
    assertEquals("1", parameters.get("prot_empai").get(0));
  }

  @Test
  public void commandLineParameters_AllTrueMasterFalse() {
    proteinParameters.setMaster(false);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(true);
    proteinParameters.setPi(true);
    proteinParameters.setTaxStr(true);
    proteinParameters.setTaxId(true);
    proteinParameters.setSeq(true);
    proteinParameters.setEmpai(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_Master() {
    proteinParameters.setMaster(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
  }

  @Test
  public void commandLineParameters_Score() {
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_score"));
    assertEquals(1, parameters.get("prot_score").size());
    assertEquals("1", parameters.get("prot_score").get(0));
  }

  @Test
  public void commandLineParameters_Desc() {
    proteinParameters.setMaster(true);
    proteinParameters.setDesc(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_desc"));
    assertEquals(1, parameters.get("prot_desc").size());
    assertEquals("1", parameters.get("prot_desc").get(0));
  }

  @Test
  public void commandLineParameters_Mass() {
    proteinParameters.setMaster(true);
    proteinParameters.setMass(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_mass"));
    assertEquals(1, parameters.get("prot_mass").size());
    assertEquals("1", parameters.get("prot_mass").get(0));
  }

  @Test
  public void commandLineParameters_Matches() {
    proteinParameters.setMaster(true);
    proteinParameters.setMatches(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_matches"));
    assertEquals(1, parameters.get("prot_matches").size());
    assertEquals("1", parameters.get("prot_matches").get(0));
  }

  @Test
  public void commandLineParameters_Cover() {
    proteinParameters.setMaster(true);
    proteinParameters.setCover(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_cover"));
    assertEquals(1, parameters.get("prot_cover").size());
    assertEquals("1", parameters.get("prot_cover").get(0));
  }

  @Test
  public void commandLineParameters_Len() {
    proteinParameters.setMaster(true);
    proteinParameters.setLen(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_len"));
    assertEquals(1, parameters.get("prot_len").size());
    assertEquals("1", parameters.get("prot_len").get(0));
  }

  @Test
  public void commandLineParameters_Pi() {
    proteinParameters.setMaster(true);
    proteinParameters.setPi(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_pi"));
    assertEquals(1, parameters.get("prot_pi").size());
    assertEquals("1", parameters.get("prot_pi").get(0));
  }

  @Test
  public void commandLineParameters_TaxStr() {
    proteinParameters.setMaster(true);
    proteinParameters.setTaxStr(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_tax_str"));
    assertEquals(1, parameters.get("prot_tax_str").size());
    assertEquals("1", parameters.get("prot_tax_str").get(0));
  }

  @Test
  public void commandLineParameters_TaxId() {
    proteinParameters.setMaster(true);
    proteinParameters.setTaxId(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_tax_id"));
    assertEquals(1, parameters.get("prot_tax_id").size());
    assertEquals("1", parameters.get("prot_tax_id").get(0));
  }

  @Test
  public void commandLineParameters_Seq() {
    proteinParameters.setMaster(true);
    proteinParameters.setSeq(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_seq"));
    assertEquals(1, parameters.get("prot_seq").size());
    assertEquals("1", parameters.get("prot_seq").get(0));
  }

  @Test
  public void commandLineParameters_Empai() {
    proteinParameters.setMaster(true);
    proteinParameters.setEmpai(true);

    Map<String, List<String>> parameters = proteinParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("protein_master"));
    assertEquals(1, parameters.get("protein_master").size());
    assertEquals("1", parameters.get("protein_master").get(0));
    assertTrue(parameters.containsKey("prot_empai"));
    assertEquals(1, parameters.get("prot_empai").size());
    assertEquals("1", parameters.get("prot_empai").get(0));
  }
}

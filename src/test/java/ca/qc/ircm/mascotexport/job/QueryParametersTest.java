/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class QueryParametersTest {
  private QueryParameters queryParameters = new QueryParameters();

  @Test
  public void commandLineParameters_AllFalse() {
    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_AllTrue() {
    queryParameters.setMaster(true);
    queryParameters.setTitle(true);
    queryParameters.setQualifiers(true);
    queryParameters.setParams(true);
    queryParameters.setPeaks(true);
    queryParameters.setRaw(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(6, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
    assertTrue(parameters.containsKey("query_title"));
    assertEquals(1, parameters.get("query_title").size());
    assertEquals("1", parameters.get("query_title").get(0));
    assertTrue(parameters.containsKey("query_qualifiers"));
    assertEquals(1, parameters.get("query_qualifiers").size());
    assertEquals("1", parameters.get("query_qualifiers").get(0));
    assertTrue(parameters.containsKey("query_params"));
    assertEquals(1, parameters.get("query_params").size());
    assertEquals("1", parameters.get("query_params").get(0));
    assertTrue(parameters.containsKey("query_peaks"));
    assertEquals(1, parameters.get("query_peaks").size());
    assertEquals("1", parameters.get("query_peaks").get(0));
    assertTrue(parameters.containsKey("query_raw"));
    assertEquals(1, parameters.get("query_raw").size());
    assertEquals("1", parameters.get("query_raw").get(0));
  }

  @Test
  public void commandLineParameters_AllTrueMasterFalse() {
    queryParameters.setMaster(false);
    queryParameters.setTitle(true);
    queryParameters.setQualifiers(true);
    queryParameters.setParams(true);
    queryParameters.setPeaks(true);
    queryParameters.setRaw(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_Master() {
    queryParameters.setMaster(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
  }

  @Test
  public void commandLineParameters_Title() {
    queryParameters.setMaster(true);
    queryParameters.setTitle(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
    assertTrue(parameters.containsKey("query_title"));
    assertEquals(1, parameters.get("query_title").size());
    assertEquals("1", parameters.get("query_title").get(0));
  }

  @Test
  public void commandLineParameters_Qualifiers() {
    queryParameters.setMaster(true);
    queryParameters.setQualifiers(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
    assertTrue(parameters.containsKey("query_qualifiers"));
    assertEquals(1, parameters.get("query_qualifiers").size());
    assertEquals("1", parameters.get("query_qualifiers").get(0));
  }

  @Test
  public void commandLineParameters_Params() {
    queryParameters.setMaster(true);
    queryParameters.setParams(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
    assertTrue(parameters.containsKey("query_params"));
    assertEquals(1, parameters.get("query_params").size());
    assertEquals("1", parameters.get("query_params").get(0));
  }

  @Test
  public void commandLineParameters_Peaks() {
    queryParameters.setMaster(true);
    queryParameters.setPeaks(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
    assertTrue(parameters.containsKey("query_peaks"));
    assertEquals(1, parameters.get("query_peaks").size());
    assertEquals("1", parameters.get("query_peaks").get(0));
  }

  @Test
  public void commandLineParameters_Raw() {
    queryParameters.setMaster(true);
    queryParameters.setRaw(true);

    Map<String, List<String>> parameters = queryParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("query_master"));
    assertEquals(1, parameters.get("query_master").size());
    assertEquals("1", parameters.get("query_master").get(0));
    assertTrue(parameters.containsKey("query_raw"));
    assertEquals(1, parameters.get("query_raw").size());
    assertEquals("1", parameters.get("query_raw").get(0));
  }
}

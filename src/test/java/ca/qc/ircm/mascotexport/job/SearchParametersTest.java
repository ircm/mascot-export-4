/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class SearchParametersTest {
  private SearchParameters searchParameters = new SearchParameters();

  @Test
  public void commandLineParameters_AllFalse() {
    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_AllTrue() {
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(6, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
    assertTrue(parameters.containsKey("show_header"));
    assertEquals(1, parameters.get("show_header").size());
    assertEquals("1", parameters.get("show_header").get(0));
    assertTrue(parameters.containsKey("show_mods"));
    assertEquals(1, parameters.get("show_mods").size());
    assertEquals("1", parameters.get("show_mods").get(0));
    assertTrue(parameters.containsKey("show_params"));
    assertEquals(1, parameters.get("show_params").size());
    assertEquals("1", parameters.get("show_params").get(0));
    assertTrue(parameters.containsKey("show_format"));
    assertEquals(1, parameters.get("show_format").size());
    assertEquals("1", parameters.get("show_format").get(0));
    assertTrue(parameters.containsKey("show_masses"));
    assertEquals(1, parameters.get("show_masses").size());
    assertEquals("1", parameters.get("show_masses").get(0));
  }

  @Test
  public void commandLineParameters_AllTrueMasterFalse() {
    searchParameters.setMaster(false);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(0, parameters.size());
  }

  @Test
  public void commandLineParameters_Master() {
    searchParameters.setMaster(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(1, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
  }

  @Test
  public void commandLineParameters_ShowHeader() {
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
    assertTrue(parameters.containsKey("show_header"));
    assertEquals(1, parameters.get("show_header").size());
    assertEquals("1", parameters.get("show_header").get(0));
  }

  @Test
  public void commandLineParameters_ShowMods() {
    searchParameters.setMaster(true);
    searchParameters.setShowMods(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
    assertTrue(parameters.containsKey("show_mods"));
    assertEquals(1, parameters.get("show_mods").size());
    assertEquals("1", parameters.get("show_mods").get(0));
  }

  @Test
  public void commandLineParameters_ShowParams() {
    searchParameters.setMaster(true);
    searchParameters.setShowParams(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
    assertTrue(parameters.containsKey("show_params"));
    assertEquals(1, parameters.get("show_params").size());
    assertEquals("1", parameters.get("show_params").get(0));
  }

  @Test
  public void commandLineParameters_ShowFormat() {
    searchParameters.setMaster(true);
    searchParameters.setShowFormat(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
    assertTrue(parameters.containsKey("show_format"));
    assertEquals(1, parameters.get("show_format").size());
    assertEquals("1", parameters.get("show_format").get(0));
  }

  @Test
  public void commandLineParameters_ShowMasses() {
    searchParameters.setMaster(true);
    searchParameters.setShowMasses(true);

    Map<String, List<String>> parameters = searchParameters.commandLineParameters();

    assertEquals(2, parameters.size());
    assertTrue(parameters.containsKey("search_master"));
    assertEquals(1, parameters.get("search_master").size());
    assertEquals("1", parameters.get("search_master").get(0));
    assertTrue(parameters.containsKey("show_masses"));
    assertEquals(1, parameters.get("show_masses").size());
    assertEquals("1", parameters.get("show_masses").get(0));
  }
}

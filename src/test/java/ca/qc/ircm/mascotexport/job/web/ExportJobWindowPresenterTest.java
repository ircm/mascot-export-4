/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.ExportJobWindowPresenter.FILE;
import static ca.qc.ircm.mascotexport.job.web.ExportJobWindowPresenter.STYLE;
import static ca.qc.ircm.mascotexport.job.web.ExportJobWindowPresenter.TITLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportJobWindowPresenterTest {
  private ExportJobWindowPresenter presenter;
  @Mock
  private ExportJobWindow view;
  @Mock
  private ExportSearchResultsFormPresenter exportSearchResultsFormPresenter;
  @Mock
  private SearchInformationFormPresenter searchInformationFormPresenter;
  @Mock
  private ProteinHitInformationFormPresenter proteinHitInformationFormPresenter;
  @Mock
  private PeptideMatchInformationFormPresenter peptideMatchInformationFormPresenter;
  @Mock
  private QueryLevelInformationFormPresenter queryLevelInformationFormPresenter;
  @Mock
  private ExportJobWindowDesign content;
  @Mock
  private ExportSearchResultsForm exportSearchResultsForm;
  @Mock
  private SearchInformationForm searchInformationForm;
  @Mock
  private ProteinHitInformationForm proteinHitInformationForm;
  @Mock
  private PeptideMatchInformationForm peptideMatchInformationForm;
  @Mock
  private QueryLevelInformationForm queryLevelInformationForm;
  @Mock
  private ExportJob job;
  @Mock
  private ExportParameters exportParameters;
  @Mock
  private SearchParameters searchParameters;
  @Mock
  private ProteinParameters proteinParameters;
  @Mock
  private PeptideParameters peptideParameters;
  @Mock
  private QueryParameters queryParameters;
  private CheckBox proteinMasterProperty = new CheckBox("proteinMaster", true);
  private CheckBox peptideProteinMasterProperty = new CheckBox("peptideProteinMaster", true);
  private String file = "2017/F012345.dat";
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(ExportJobWindow.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new ExportJobWindowPresenter();
    view.content = content;
    view.content.fileLabel = new Label();
    view.exportSearchResultsForm = exportSearchResultsForm;
    view.searchInformationForm = searchInformationForm;
    view.proteinHitInformationForm = proteinHitInformationForm;
    view.peptideMatchInformationForm = peptideMatchInformationForm;
    view.queryLevelInformationForm = queryLevelInformationForm;
    when(view.proteinHitInformationForm.masterProperty()).thenReturn(proteinMasterProperty);
    when(view.peptideMatchInformationForm.proteinMasterProperty())
        .thenReturn(peptideProteinMasterProperty);
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    when(job.getExportParameters()).thenReturn(exportParameters);
    when(job.getSearchParameters()).thenReturn(searchParameters);
    when(job.getProteinParameters()).thenReturn(proteinParameters);
    when(job.getPeptideParameters()).thenReturn(peptideParameters);
    when(job.getQueryParameters()).thenReturn(queryParameters);
    when(job.getFilePath()).thenReturn(file);
    presenter.init(view);
  }

  @Test
  public void styles() {
    verify(view).addStyleName(STYLE);
    assertTrue(view.content.fileLabel.getStyleName().contains(FILE));
  }

  @Test
  public void captions() {
    verify(view).setCaption(resources.message(TITLE, ""));
    assertEquals(resources.message(FILE), view.content.fileLabel.getCaption());
  }

  @Test
  public void bindProteinMasterProperty() {
    assertTrue(proteinMasterProperty.getValue());
    assertTrue(peptideProteinMasterProperty.getValue());

    proteinMasterProperty.setValue(false);

    assertFalse(proteinMasterProperty.getValue());
    assertFalse(peptideProteinMasterProperty.getValue());

    proteinMasterProperty.setValue(true);

    assertTrue(proteinMasterProperty.getValue());
    assertTrue(peptideProteinMasterProperty.getValue());
  }

  @Test
  public void defaultValues() {
    presenter.setValue(job);

    assertEquals(file, view.content.fileLabel.getValue());
  }

  @Test
  public void readOnly() {
    verify(view.exportSearchResultsForm).setReadOnly(true);
    verify(view.searchInformationForm).setReadOnly(true);
    verify(view.proteinHitInformationForm).setReadOnly(true);
    verify(view.peptideMatchInformationForm).setReadOnly(true);
    verify(view.queryLevelInformationForm).setReadOnly(true);
  }

  @Test
  public void setJob() {
    presenter.setValue(job);

    assertEquals(file, view.content.fileLabel.getValue());
    verify(view).setCaption(resources.message(TITLE, file));
    verify(view.exportSearchResultsForm).setBean(exportParameters);
    verify(view.searchInformationForm).setBean(searchParameters);
    verify(view.proteinHitInformationForm).setBean(proteinParameters);
    verify(view.peptideMatchInformationForm).setBean(peptideParameters);
    verify(view.queryLevelInformationForm).setBean(queryParameters);
  }
}

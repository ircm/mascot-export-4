/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.test.config.AbstractComponentTestCase;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportJobWindowTest extends AbstractComponentTestCase {
  private ExportJobWindow window;
  @Mock
  private ExportJobWindowPresenter presenter;
  @Mock
  private ExportJob job;

  @Before
  public void beforeTest() {
    window = new ExportJobWindow(presenter);
  }

  @Test
  public void setValue() {
    window.setParent(ui);
    window.setValue(job);

    verify(presenter).setValue(job);
  }

  @Test
  public void setValue_BeforeAttach() {
    window.setValue(job);
    window.setParent(ui);

    verify(presenter).setValue(job);
  }

  @Test
  public void setValue_NoAttach() {
    window.setValue(job);

    verify(presenter, never()).setValue(job);
  }
}

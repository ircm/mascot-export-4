/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.ExportProgressWindowPresenter.CANCEL;
import static ca.qc.ircm.mascotexport.job.web.ExportProgressWindowPresenter.STYLE;
import static ca.qc.ircm.mascotexport.job.web.ExportProgressWindowPresenter.TITLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import java.util.Locale;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportProgressWindowPresenterTest {
  @Inject
  @InjectMocks
  private ExportProgressWindowPresenter presenter;
  @Mock
  private ExportProgressWindow view;
  @Mock
  private ClickListener clickListener;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(ExportProgressWindow.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    view.titleLabel = new Label();
    view.messageLabel = new Label();
    view.cancelButton = new Button();
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    presenter.init(view);
  }

  @Test
  public void styles() {
    verify(view).addStyleName(STYLE);
    assertTrue(view.cancelButton.getStyleName().contains(CANCEL));
  }

  @Test
  public void captions() {
    verify(view).setCaption(resources.message(TITLE));
    assertEquals(resources.message(CANCEL), view.cancelButton.getCaption());
  }

  @Test
  public void cancel() {
    view.cancelButton.addClickListener(clickListener);

    presenter.cancel();

    verify(clickListener).buttonClick(any());
  }

  @Test
  public void addCancelledClickListener() {
    presenter.addCancelledClickListener(clickListener);

    view.cancelButton.click();
    verify(clickListener).buttonClick(any());
  }

  @Test
  public void getTitle() {
    assertNull("", presenter.getTitle());

    String newTitle = "new title";
    presenter.setTitle(newTitle);
    assertEquals(newTitle, presenter.getTitle());
  }

  @Test
  public void setTitle() {
    String title = "new title";

    presenter.setTitle(title);

    assertEquals(title, title);
  }

  @Test
  public void getMessage() {
    assertNull("", presenter.getMessage());

    String newMessage = "new message";
    presenter.setMessage(newMessage);
    assertEquals(newMessage, presenter.getMessage());
  }

  @Test
  public void setMessage() {
    String message = "new message";

    presenter.setMessage(message);

    assertEquals(message, message);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBarWithListeners;
import ca.qc.ircm.progressbar.Property;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import java.util.function.BiConsumer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportProgressWindowProgressBarListenerTest {
  private ExportProgressWindowProgressBarListeners exportProgressWindowProgressBarListeners;
  @Mock
  private ExportProgressWindow window;
  @Mock
  private UI ui;
  @Mock
  private ProgressBarWithListeners progressBar;
  @Captor
  private ArgumentCaptor<Runnable> runnableCaptor;
  @Captor
  private ArgumentCaptor<BiConsumer<String, String>> stringListenerCaptor;
  @Captor
  private ArgumentCaptor<BiConsumer<Double, Double>> numberListenerCaptor;
  private Property<String> titleProperty = new Property<>("");
  private Property<String> messageProperty = new Property<>("");
  private Property<Double> progressProperty = new Property<>(0.0);

  /**
   * Configuration before test.
   */
  @Before
  public void beforeTest() {
    exportProgressWindowProgressBarListeners =
        new ExportProgressWindowProgressBarListeners(ui, window);
    when(ui.isAttached()).thenReturn(true);
    when(progressBar.title()).thenReturn(titleProperty);
    when(progressBar.message()).thenReturn(messageProperty);
    when(progressBar.progress()).thenReturn(progressProperty);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void addValueChangeListeners() {
    Property<String> titleProperty = mock(Property.class);
    Property<String> messageProperty = mock(Property.class);
    Property<Double> progressProperty = mock(Property.class);
    when(progressBar.title()).thenReturn(titleProperty);
    when(progressBar.message()).thenReturn(messageProperty);
    when(progressBar.progress()).thenReturn(progressProperty);

    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    verify(progressBar).title();
    verify(titleProperty).addListener(stringListenerCaptor.capture());
    verify(progressBar).message();
    verify(messageProperty).addListener(stringListenerCaptor.capture());
    verify(progressBar).progress();
    verify(progressProperty).addListener(numberListenerCaptor.capture());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void removeValueChangeListeners() {
    Property<String> titleProperty = mock(Property.class);
    Property<String> messageProperty = mock(Property.class);
    Property<Double> progressProperty = mock(Property.class);
    when(progressBar.title()).thenReturn(titleProperty);
    when(progressBar.message()).thenReturn(messageProperty);
    when(progressBar.progress()).thenReturn(progressProperty);
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);
    verify(titleProperty).addListener(stringListenerCaptor.capture());
    final BiConsumer<String, String> titleListener = stringListenerCaptor.getValue();
    verify(messageProperty).addListener(stringListenerCaptor.capture());
    final BiConsumer<String, String> messageListener = stringListenerCaptor.getValue();
    verify(progressProperty).addListener(numberListenerCaptor.capture());
    final BiConsumer<Double, Double> progressListener = numberListenerCaptor.getValue();

    exportProgressWindowProgressBarListeners.removeValueChangeListeners(progressBar);

    verify(progressBar, times(2)).title();
    verify(titleProperty).removeListener(titleListener);
    verify(progressBar, times(2)).message();
    verify(messageProperty).removeListener(messageListener);
    verify(progressBar, times(2)).progress();
    verify(progressProperty).removeListener(progressListener);
  }

  @Test
  public void titleChanged() {
    String newTitle = "new title";
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    titleProperty.set(newTitle);

    verify(ui).access(runnableCaptor.capture());
    verify(window, never()).setTitle(any());
    runnableCaptor.getValue().run();
    verify(window).setTitle(newTitle);
  }

  @Test
  public void titleChanged_NotAttached() {
    when(ui.isAttached()).thenReturn(false);
    String newTitle = "new title";
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    titleProperty.set(newTitle);

    verify(ui, never()).access(any());
  }

  @Test
  public void titleChanged_UiDetachedException() {
    when(ui.access(any())).thenThrow(new UIDetachedException());
    String newTitle = "new title";
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    titleProperty.set(newTitle);

    verify(ui).access(runnableCaptor.capture());
    verify(window, never()).setTitle(any());
  }

  @Test
  public void messageChanged() {
    String newMessage = "new message";
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    messageProperty.set(newMessage);

    verify(ui).access(runnableCaptor.capture());
    verify(window, never()).setTitle(any());
    runnableCaptor.getValue().run();
    verify(window).setMessage(newMessage);
  }

  @Test
  public void messageChanged_NotAttached() {
    when(ui.isAttached()).thenReturn(false);
    String newMessage = "new message";
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    messageProperty.set(newMessage);

    verify(ui, never()).access(runnableCaptor.capture());
    verify(window, never()).setTitle(any());
  }

  @Test
  public void messageChanged_UiDetachedException() {
    when(ui.access(any())).thenThrow(new UIDetachedException());
    String newMessage = "new message";
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    messageProperty.set(newMessage);

    verify(ui).access(runnableCaptor.capture());
    verify(window, never()).setTitle(any());
  }

  @Test
  public void progressChanged() {
    Double newProgress = 0.4;
    exportProgressWindowProgressBarListeners.addValueChangeListeners(progressBar);

    progressProperty.set(newProgress);

    // Nothing should happen.
  }
}

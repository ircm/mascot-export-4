/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button.ClickListener;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportProgressWindowTest {
  private ExportProgressWindow window;
  @Mock
  private ExportProgressWindowPresenter presenter;
  @Mock
  private ClickListener listener;
  @Mock
  private Registration registration;
  private final String title = "test title";
  private final String message = "test message";

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    window = new ExportProgressWindow(presenter);
  }

  @Test
  public void addCancelledClickListener() {
    when(presenter.addCancelledClickListener(any())).thenReturn(registration);

    Registration registration = window.addCancelledClickListener(listener);

    verify(presenter).addCancelledClickListener(listener);
    assertSame(this.registration, registration);
  }

  @Test
  public void getTitle() {
    when(presenter.getTitle()).thenReturn(title);

    String title = window.getTitle();

    verify(presenter).getTitle();
    assertSame(this.title, title);
  }

  @Test
  public void setTitle() {
    window.setTitle(title);

    verify(presenter).setTitle(title);
  }

  @Test
  public void getMessage() {
    when(presenter.getMessage()).thenReturn(message);

    String message = window.getMessage();

    verify(presenter).getMessage();
    assertSame(this.message, message);
  }

  @Test
  public void setMessage() {
    window.setMessage(message);

    verify(presenter).setMessage(message);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportFormat.XML;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.EXPORT_FORMAT;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.GROUP_FAMILY;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.IGNORE_ION_SCORE_BELOW;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.PREFER_TAXONOMY;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.REPORT;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.REQUIRE_BOLD_RED;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SERVER_MUDPIT_SWITCH;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SHOW_SAME_SETS;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SHOW_SUBSETS;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SIG_THRESHOLD;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.USE_HOMOLOGY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.BindingValidationStatus;
import com.vaadin.data.HasValue;
import java.util.Locale;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportSearchResultsFormPresenterTest {
  private ExportSearchResultsFormPresenter presenter;
  @Mock
  private ExportSearchResultsForm view;
  private ExportSearchResultsFormDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(ExportSearchResultsForm.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new ExportSearchResultsFormPresenter();
    design = new ExportSearchResultsFormDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
  }

  private Optional<BindingValidationStatus<?>> optionalError(HasValue<?> field) {
    return presenter.validate().getFieldValidationErrors().stream()
        .filter(error -> ((BindingValidationStatus<?>) error).getField() == field).findFirst();
  }

  @Test
  public void enabledFields() {
    presenter.init(view);

    assertTrue(design.exportFormat.isEnabled());
    assertTrue(design.sigThreshold.isEnabled());
    assertTrue(design.ignoreIonsScoreBelow.isEnabled());
    assertTrue(design.useHomology.isEnabled());
    assertTrue(design.report.isEnabled());
    assertTrue(design.serverMudpitSwitch.isEnabled());
    assertTrue(design.showSameSets.isEnabled());
    assertTrue(design.showSubsets.isEnabled());
    assertTrue(design.groupFamily.isEnabled());
    assertTrue(design.requireBoldRed.isEnabled());
    assertFalse(design.preferTaxonomy.isEnabled());
  }

  @Test
  public void defaultValues() {
    presenter.init(view);

    assertEquals(CSV, design.exportFormat.getValue());
    assertEquals("0.05", design.sigThreshold.getValue());
    assertEquals(0.05, Double.parseDouble(design.sigThreshold.getValue()), 0.001);
    assertEquals("15", design.ignoreIonsScoreBelow.getValue());
    assertEquals(15, Integer.parseInt(design.ignoreIonsScoreBelow.getValue()));
    assertEquals(false, design.useHomology.getValue());
    assertEquals("200", design.report.getValue());
    assertEquals(200, Integer.parseInt(design.report.getValue()));
    assertEquals(ProteinScoring.STANDARD, design.serverMudpitSwitch.getValue());
    assertEquals(true, design.showSameSets.getValue());
    assertEquals("1", design.showSubsets.getValue());
    assertEquals(1, Integer.parseInt(design.showSubsets.getValue()));
    assertEquals(false, design.groupFamily.getValue());
    assertEquals(true, design.requireBoldRed.getValue());
    assertEquals("", design.preferTaxonomy.getValue());
  }

  @Test
  public void captions() {
    presenter.init(view);

    assertEquals(resources.message(EXPORT_FORMAT), design.exportFormat.getCaption());
    assertEquals(resources.message(SIG_THRESHOLD), design.sigThreshold.getCaption());
    assertEquals(resources.message(IGNORE_ION_SCORE_BELOW),
        design.ignoreIonsScoreBelow.getCaption());
    assertEquals(resources.message(USE_HOMOLOGY), design.useHomology.getCaption());
    assertEquals(resources.message(USE_HOMOLOGY + "." + false),
        design.useHomology.getItemCaptionGenerator().apply(false));
    assertEquals(resources.message(USE_HOMOLOGY + "." + true),
        design.useHomology.getItemCaptionGenerator().apply(true));
    assertEquals(resources.message(REPORT), design.report.getCaption());
    assertEquals(resources.message(SERVER_MUDPIT_SWITCH), design.serverMudpitSwitch.getCaption());
    for (ProteinScoring value : ProteinScoring.values()) {
      assertEquals(resources.message(SERVER_MUDPIT_SWITCH + "." + value.name()),
          design.serverMudpitSwitch.getItemCaptionGenerator().apply(value));
    }
    assertEquals(resources.message(SHOW_SAME_SETS), design.showSameSets.getCaption());
    assertEquals(resources.message(SHOW_SUBSETS), design.showSubsets.getCaption());
    assertEquals(resources.message(GROUP_FAMILY), design.groupFamily.getCaption());
    assertEquals(resources.message(REQUIRE_BOLD_RED), design.requireBoldRed.getCaption());
    assertEquals(resources.message(PREFER_TAXONOMY), design.preferTaxonomy.getCaption());
    assertEquals(resources.message(PREFER_TAXONOMY + ".null"),
        design.preferTaxonomy.getItemCaptionGenerator().apply(null));
  }

  @Test
  public void groupFamilyListener_True() {
    presenter.init(view);
    design.groupFamily.setValue(false);
    design.groupFamily.setValue(true);

    assertFalse(design.requireBoldRed.getValue());
    assertFalse(design.requireBoldRed.isEnabled());
  }

  @Test
  public void groupFamilyListener_False() {
    presenter.init(view);
    design.groupFamily.setValue(true);
    design.groupFamily.setValue(false);

    assertFalse(design.requireBoldRed.getValue());
    assertTrue(design.requireBoldRed.isEnabled());
  }

  @Test
  public void isReadOnly_Default() {
    presenter.init(view);

    assertFalse(presenter.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    presenter.init(view);
    presenter.setReadOnly(true);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.exportFormat.isReadOnly());
    assertTrue(design.sigThreshold.isReadOnly());
    assertTrue(design.ignoreIonsScoreBelow.isReadOnly());
    assertTrue(design.useHomology.isReadOnly());
    assertTrue(design.report.isReadOnly());
    assertTrue(design.serverMudpitSwitch.isReadOnly());
    assertTrue(design.showSameSets.isReadOnly());
    assertTrue(design.showSubsets.isReadOnly());
    assertTrue(design.groupFamily.isReadOnly());
    assertTrue(design.requireBoldRed.isReadOnly());
    assertTrue(design.preferTaxonomy.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    presenter.init(view);
    presenter.setReadOnly(false);

    assertFalse(presenter.isReadOnly());
    assertFalse(design.exportFormat.isReadOnly());
    assertFalse(design.sigThreshold.isReadOnly());
    assertFalse(design.ignoreIonsScoreBelow.isReadOnly());
    assertFalse(design.useHomology.isReadOnly());
    assertFalse(design.report.isReadOnly());
    assertFalse(design.serverMudpitSwitch.isReadOnly());
    assertFalse(design.showSameSets.isReadOnly());
    assertFalse(design.showSubsets.isReadOnly());
    assertFalse(design.groupFamily.isReadOnly());
    assertFalse(design.requireBoldRed.isReadOnly());
    assertFalse(design.preferTaxonomy.isReadOnly());
  }

  @Test
  public void setReadOnly_BeforeInit() {
    presenter.setReadOnly(true);
    presenter.init(view);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.exportFormat.isReadOnly());
    assertTrue(design.sigThreshold.isReadOnly());
    assertTrue(design.ignoreIonsScoreBelow.isReadOnly());
    assertTrue(design.useHomology.isReadOnly());
    assertTrue(design.report.isReadOnly());
    assertTrue(design.serverMudpitSwitch.isReadOnly());
    assertTrue(design.showSameSets.isReadOnly());
    assertTrue(design.showSubsets.isReadOnly());
    assertTrue(design.groupFamily.isReadOnly());
    assertTrue(design.requireBoldRed.isReadOnly());
    assertTrue(design.preferTaxonomy.isReadOnly());
  }

  @Test
  public void isValid_Defaults() {
    presenter.init(view);

    assertTrue(presenter.isValid());
  }

  @Test
  public void exportFormat_Null() {
    presenter.init(view);
    design.exportFormat.setValue(null);

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.exportFormat);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(EXPORT_FORMAT + ".empty"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void sigThreshold_Invalid() {
    presenter.init(view);
    design.sigThreshold.setValue("a");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.sigThreshold);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(SIG_THRESHOLD + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void ignoreIonsScoreBelow_Invalid() {
    presenter.init(view);
    design.ignoreIonsScoreBelow.setValue("a");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.ignoreIonsScoreBelow);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(IGNORE_ION_SCORE_BELOW + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void ignoreIonsScoreBelow_NotInteger() {
    presenter.init(view);
    design.ignoreIonsScoreBelow.setValue("1.2");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.ignoreIonsScoreBelow);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(IGNORE_ION_SCORE_BELOW + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void report_Invalid() {
    presenter.init(view);
    design.report.setValue("a");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.report);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(REPORT + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void report_NotInteger() {
    presenter.init(view);
    design.report.setValue("1.2");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.report);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(REPORT + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void showSubsets_Invalid() {
    presenter.init(view);
    design.showSubsets.setValue("a");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.showSubsets);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(SHOW_SUBSETS + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void showSubsets_NotInteger() {
    presenter.init(view);
    design.showSubsets.setValue("1.2");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.showSubsets);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(SHOW_SUBSETS + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void preferTaxonomy_Invalid() {
    presenter.init(view);
    design.preferTaxonomy.setValue("a");

    assertFalse(presenter.isValid());
    Optional<BindingValidationStatus<?>> optionalError = optionalError(design.preferTaxonomy);
    assertTrue(optionalError.isPresent());
    assertEquals(resources.message(PREFER_TAXONOMY + ".invalid"),
        optionalError.get().getMessage().orElse(""));
  }

  @Test
  public void getBean_1() {
    presenter.init(view);
    design.exportFormat.setValue(CSV);
    design.sigThreshold.setValue("0.05");
    design.ignoreIonsScoreBelow.setValue("15");
    design.useHomology.setValue(false);
    design.report.setValue("200");
    design.serverMudpitSwitch.setValue(ProteinScoring.STANDARD);
    design.showSameSets.setValue(true);
    design.showSubsets.setValue("1");
    design.groupFamily.setValue(false);
    design.requireBoldRed.setValue(true);
    design.preferTaxonomy.setValue(null);

    ExportParameters parameters = presenter.getBean();

    assertEquals(CSV, parameters.getExportFormat());
    assertEquals(0.05, parameters.getSigThreshold(), 0.0001);
    assertEquals((Integer) 15, parameters.getIgnoreIonsScoreBelow());
    assertFalse(parameters.isUseHomology());
    assertEquals((Integer) 200, parameters.getReport());
    assertEquals(ProteinScoring.STANDARD, parameters.getServerMudpitSwitch());
    assertTrue(parameters.isShowSameSets());
    assertEquals((Integer) 1, parameters.getShowSubsets());
    assertFalse(parameters.isGroupFamily());
    assertTrue(parameters.isRequireBoldRed());
    assertNull(parameters.getPreferTaxonomy());
  }

  @Test
  public void getBean_2() {
    presenter.init(view);
    design.exportFormat.setValue(XML);
    design.sigThreshold.setValue("0.1");
    design.ignoreIonsScoreBelow.setValue("20");
    design.useHomology.setValue(true);
    design.report.setValue("100");
    design.serverMudpitSwitch.setValue(ProteinScoring.MUDPIT);
    design.showSameSets.setValue(false);
    design.showSubsets.setValue("2");
    design.groupFamily.setValue(false);
    design.requireBoldRed.setValue(false);
    design.preferTaxonomy.setValue(null);

    ExportParameters parameters = presenter.getBean();

    assertEquals(XML, parameters.getExportFormat());
    assertEquals(0.1, parameters.getSigThreshold(), 0.0001);
    assertEquals((Integer) 20, parameters.getIgnoreIonsScoreBelow());
    assertTrue(parameters.isUseHomology());
    assertEquals((Integer) 100, parameters.getReport());
    assertEquals(ProteinScoring.MUDPIT, parameters.getServerMudpitSwitch());
    assertFalse(parameters.isShowSameSets());
    assertEquals((Integer) 2, parameters.getShowSubsets());
    assertFalse(parameters.isGroupFamily());
    assertFalse(parameters.isRequireBoldRed());
    assertNull(parameters.getPreferTaxonomy());
  }

  @Test
  public void getBean_3() {
    presenter.init(view);
    design.exportFormat.setValue(XML);
    design.sigThreshold.setValue("0.1");
    design.ignoreIonsScoreBelow.setValue("20");
    design.useHomology.setValue(true);
    design.report.setValue("100");
    design.serverMudpitSwitch.setValue(ProteinScoring.MUDPIT);
    design.showSameSets.setValue(false);
    design.showSubsets.setValue("2");
    design.groupFamily.setValue(true);
    design.requireBoldRed.setValue(false);
    design.preferTaxonomy.setValue(null);

    ExportParameters parameters = presenter.getBean();

    assertEquals(XML, parameters.getExportFormat());
    assertEquals(0.1, parameters.getSigThreshold(), 0.0001);
    assertEquals((Integer) 20, parameters.getIgnoreIonsScoreBelow());
    assertTrue(parameters.isUseHomology());
    assertEquals((Integer) 100, parameters.getReport());
    assertEquals(ProteinScoring.MUDPIT, parameters.getServerMudpitSwitch());
    assertFalse(parameters.isShowSameSets());
    assertEquals((Integer) 2, parameters.getShowSubsets());
    assertTrue(parameters.isGroupFamily());
    assertFalse(parameters.isRequireBoldRed());
    assertNull(parameters.getPreferTaxonomy());
  }

  @Test
  public void setBean() {
    presenter.init(view);
    ExportParameters parameters = new ExportParameters();
    parameters.setExportFormat(XML);
    parameters.setSigThreshold(0.1);
    parameters.setIgnoreIonsScoreBelow(20);
    parameters.setUseHomology(true);
    parameters.setReport(20);
    parameters.setServerMudpitSwitch(ProteinScoring.MUDPIT);
    parameters.setShowSameSets(false);
    parameters.setShowSubsets(2);
    parameters.setGroupFamily(true);
    parameters.setRequireBoldRed(false);

    presenter.setBean(parameters);

    assertEquals(XML, design.exportFormat.getValue());
    assertEquals("0.1", design.sigThreshold.getValue());
    assertEquals(0.1, Double.parseDouble(design.sigThreshold.getValue()), 0.001);
    assertEquals("20", design.ignoreIonsScoreBelow.getValue());
    assertEquals(20, Integer.parseInt(design.ignoreIonsScoreBelow.getValue()));
    assertEquals(true, design.useHomology.getValue());
    assertEquals("20", design.report.getValue());
    assertEquals(20, Integer.parseInt(design.report.getValue()));
    assertEquals(ProteinScoring.MUDPIT, design.serverMudpitSwitch.getValue());
    assertEquals(false, design.showSameSets.getValue());
    assertEquals("2", design.showSubsets.getValue());
    assertEquals(2, Integer.parseInt(design.showSubsets.getValue()));
    assertEquals(true, design.groupFamily.getValue());
    assertEquals(false, design.requireBoldRed.getValue());
    assertEquals("", design.preferTaxonomy.getValue());
  }

  @Test
  public void setBean_BeforeInit() {
    ExportParameters parameters = new ExportParameters();
    parameters.setExportFormat(XML);
    parameters.setSigThreshold(0.1);
    parameters.setIgnoreIonsScoreBelow(20);
    parameters.setUseHomology(true);
    parameters.setReport(20);
    parameters.setServerMudpitSwitch(ProteinScoring.MUDPIT);
    parameters.setShowSameSets(false);
    parameters.setShowSubsets(2);
    parameters.setGroupFamily(true);
    parameters.setRequireBoldRed(false);

    presenter.setBean(parameters);
    presenter.init(view);

    assertEquals(XML, design.exportFormat.getValue());
    assertEquals("0.1", design.sigThreshold.getValue());
    assertEquals(0.1, Double.parseDouble(design.sigThreshold.getValue()), 0.001);
    assertEquals("20", design.ignoreIonsScoreBelow.getValue());
    assertEquals(20, Integer.parseInt(design.ignoreIonsScoreBelow.getValue()));
    assertEquals(true, design.useHomology.getValue());
    assertEquals("20", design.report.getValue());
    assertEquals(20, Integer.parseInt(design.report.getValue()));
    assertEquals(ProteinScoring.MUDPIT, design.serverMudpitSwitch.getValue());
    assertEquals(false, design.showSameSets.getValue());
    assertEquals("2", design.showSubsets.getValue());
    assertEquals(2, Integer.parseInt(design.showSubsets.getValue()));
    assertEquals(true, design.groupFamily.getValue());
    assertEquals(false, design.requireBoldRed.getValue());
    assertEquals("", design.preferTaxonomy.getValue());
  }

  @Test
  public void writeBean() throws Throwable {
    presenter.init(view);
    ExportParameters parameters = new ExportParameters();

    presenter.writeBean(parameters);

    assertEquals(CSV, parameters.getExportFormat());
    assertEquals(0.05, parameters.getSigThreshold(), 0.0001);
    assertEquals((Integer) 15, parameters.getIgnoreIonsScoreBelow());
    assertFalse(parameters.isUseHomology());
    assertEquals((Integer) 200, parameters.getReport());
    assertEquals(ProteinScoring.STANDARD, parameters.getServerMudpitSwitch());
    assertTrue(parameters.isShowSameSets());
    assertEquals((Integer) 1, parameters.getShowSubsets());
    assertFalse(parameters.isGroupFamily());
    assertTrue(parameters.isRequireBoldRed());
    assertNull(parameters.getPreferTaxonomy());
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.EXPORT_FORMAT;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.GROUP_FAMILY;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.IGNORE_ION_SCORE_BELOW;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.REPORT;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.REQUIRE_BOLD_RED;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SERVER_MUDPIT_SWITCH;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SHOW_SAME_SETS;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SHOW_SUBSETS;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.SIG_THRESHOLD;
import static ca.qc.ircm.mascotexport.job.web.ExportSearchResultsFormPresenter.USE_HOMOLOGY;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEPTIDE_MASTER;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_CALC_MR;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_DELTA;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_END;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_EXPECT;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_EXP_MR;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_EXP_Z;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_FRAME;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_HOMOL;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_IDENT;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_MISS;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_NUM_MATCH;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_SCAN_TITLE;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_SCORE;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_SEQ;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_START;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_VAR_MOD;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.SHOW_PEP_DUPES;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.SHOW_UNASSIGNED;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROTEIN_MASTER;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_COVER;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_DESC;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_EMPAI;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_LEN;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_MASS;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_MATCHES;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_PI;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_SCORE;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_SEQ;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_TAX_ID;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_TAX_STR;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_MASTER;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_PARAMS;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_PEAKS;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_QUALIFIERS;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_RAW;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_TITLE;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SEARCH_MASTER;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_HEADER;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_MASSES;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_MODS;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_PARAMS;
import static com.vaadin.testbench.By.vaadin;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.tagName;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelection;
import ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionWindowPresenter;
import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.testbench.elements.CheckBoxElement;
import com.vaadin.testbench.elements.ComboBoxElement;
import com.vaadin.testbench.elements.GridElement;
import com.vaadin.testbench.elements.GridElement.GridCellElement;
import com.vaadin.testbench.elements.RadioButtonGroupElement;
import com.vaadin.testbench.elements.TextFieldElement;
import com.vaadin.testbench.elements.WindowElement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public abstract class ExportViewPageObject extends AbstractTestBenchTestCase {
  private static final int JOB_FILE_COLUMN = 1;
  private static final int JOB_DATE_COLUMN = 3;

  protected void open() {
    openView(ExportView.VIEW_NAME);
  }

  private void iteratorRows(GridElement grid, Consumer<Integer> consumer) {
    int rowIndex = 0;
    try {
      while (true) {
        grid.getRow(rowIndex);
        consumer.accept(rowIndex);
        rowIndex++;
      }
    } catch (NoSuchElementException exception) {
      // No more rows.
    }
  }

  /**
   * Select files.
   *
   * @param rows
   *          rows to select
   */
  public void selectFiles(int... rows) {
    findElement(className(ExportViewPresenter.ADD_FILES_STYLE)).click();
    WindowElement addFilesWindow = wrap(WindowElement.class,
        findElement(className(MascotFilesSelectionWindowPresenter.STYLE)));
    GridElement grid = addFilesWindow.$(GridElement.class).first();
    for (int row : rows) {
      grid.getRow(row).getCell(0).click();
    }
    addFilesWindow.findElement(className(MascotFilesSelection.SELECT_STYLE)).click();
  }

  /**
   * Returns selected files.
   *
   * @return selected files
   */
  public List<String> getSelectedFiles() {
    GridElement files =
        wrap(GridElement.class, findElement(className(ExportViewPresenter.FILES_STYLE)));
    List<String> selectedFiles = new ArrayList<>();
    int row = 0;
    try {
      while (true) {
        GridCellElement cell = files.getCell(row++, 1);
        selectedFiles.add(cell.getText());
      }
    } catch (NoSuchElementException exception) {
      // No more rows.
    }
    return selectedFiles;
  }

  private WebElement generalParameter(String style) {
    return findElement(className(ExportSearchResultsFormPresenter.STYLE))
        .findElement(className(style));
  }

  private WebElement searchParameter(String style) {
    return findElement(className(SearchInformationFormPresenter.STYLE))
        .findElement(className(style));
  }

  private WebElement proteinParameter(String style) {
    return findElement(className(ProteinHitInformationFormPresenter.STYLE))
        .findElement(className(style));
  }

  private WebElement peptideParameter(String style) {
    return findElement(className(PeptideMatchInformationFormPresenter.STYLE))
        .findElement(className(style));
  }

  private WebElement queryParameter(String style) {
    return findElement(className(QueryLevelInformationFormPresenter.STYLE))
        .findElement(className(style));
  }

  private ComboBoxElement exportFormatField() {
    return wrap(ComboBoxElement.class, generalParameter(EXPORT_FORMAT));
  }

  public String getExportFormat() {
    return exportFormatField().getValue();
  }

  /**
   * Sets export format.<br>
   * Export format must be CSV or XML or field will not change.
   *
   * @param value
   *          export format
   */
  public void setExportFormat(String value) {
    ComboBoxElement field = exportFormatField();
    field.openPopup();
    WebElement popup = field.findElement(vaadin("#popup"));
    Optional<WebElement> valueField =
        popup.findElements(tagName("td")).stream().map(td -> td.findElement(tagName("span")))
            .filter(span -> value.equals(span.getText())).findFirst();
    if (valueField.isPresent()) {
      valueField.get().click();
    }
  }

  private TextFieldElement sigThresholdField() {
    return wrap(TextFieldElement.class, generalParameter(SIG_THRESHOLD));
  }

  public String getSigThreshold() {
    return sigThresholdField().getValue();
  }

  /**
   * Sets significance threshold.<br>
   * Significance threshold should be a double.
   *
   * @param value
   *          significance threshold
   */
  public void setSigThreshold(String value) {
    sigThresholdField().setValue(value);
  }

  private TextFieldElement ignoreIonsScoreField() {
    return wrap(TextFieldElement.class, generalParameter(IGNORE_ION_SCORE_BELOW));
  }

  public String getIgnoreIonsScoreBelow() {
    return ignoreIonsScoreField().getValue();
  }

  public void setIgnoreIonsScoreBelow(String value) {
    ignoreIonsScoreField().setValue(value);
  }

  private RadioButtonGroupElement useHomologyField() {
    return wrap(RadioButtonGroupElement.class, generalParameter(USE_HOMOLOGY));
  }

  private String useHomologyText(boolean value) {
    return new MessageResource(ExportSearchResultsForm.class, Locale.getDefault())
        .message(USE_HOMOLOGY + "." + value);
  }

  public boolean getUseHomology() {
    String text = useHomologyField().getValue();
    return useHomologyText(true).equals(text) ? true : false;
  }

  /**
   * Sets use homology.
   *
   * @param value
   *          use homology
   */
  public void setUseHomology(boolean value) {
    useHomologyField().setValue(useHomologyText(value));
  }

  private TextFieldElement reportField() {
    return wrap(TextFieldElement.class, generalParameter(REPORT));
  }

  public String getReport() {
    return reportField().getValue();
  }

  /**
   * Sets report.<br>
   * Report should be an integer.
   *
   * @param value
   *          report
   */
  public void setReport(String value) {
    reportField().setValue(value);
  }

  private RadioButtonGroupElement serverMudpitSwitchField() {
    return wrap(RadioButtonGroupElement.class, generalParameter(SERVER_MUDPIT_SWITCH));
  }

  private String serverMudpitSwitchText(ProteinScoring value) {
    return new MessageResource(ExportSearchResultsForm.class, Locale.getDefault())
        .message(SERVER_MUDPIT_SWITCH + "." + value);
  }

  /**
   * Returns server mudpit switch.
   *
   * @return server mudpit switch
   */
  public ProteinScoring getServerMudpitSwitch() {
    String text = serverMudpitSwitchField().getValue();
    ProteinScoring ret = null;
    for (ProteinScoring proteinScoring : ProteinScoring.values()) {
      if (serverMudpitSwitchText(proteinScoring).equals(text)) {
        ret = proteinScoring;
      }
    }
    return ret;
  }

  public void setServerMudpitSwitch(ProteinScoring value) {
    serverMudpitSwitchField().setValue(serverMudpitSwitchText(value));
  }

  private CheckBoxElement showSameSetsField() {
    return wrap(CheckBoxElement.class, generalParameter(SHOW_SAME_SETS));
  }

  public boolean getShowSameSets() {
    return showSameSetsField().isChecked();
  }

  public void setShowSameSets(boolean value) {
    setCheckBoxValue(showSameSetsField(), value);
  }

  private TextFieldElement showSubsetsField() {
    return wrap(TextFieldElement.class, generalParameter(SHOW_SUBSETS));
  }

  public String getShowSubsets() {
    return showSubsetsField().getValue();
  }

  /**
   * Sets show subsets.<br>
   * Show subsets should be an integer.
   *
   * @param value
   *          show subsets
   */
  public void setShowSubsets(String value) {
    showSubsetsField().setValue(value);
  }

  private CheckBoxElement groupFamilyField() {
    return wrap(CheckBoxElement.class, generalParameter(GROUP_FAMILY));
  }

  public boolean getGroupFamily() {
    return groupFamilyField().isChecked();
  }

  public void setGroupFamily(boolean value) {
    setCheckBoxValue(groupFamilyField(), value);
  }

  private CheckBoxElement requireBoldRedField() {
    return wrap(CheckBoxElement.class, generalParameter(REQUIRE_BOLD_RED));
  }

  public boolean getRequireBoldRed() {
    return requireBoldRedField().isChecked();
  }

  public void setRequireBoldRed(boolean value) {
    setCheckBoxValue(requireBoldRedField(), value);
  }

  private CheckBoxElement searchMasterField() {
    return wrap(CheckBoxElement.class, searchParameter(SEARCH_MASTER));
  }

  public boolean getSearchMaster() {
    return searchMasterField().isChecked();
  }

  public void setSearchMaster(boolean value) {
    setCheckBoxValue(searchMasterField(), value);
  }

  private CheckBoxElement showHeaderField() {
    return wrap(CheckBoxElement.class, searchParameter(SHOW_HEADER));
  }

  public boolean getShowHeader() {
    return showHeaderField().isChecked();
  }

  public void setShowHeader(boolean value) {
    setCheckBoxValue(showHeaderField(), value);
  }

  private CheckBoxElement showModsField() {
    return wrap(CheckBoxElement.class, searchParameter(SHOW_MODS));
  }

  public boolean getShowMods() {
    return showModsField().isChecked();
  }

  public void setShowMods(boolean value) {
    setCheckBoxValue(showModsField(), value);
  }

  private CheckBoxElement showParamsField() {
    return wrap(CheckBoxElement.class, searchParameter(SHOW_PARAMS));
  }

  public boolean getShowParams() {
    return showParamsField().isChecked();
  }

  public void setShowParams(boolean value) {
    setCheckBoxValue(showParamsField(), value);
  }

  private CheckBoxElement showFormatField() {
    return wrap(CheckBoxElement.class, searchParameter(SHOW_PARAMS));
  }

  public boolean getShowFormat() {
    return showFormatField().isChecked();
  }

  public void setShowFormat(boolean value) {
    setCheckBoxValue(showFormatField(), value);
  }

  private CheckBoxElement showMassesField() {
    return wrap(CheckBoxElement.class, searchParameter(SHOW_MASSES));
  }

  public boolean getShowMasses() {
    return showMassesField().isChecked();
  }

  public void setShowMasses(boolean value) {
    setCheckBoxValue(showMassesField(), value);
  }

  private CheckBoxElement proteinMasterField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROTEIN_MASTER));
  }

  public boolean getProteinMaster() {
    return proteinMasterField().isChecked();
  }

  public void setProteinMaster(boolean value) {
    setCheckBoxValue(proteinMasterField(), value);
  }

  private CheckBoxElement proteinScoreField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_SCORE));
  }

  public boolean getProteinScore() {
    return proteinScoreField().isChecked();
  }

  public void setProteinScore(boolean value) {
    setCheckBoxValue(proteinScoreField(), value);
  }

  private CheckBoxElement proteinDescField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_DESC));
  }

  public boolean getProteinDesc() {
    return proteinDescField().isChecked();
  }

  public void setProteinDesc(boolean value) {
    setCheckBoxValue(proteinDescField(), value);
  }

  private CheckBoxElement proteinMassField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_MASS));
  }

  public boolean getProteinMass() {
    return proteinMassField().isChecked();
  }

  public void setProteinMass(boolean value) {
    setCheckBoxValue(proteinMassField(), value);
  }

  private CheckBoxElement proteinMatchesField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_MATCHES));
  }

  public boolean getProteinMatches() {
    return proteinMatchesField().isChecked();
  }

  public void setProteinMatches(boolean value) {
    setCheckBoxValue(proteinMatchesField(), value);
  }

  private CheckBoxElement proteinCoverField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_COVER));
  }

  public boolean getProteinCover() {
    return proteinCoverField().isChecked();
  }

  public void setProteinCover(boolean value) {
    setCheckBoxValue(proteinCoverField(), value);
  }

  private CheckBoxElement proteinLenField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_LEN));
  }

  public boolean getProteinLen() {
    return proteinLenField().isChecked();
  }

  public void setProteinLen(boolean value) {
    setCheckBoxValue(proteinLenField(), value);
  }

  private CheckBoxElement proteinPiField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_PI));
  }

  public boolean getProteinPi() {
    return proteinPiField().isChecked();
  }

  public void setProteinPi(boolean value) {
    setCheckBoxValue(proteinPiField(), value);
  }

  private CheckBoxElement proteinTaxStrField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_TAX_STR));
  }

  public boolean getProteinTaxStr() {
    return proteinTaxStrField().isChecked();
  }

  public void setProteinTaxStr(boolean value) {
    setCheckBoxValue(proteinTaxStrField(), value);
  }

  private CheckBoxElement proteinTaxIdField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_TAX_ID));
  }

  public boolean getProteinTaxId() {
    return proteinTaxIdField().isChecked();
  }

  public void setProteinTaxId(boolean value) {
    setCheckBoxValue(proteinTaxIdField(), value);
  }

  private CheckBoxElement proteinSeqField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_SEQ));
  }

  public boolean getProteinSeq() {
    return proteinSeqField().isChecked();
  }

  public void setProteinSeq(boolean value) {
    setCheckBoxValue(proteinSeqField(), value);
  }

  private CheckBoxElement proteinEmpaiField() {
    return wrap(CheckBoxElement.class, proteinParameter(PROT_EMPAI));
  }

  public boolean getProteinEmpai() {
    return proteinEmpaiField().isChecked();
  }

  public void setProteinEmpai(boolean value) {
    setCheckBoxValue(proteinEmpaiField(), value);
  }

  private CheckBoxElement peptideMasterField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEPTIDE_MASTER));
  }

  public boolean getPeptideMaster() {
    return peptideMasterField().isChecked();
  }

  public void setPeptideMaster(boolean value) {
    setCheckBoxValue(peptideMasterField(), value);
  }

  private CheckBoxElement peptideExpMrField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_EXP_MR));
  }

  public boolean getPeptideExpMr() {
    return peptideExpMrField().isChecked();
  }

  public void setPeptideExpMr(boolean value) {
    setCheckBoxValue(peptideExpMrField(), value);
  }

  private CheckBoxElement peptideExpZField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_EXP_Z));
  }

  public boolean getPeptideExpZ() {
    return peptideExpZField().isChecked();
  }

  public void setPeptideExpZ(boolean value) {
    setCheckBoxValue(peptideExpZField(), value);
  }

  private CheckBoxElement peptideCalcMrField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_CALC_MR));
  }

  public boolean getPeptideCalcMr() {
    return peptideCalcMrField().isChecked();
  }

  public void setPeptideCalcMr(boolean value) {
    setCheckBoxValue(peptideCalcMrField(), value);
  }

  private CheckBoxElement peptideDeltaField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_DELTA));
  }

  public boolean getPeptideDelta() {
    return peptideDeltaField().isChecked();
  }

  public void setPeptideDelta(boolean value) {
    setCheckBoxValue(peptideDeltaField(), value);
  }

  private CheckBoxElement peptideStartField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_START));
  }

  public boolean getPeptideStart() {
    return peptideStartField().isChecked();
  }

  public void setPeptideStart(boolean value) {
    setCheckBoxValue(peptideStartField(), value);
  }

  private CheckBoxElement peptideEndField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_END));
  }

  public boolean getPeptideEnd() {
    return peptideEndField().isChecked();
  }

  public void setPeptideEnd(boolean value) {
    setCheckBoxValue(peptideEndField(), value);
  }

  private CheckBoxElement peptideMissField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_MISS));
  }

  public boolean getPeptideMiss() {
    return peptideMissField().isChecked();
  }

  public void setPeptideMiss(boolean value) {
    setCheckBoxValue(peptideMissField(), value);
  }

  private CheckBoxElement peptideScoreField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_SCORE));
  }

  public boolean getPeptideScore() {
    return peptideScoreField().isChecked();
  }

  public void setPeptideScore(boolean value) {
    setCheckBoxValue(peptideScoreField(), value);
  }

  private CheckBoxElement peptideHomolField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_HOMOL));
  }

  public boolean getPeptideHomol() {
    return peptideHomolField().isChecked();
  }

  public void setPeptideHomol(boolean value) {
    setCheckBoxValue(peptideHomolField(), value);
  }

  private CheckBoxElement peptideIdentField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_IDENT));
  }

  public boolean getPeptideIdent() {
    return peptideIdentField().isChecked();
  }

  public void setPeptideIdent(boolean value) {
    setCheckBoxValue(peptideIdentField(), value);
  }

  private CheckBoxElement peptideExpectField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_EXPECT));
  }

  public boolean getPeptideExpect() {
    return peptideExpectField().isChecked();
  }

  public void setPeptideExpect(boolean value) {
    setCheckBoxValue(peptideExpectField(), value);
  }

  private CheckBoxElement peptideSeqField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_SEQ));
  }

  public boolean getPeptideSeq() {
    return peptideSeqField().isChecked();
  }

  public void setPeptideSeq(boolean value) {
    setCheckBoxValue(peptideSeqField(), value);
  }

  private CheckBoxElement peptideFrameField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_FRAME));
  }

  public boolean getPeptideFrame() {
    return peptideFrameField().isChecked();
  }

  public void setPeptideFrame(boolean value) {
    setCheckBoxValue(peptideFrameField(), value);
  }

  private CheckBoxElement peptideVarModField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_VAR_MOD));
  }

  public boolean getPeptideVarMod() {
    return peptideVarModField().isChecked();
  }

  public void setPeptideVarMod(boolean value) {
    setCheckBoxValue(peptideVarModField(), value);
  }

  private CheckBoxElement peptideNumMatchField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_NUM_MATCH));
  }

  public boolean getPeptideNumMatch() {
    return peptideNumMatchField().isChecked();
  }

  public void setPeptideNumMatch(boolean value) {
    setCheckBoxValue(peptideNumMatchField(), value);
  }

  private CheckBoxElement peptideScanTitleField() {
    return wrap(CheckBoxElement.class, peptideParameter(PEP_SCAN_TITLE));
  }

  public boolean getPeptideScanTitle() {
    return peptideScanTitleField().isChecked();
  }

  public void setPeptideScanTitle(boolean value) {
    setCheckBoxValue(peptideScanTitleField(), value);
  }

  private CheckBoxElement peptideShowUnassignedField() {
    return wrap(CheckBoxElement.class, peptideParameter(SHOW_UNASSIGNED));
  }

  public boolean getPeptideShowUnassigned() {
    return peptideShowUnassignedField().isChecked();
  }

  public void setPeptideShowUnassigned(boolean value) {
    setCheckBoxValue(peptideShowUnassignedField(), value);
  }

  private CheckBoxElement peptideShowDupesField() {
    return wrap(CheckBoxElement.class, peptideParameter(SHOW_PEP_DUPES));
  }

  public boolean getPeptideShowDupes() {
    return peptideShowDupesField().isChecked();
  }

  public void setPeptideShowDupes(boolean value) {
    setCheckBoxValue(peptideShowDupesField(), value);
  }

  private CheckBoxElement queryMasterField() {
    return wrap(CheckBoxElement.class, queryParameter(QUERY_MASTER));
  }

  public boolean getQueryMaster() {
    return queryMasterField().isChecked();
  }

  public void setQueryMaster(boolean value) {
    setCheckBoxValue(queryMasterField(), value);
  }

  private CheckBoxElement queryTitleField() {
    return wrap(CheckBoxElement.class, queryParameter(QUERY_TITLE));
  }

  public boolean getQueryTitle() {
    return queryTitleField().isChecked();
  }

  public void setQueryTitle(boolean value) {
    setCheckBoxValue(queryTitleField(), value);
  }

  private CheckBoxElement queryQualifiersField() {
    return wrap(CheckBoxElement.class, queryParameter(QUERY_QUALIFIERS));
  }

  public boolean getQueryQualifiers() {
    return queryQualifiersField().isChecked();
  }

  public void setQueryQualifiers(boolean value) {
    setCheckBoxValue(queryQualifiersField(), value);
  }

  private CheckBoxElement queryParamsField() {
    return wrap(CheckBoxElement.class, queryParameter(QUERY_PARAMS));
  }

  public boolean getQueryParams() {
    return queryParamsField().isChecked();
  }

  public void setQueryParams(boolean value) {
    setCheckBoxValue(queryParamsField(), value);
  }

  private CheckBoxElement queryPeaksField() {
    return wrap(CheckBoxElement.class, queryParameter(QUERY_PEAKS));
  }

  public boolean getQueryPeaks() {
    return queryPeaksField().isChecked();
  }

  public void setQueryPeaks(boolean value) {
    setCheckBoxValue(queryPeaksField(), value);
  }

  private CheckBoxElement queryRawField() {
    return wrap(CheckBoxElement.class, queryParameter(QUERY_RAW));
  }

  public boolean getQueryRaw() {
    return queryRawField().isChecked();
  }

  public void setQueryRaw(boolean value) {
    setCheckBoxValue(queryRawField(), value);
  }

  public void clickSave() {
    findElement(className(ExportViewPresenter.SAVE_STYLE)).click();
  }

  public void clickSaveAndRun() {
    findElement(className(ExportViewPresenter.SAVE_RUN_STYLE)).click();
  }

  protected GridElement jobsGrid() {
    return $(GridElement.class).id(JOBS_ID);
  }

  protected List<ExportJob> getJobs() {
    GridElement jobsGrid = jobsGrid();
    List<ExportJob> jobs = new ArrayList<>();
    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
    iteratorRows(jobsGrid, rowIndex -> {
      GridCellElement fileCell = jobsGrid.getCell(rowIndex, JOB_FILE_COLUMN);
      GridCellElement dateCell = jobsGrid.getCell(rowIndex, JOB_DATE_COLUMN);
      ExportJob job = new ExportJob();
      job.setFilePath(fileCell.getText());
      job.setDate(
          dateCell.getText() != null
              ? LocalDate.parse(dateCell.getText(), formatter.withLocale(Locale.getDefault()))
                  .atTime(0, 0)
              : null);
      jobs.add(job);
    });
    return jobs;
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.ExportViewPresenter.FILE;
import static ca.qc.ircm.mascotexport.web.WebConstants.FIELD_NOTIFICATION;
import static ca.qc.ircm.mascotexport.web.WebConstants.GENERAL_MESSAGES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionWindow;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.mascotexport.web.SaveEvent;
import ca.qc.ircm.mascotexport.web.SaveListener;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.CheckBox;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import javax.inject.Provider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ExportViewPresenterTest {
  private ExportViewPresenter presenter;
  @Mock
  private ExportView view;
  @Mock
  private ExportJobService jobService;
  @Mock
  private ApplicationEventPublisher publisher;
  @Mock
  private Provider<MascotFilesSelectionWindow> mascotFilesSelectionWindowProvider;
  @Mock
  private MascotFilesSelectionWindow mascotFilesSelectionWindow;
  @Mock
  private BinderValidationStatus<ExportParameters> exportParametersValidationStatus;
  @Mock
  private BinderValidationStatus<SearchParameters> searchParametersValidationStatus;
  @Mock
  private BinderValidationStatus<ProteinParameters> proteinParametersValidationStatus;
  @Mock
  private BinderValidationStatus<PeptideParameters> peptideParametersValidationStatus;
  @Mock
  private BinderValidationStatus<QueryParameters> queryParametersValidationStatus;
  @Mock
  private SaveEvent<List<Path>> saveEvent;
  @Captor
  private ArgumentCaptor<MascotFilesSelectionWindow> mascotFilesSelectionWindowCaptor;
  @Captor
  private ArgumentCaptor<SaveListener<List<Path>>> saveListenerCaptor;
  @Captor
  private ArgumentCaptor<Collection<ExportJob>> jobsCaptor;
  @Captor
  private ArgumentCaptor<ApplicationEvent> applicationEventCaptor;
  private ExportViewDesign design;
  private CheckBox proteinMasterProperty = new CheckBox("proteinMaster", true);
  private CheckBox peptideProteinMasterProperty = new CheckBox("peptideProteinMaster", true);
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(ExportView.class, locale);
  private MessageResource generalResources = new MessageResource(GENERAL_MESSAGES, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new ExportViewPresenter(jobService, publisher, mascotFilesSelectionWindowProvider);
    view.exportSearchResultsForm = mock(ExportSearchResultsForm.class);
    view.searchInformationForm = mock(SearchInformationForm.class);
    view.proteinHitInformationForm = mock(ProteinHitInformationForm.class);
    view.peptideMatchInformationForm = mock(PeptideMatchInformationForm.class);
    view.queryLevelInformationForm = mock(QueryLevelInformationForm.class);
    design = new ExportViewDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    when(view.exportSearchResultsForm.validate()).thenReturn(exportParametersValidationStatus);
    when(exportParametersValidationStatus.isOk()).thenReturn(true);
    when(view.searchInformationForm.validate()).thenReturn(searchParametersValidationStatus);
    when(searchParametersValidationStatus.isOk()).thenReturn(true);
    when(view.proteinHitInformationForm.validate()).thenReturn(proteinParametersValidationStatus);
    when(proteinParametersValidationStatus.isOk()).thenReturn(true);
    when(view.peptideMatchInformationForm.validate()).thenReturn(peptideParametersValidationStatus);
    when(peptideParametersValidationStatus.isOk()).thenReturn(true);
    when(view.queryLevelInformationForm.validate()).thenReturn(queryParametersValidationStatus);
    when(queryParametersValidationStatus.isOk()).thenReturn(true);
    when(mascotFilesSelectionWindowProvider.get()).thenReturn(mascotFilesSelectionWindow);
    when(view.proteinHitInformationForm.masterProperty()).thenReturn(proteinMasterProperty);
    when(view.peptideMatchInformationForm.proteinMasterProperty())
        .thenReturn(peptideProteinMasterProperty);
    presenter.init(view);
  }

  @SuppressWarnings("unchecked")
  private Collection<Path> getFilesInGrid() {
    return ((ListDataProvider<Path>) design.filesGrid.getDataProvider()).getItems();
  }

  private List<Path> setFilesInGrid() {
    List<Path> files = new ArrayList<>();
    files.add(Paths.get("F00123.dat"));
    files.add(Paths.get("F01456.dat"));
    files.add(Paths.get("F01457.dat"));
    files.add(Paths.get("F01458.dat"));
    presenter.addFiles(files);
    return files;
  }

  private ExportJob findJob(Collection<ExportJob> jobs, Path file) {
    for (ExportJob job : jobs) {
      if (file.toString().equals(job.getFilePath())) {
        return job;
      }
    }
    return null;
  }

  @Test
  public void defaultValues() {
    Collection<Path> files = getFilesInGrid();

    assertTrue(files.isEmpty());
  }

  @Test
  public void captions() {
    verify(view).setTitle(resources.message("title"));
    assertEquals(resources.message("parameters"), design.parametersLabel.getValue());
    assertEquals(resources.message("files"), design.filesLabel.getValue());
    assertEquals(resources.message("addFiles"), design.addFilesButton.getCaption());
    assertEquals(resources.message("removeFiles"), design.removeFilesButton.getCaption());
    assertEquals(resources.message("save"), design.saveButton.getCaption());
    assertEquals(resources.message("saveAndRun"), design.saveAndRunButton.getCaption());
    assertEquals(resources.message("file.header"), design.filesGrid.getColumn(FILE).getCaption());
  }

  @Test
  public void bindProteinMasterProperty() {
    assertTrue(proteinMasterProperty.getValue());
    assertTrue(peptideProteinMasterProperty.getValue());

    proteinMasterProperty.setValue(false);

    assertFalse(proteinMasterProperty.getValue());
    assertFalse(peptideProteinMasterProperty.getValue());

    proteinMasterProperty.setValue(true);

    assertTrue(proteinMasterProperty.getValue());
    assertTrue(peptideProteinMasterProperty.getValue());
  }

  @Test
  public void addFiles() {
    List<Path> files = new ArrayList<>();
    files.add(Paths.get("F00123.dat"));
    files.add(Paths.get("F01456.dat"));
    when(saveEvent.getSavedObject()).thenReturn(new ArrayList<>(files));

    design.addFilesButton.click();

    verify(mascotFilesSelectionWindowProvider).get();
    verify(mascotFilesSelectionWindow).addSaveListener(saveListenerCaptor.capture());
    verify(view).addWindow(mascotFilesSelectionWindow);
    SaveListener<List<Path>> listener = saveListenerCaptor.getValue();
    listener.saved(saveEvent);
    Collection<?> itemIds = getFilesInGrid();
    assertEquals(files.size(), itemIds.size());
    for (Path file : files) {
      assertTrue(itemIds.contains(file));
    }
  }

  @Test
  public void removeFiles() {
    List<Path> files = setFilesInGrid();
    design.filesGrid.select(files.get(0));
    design.filesGrid.select(files.get(1));

    design.removeFilesButton.click();

    Collection<?> itemIds = getFilesInGrid();
    assertEquals(files.size() - 2, itemIds.size());
    for (int i = 2; i < files.size(); i++) {
      assertTrue(itemIds.contains(files.get(i)));
    }
  }

  @Test
  public void save() throws Throwable {
    final List<Path> files = setFilesInGrid();

    design.saveButton.click();

    verify(view.exportSearchResultsForm).validate();
    verify(view.searchInformationForm).validate();
    verify(view.proteinHitInformationForm).validate();
    verify(view.peptideMatchInformationForm).validate();
    verify(view.queryLevelInformationForm).validate();
    verify(view.exportSearchResultsForm, times(files.size())).writeBean(any());
    verify(view.searchInformationForm, times(files.size())).writeBean(any());
    verify(view.proteinHitInformationForm, times(files.size())).writeBean(any());
    verify(view.peptideMatchInformationForm, times(files.size())).writeBean(any());
    verify(view.queryLevelInformationForm, times(files.size())).writeBean(any());
    verify(jobService).insert(jobsCaptor.capture());
    Collection<ExportJob> jobs = jobsCaptor.getValue();
    assertEquals(files.size(), jobs.size());
    for (Path file : files) {
      ExportJob job = findJob(jobs, file);
      assertNotNull(job);
      assertEquals(file.toString(), job.getFilePath());
      assertNull(job.getDate());
      assertNull(job.getDoneDate());
      assertNotNull(job.getExportParameters());
      assertNotNull(job.getSearchParameters());
      assertNotNull(job.getProteinParameters());
      assertNotNull(job.getPeptideParameters());
      assertNotNull(job.getQueryParameters());
    }
    verify(view).navigateTo(JobsView.VIEW_NAME);
  }

  @Test
  public void save_NoFiles() {
    design.saveButton.click();

    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_InvalidExportSearchResults() throws Throwable {
    setFilesInGrid();
    when(exportParametersValidationStatus.isOk()).thenReturn(false);

    design.saveButton.click();

    verify(view.exportSearchResultsForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_InvalidSearchInformation() throws Throwable {
    setFilesInGrid();
    when(searchParametersValidationStatus.isOk()).thenReturn(false);

    design.saveButton.click();

    verify(view.searchInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_InvalidProteinHitInformation() throws Throwable {
    setFilesInGrid();
    when(proteinParametersValidationStatus.isOk()).thenReturn(false);

    design.saveButton.click();

    verify(view.proteinHitInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_InvalidPeptideMatchInformation() throws Throwable {
    setFilesInGrid();
    when(peptideParametersValidationStatus.isOk()).thenReturn(false);

    design.saveButton.click();

    verify(view.peptideMatchInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void save_InvalidQueryLevelInformation() throws Throwable {
    setFilesInGrid();
    when(queryParametersValidationStatus.isOk()).thenReturn(false);

    design.saveButton.click();

    verify(view.queryLevelInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void saveAndRun() throws Throwable {
    final List<Path> files = setFilesInGrid();

    design.saveAndRunButton.click();

    verify(view.exportSearchResultsForm).validate();
    verify(view.searchInformationForm).validate();
    verify(view.proteinHitInformationForm).validate();
    verify(view.peptideMatchInformationForm).validate();
    verify(view.queryLevelInformationForm).validate();
    verify(view.exportSearchResultsForm, times(files.size())).writeBean(any());
    verify(view.searchInformationForm, times(files.size())).writeBean(any());
    verify(view.proteinHitInformationForm, times(files.size())).writeBean(any());
    verify(view.peptideMatchInformationForm, times(files.size())).writeBean(any());
    verify(view.queryLevelInformationForm, times(files.size())).writeBean(any());
    verify(jobService).insert(jobsCaptor.capture());
    Collection<ExportJob> jobs = jobsCaptor.getValue();
    assertEquals(files.size(), jobs.size());
    for (Path file : files) {
      ExportJob job = findJob(jobs, file);
      assertNotNull(job);
      assertEquals(file.toString(), job.getFilePath());
      assertNull(job.getDate());
      assertNull(job.getDoneDate());
      assertNotNull(job.getExportParameters());
      assertNotNull(job.getSearchParameters());
      assertNotNull(job.getProteinParameters());
      assertNotNull(job.getPeptideParameters());
      assertNotNull(job.getQueryParameters());
    }
    verify(publisher).publishEvent(applicationEventCaptor.capture());
    assertTrue(applicationEventCaptor.getAllValues().get(0) instanceof StartProcessExportJobsEvent);
    StartProcessExportJobsEvent startJobsEvent =
        (StartProcessExportJobsEvent) applicationEventCaptor.getAllValues().get(0);
    assertEquals(jobs, startJobsEvent.getJobs());
    verify(view).navigateTo(JobsView.VIEW_NAME);
  }

  @Test
  public void saveAndRun_NoFiles() {
    design.saveAndRunButton.click();

    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void saveAndRun_InvalidExportSearchResults() throws Throwable {
    setFilesInGrid();
    when(exportParametersValidationStatus.isOk()).thenReturn(false);

    design.saveAndRunButton.click();

    verify(view.exportSearchResultsForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void saveAndRun_InvalidSearchInformation() throws Throwable {
    setFilesInGrid();
    when(searchParametersValidationStatus.isOk()).thenReturn(false);

    design.saveAndRunButton.click();

    verify(view.searchInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void saveAndRun_InvalidProteinHitInformation() throws Throwable {
    setFilesInGrid();
    when(proteinParametersValidationStatus.isOk()).thenReturn(false);

    design.saveAndRunButton.click();

    verify(view.proteinHitInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void saveAndRun_InvalidPeptideMatchInformation() throws Throwable {
    setFilesInGrid();
    when(peptideParametersValidationStatus.isOk()).thenReturn(false);

    design.saveAndRunButton.click();

    verify(view.peptideMatchInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }

  @Test
  public void saveAndRun_InvalidQueryLevelInformation() throws Throwable {
    setFilesInGrid();
    when(queryParametersValidationStatus.isOk()).thenReturn(false);

    design.saveAndRunButton.click();

    verify(view.queryLevelInformationForm).validate();
    verify(jobService, never()).insert(any(ExportJob.class));
    verify(jobService, never()).insert(anyCollectionOf(ExportJob.class));
    verify(view, never()).navigateTo(JobsView.VIEW_NAME);
    verify(view).showError(generalResources.message(FIELD_NOTIFICATION));
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.className;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.testbench.elements.NotificationElement;
import com.vaadin.ui.Notification;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class ExportViewTest extends ExportViewPageObject {
  @Inject
  private MongoTemplate mongoTemplate;
  @Inject
  private MascotConfiguration mascotConfiguration;
  private Path mascotFilesRoot;
  private Path mascotFile10 = Paths.get("20110330/F017513.dat");
  private Path mascotFile11 = Paths.get("20110330/F017514.dat");
  private Path mascotFile20 = Paths.get("20110331/F017515.dat");
  private Path mascotFile21 = Paths.get("20110331/F017516.dat");
  private MessageResource resources = new MessageResource(ExportView.class, Locale.getDefault());

  /**
   * Before tests.
   */
  @Before
  public void beforeTest() throws Throwable {
    mascotFilesRoot = mascotConfiguration.getData();
    open();
  }

  private List<ExportJob> allJobs() {
    return mongoTemplate.findAll(ExportJob.class);
  }

  @After
  public void afterTest() throws Throwable {
    FileUtils.deleteDirectory(mascotFilesRoot.toFile());
  }

  private void copyMascotFile(Path mascotFile) throws URISyntaxException, IOException {
    Path sourceDatFile = Paths.get(this.getClass().getResource("/F011906.dat").toURI());
    Files.createDirectories(mascotFile.getParent());
    Files.copy(sourceDatFile, mascotFile);
  }

  @Test
  public void title() throws Throwable {
    assertEquals(resources.message("title"), getDriver().getTitle());
  }

  @Test
  public void fileSelection_All() throws Throwable {
    copyMascotFile(mascotFilesRoot.resolve(mascotFile10));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile11));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile20));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile21));

    selectFiles(0, 1, 2, 3);

    List<String> files = getSelectedFiles();
    assertTrue(files.contains(mascotFile10.getFileName().toString()));
    assertTrue(files.contains(mascotFile11.getFileName().toString()));
    assertTrue(files.contains(mascotFile20.getFileName().toString()));
    assertTrue(files.contains(mascotFile21.getFileName().toString()));
    assertEquals(4, files.size());
  }

  @Test
  public void fileSelection_Files() throws Throwable {
    copyMascotFile(mascotFilesRoot.resolve(mascotFile10));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile11));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile20));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile21));

    selectFiles(0, 3);

    List<String> files = getSelectedFiles();
    assertFalse(files.contains(mascotFile10.getFileName().toString()));
    assertTrue(files.contains(mascotFile11.getFileName().toString()));
    assertTrue(files.contains(mascotFile20.getFileName().toString()));
    assertFalse(files.contains(mascotFile21.getFileName().toString()));
    assertEquals(2, files.size());
  }

  @Test
  public void save() throws Throwable {
    copyMascotFile(mascotFilesRoot.resolve(mascotFile10));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile11));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile20));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile21));
    selectFiles(2, 3);
    final List<ExportJob> beforeSaveJobs = allJobs();

    clickSave();

    assertEquals(viewUrl(JobsView.VIEW_NAME), getDriver().getCurrentUrl());
    List<ExportJob> jobs = getJobs();
    assertEquals(beforeSaveJobs.size() + 2, jobs.size());
  }

  @Test
  public void save_Error() throws Throwable {
    clickSave();

    NotificationElement notification = $(NotificationElement.class).first();
    assertEquals(Notification.Type.ERROR_MESSAGE.getStyle(), notification.getType());
    assertNotNull(notification.getCaption());
  }

  @Test
  public void saveAndRun() throws Throwable {
    copyMascotFile(mascotFilesRoot.resolve(mascotFile10));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile11));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile20));
    copyMascotFile(mascotFilesRoot.resolve(mascotFile21));
    selectFiles(2, 3);
    final List<ExportJob> beforeSaveJobs = allJobs();

    clickSaveAndRun();

    assertEquals(viewUrl(JobsView.VIEW_NAME), getDriver().getCurrentUrl());
    List<ExportJob> jobs = getJobs();
    assertEquals(beforeSaveJobs.size() + 2, jobs.size());
    assertNotNull(findElement(className(ExportProgressWindowPresenter.STYLE)));
  }

  @Test
  public void saveAndRun_Error() throws Throwable {
    clickSaveAndRun();

    NotificationElement notification = $(NotificationElement.class).first();
    assertEquals(Notification.Type.ERROR_MESSAGE.getStyle(), notification.getType());
    assertNotNull(notification.getCaption());
  }
}

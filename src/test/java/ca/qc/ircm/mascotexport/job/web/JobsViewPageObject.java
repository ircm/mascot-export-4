/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.EXECUTE_JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REFRESH_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REMOVE_DONE_JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REMOVE_JOBS_ID;
import static org.openqa.selenium.By.tagName;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import com.vaadin.testbench.elements.ButtonElement;
import com.vaadin.testbench.elements.GridElement;
import com.vaadin.testbench.elements.GridElement.GridCellElement;
import com.vaadin.testbench.elements.GridElement.GridRowElement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Consumer;
import org.openqa.selenium.NoSuchElementException;

public abstract class JobsViewPageObject extends AbstractTestBenchTestCase {
  private static final int FILE_COLUMN = 1;
  private static final int DATE_COLUMN = 3;
  private static final int DONE_DATE_COLUMN = 4;
  private static final int VIEW_PARAMETERS_COLUMN = 5;
  private static final int REMOVE_COLUMN = 6;

  protected void open() {
    openView(JobsView.VIEW_NAME);
  }

  protected void iteratorRows(GridElement grid, Consumer<Integer> consumer) {
    int rowIndex = 0;
    try {
      while (true) {
        grid.getRow(rowIndex);
        consumer.accept(rowIndex);
        rowIndex++;
      }
    } catch (NoSuchElementException exception) {
      // No more rows.
    }
  }

  protected GridElement jobsGrid() {
    return $(GridElement.class).id(JOBS_ID);
  }

  protected void selectJobsRows(List<Integer> rows) {
    Set<Integer> rowsToSelect = new HashSet<>(rows);
    GridElement jobsGrid = jobsGrid();
    iteratorRows(jobsGrid, rowIndex -> {
      if (rowsToSelect.contains(rowIndex)) {
        GridCellElement selectedCell = jobsGrid.getCell(rowIndex, 0);
        selectedCell.click();
      }
    });
  }

  protected List<ExportJob> getJobs() {
    GridElement jobsGrid = jobsGrid();
    List<ExportJob> jobs = new ArrayList<>();
    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;
    iteratorRows(jobsGrid, rowIndex -> {
      GridCellElement fileCell = jobsGrid.getCell(rowIndex, FILE_COLUMN);
      GridCellElement dateCell = jobsGrid.getCell(rowIndex, DATE_COLUMN);
      GridCellElement doneDateCell = jobsGrid.getCell(rowIndex, DONE_DATE_COLUMN);
      ExportJob job = new ExportJob();
      job.setFilePath(fileCell.getText());
      job.setDate(
          dateCell.getText() != null
              ? LocalDate.parse(dateCell.getText(), formatter.withLocale(Locale.getDefault()))
                  .atTime(0, 0)
              : null);
      if (!doneDateCell.getText().isEmpty()) {
        job.setDoneDate(dateCell.getText() != null ? LocalDate
            .parse(doneDateCell.getText(), formatter.withLocale(Locale.getDefault())).atTime(0, 0)
            : null);
      }
      jobs.add(job);
    });
    return jobs;
  }

  protected List<Integer> getJobsRows() {
    GridElement jobsGrid = jobsGrid();
    List<Integer> allRows = new ArrayList<>();
    iteratorRows(jobsGrid, rowIndex -> {
      allRows.add(rowIndex);
    });
    return allRows;
  }

  protected List<Integer> getSelectedJobsRows() {
    GridElement jobsGrid = jobsGrid();
    List<Integer> selectedRows = new ArrayList<>();
    iteratorRows(jobsGrid, rowIndex -> {
      GridRowElement row = jobsGrid.getRow(rowIndex);
      if (row.isSelected()) {
        selectedRows.add(rowIndex);
      }
    });
    return selectedRows;
  }

  protected void clickViewParametersByRow(int row) {
    GridElement jobsGrid = jobsGrid();
    jobsGrid.getCell(row, VIEW_PARAMETERS_COLUMN).findElement(tagName("button")).click();
  }

  protected void clickRemoveJobByRow(int row) {
    GridElement jobsGrid = jobsGrid();
    jobsGrid.getCell(row, REMOVE_COLUMN).findElement(tagName("button")).click();
  }

  protected ButtonElement executeJobsButton() {
    return $(ButtonElement.class).id(EXECUTE_JOBS_ID);
  }

  protected void clickExecuteJobsButton() {
    executeJobsButton().click();
  }

  protected ButtonElement removeJobsButton() {
    return $(ButtonElement.class).id(REMOVE_JOBS_ID);
  }

  protected void clickRemoveJobsButton() {
    removeJobsButton().click();
  }

  protected ButtonElement removeDoneJobsButton() {
    return $(ButtonElement.class).id(REMOVE_DONE_JOBS_ID);
  }

  protected void clickRemoveDoneJobsButton() {
    removeDoneJobsButton().click();
  }

  protected ButtonElement refreshButton() {
    return $(ButtonElement.class).id(REFRESH_ID);
  }

  protected void clickRefreshButton() {
    refreshButton().click();
  }
}

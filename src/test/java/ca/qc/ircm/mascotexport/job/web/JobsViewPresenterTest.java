/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.DATE;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.DONE_DATE;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.EXECUTE_JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.FILE;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.INPUT;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REFRESH_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REMOVE_DONE_JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REMOVE_JOB;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.REMOVE_JOBS_ID;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.TITLE;
import static ca.qc.ircm.mascotexport.job.web.JobsViewPresenter.VIEW_PARAMETERS;
import static ca.qc.ircm.mascotexport.test.utils.VaadinGridUtils.columnValue;
import static ca.qc.ircm.mascotexport.test.utils.VaadinGridUtils.renderer;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobService;
import ca.qc.ircm.mascotexport.test.config.ServiceTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Provider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ServiceTestAnnotations
public class JobsViewPresenterTest {
  private JobsViewPresenter presenter;
  @Inject
  private MongoTemplate mongoTemplate;
  @Mock
  private JobsView view;
  @Mock
  private ExportJobService jobService;
  @Mock
  private ApplicationEventPublisher publisher;
  @Mock
  private Provider<ExportJobWindow> exportJobWindowProvider;
  @Mock
  private ExportJobWindow exportJobWindow;
  @Captor
  private ArgumentCaptor<ApplicationEvent> applicationEventCaptor;
  @Captor
  private ArgumentCaptor<Collection<ExportJob>> jobsCaptor;
  @Captor
  private ArgumentCaptor<ClickListener> clickListenerCaptor;
  private JobsViewDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(JobsView.class, locale);
  private List<ExportJob> allJobs;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new JobsViewPresenter(jobService, publisher, exportJobWindowProvider);
    design = new JobsViewDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    allJobs = allJobs();
    when(jobService.all()).thenReturn(allJobs);
    when(exportJobWindowProvider.get()).thenReturn(exportJobWindow);
    presenter.attach(view);
  }

  private List<ExportJob> allJobs() {
    return mongoTemplate.findAll(ExportJob.class);
  }

  @SuppressWarnings("unchecked")
  private Collection<ExportJob> gridJobs() {
    return ((ListDataProvider<ExportJob>) design.jobsGrid.getDataProvider()).getItems();
  }

  @Test
  public void defaultValues() {
    Collection<?> jobs = gridJobs();

    verify(jobService).all();
    assertEquals(allJobs.size(), jobs.size());
    for (ExportJob job : allJobs) {
      assertTrue(jobs.contains(job));
    }
    List<GridSortOrder<ExportJob>> sortOrders = design.jobsGrid.getSortOrder();
    assertEquals(1, sortOrders.size());
    GridSortOrder<ExportJob> sortOrder = sortOrders.get(0);
    assertEquals(DATE, sortOrder.getSorted().getId());
    assertEquals(SortDirection.ASCENDING, sortOrder.getDirection());
  }

  @Test
  public void styles() {
  }

  @Test
  public void captions() {
    verify(view).setTitle(resources.message(TITLE));
    assertEquals(resources.message(EXECUTE_JOBS_ID), design.executeJobsButton.getCaption());
    assertEquals(resources.message(REMOVE_JOBS_ID), design.removeJobsButton.getCaption());
    assertEquals(resources.message(REMOVE_DONE_JOBS_ID), design.removeDoneJobsButton.getCaption());
    assertEquals(resources.message(REFRESH_ID), design.refreshButton.getCaption());
  }

  @Test
  public void jobs() throws Throwable {
    assertEquals(6, design.jobsGrid.getColumns().size());
    assertEquals(FILE, design.jobsGrid.getColumns().get(0).getId());
    assertEquals(resources.message(FILE), design.jobsGrid.getColumn(FILE).getCaption());
    for (ExportJob job : allJobs) {
      assertEquals(job.getFilePath(), columnValue(design.jobsGrid.getColumn(FILE), job));
    }
    assertEquals(INPUT, design.jobsGrid.getColumns().get(1).getId());
    assertEquals(resources.message(INPUT), design.jobsGrid.getColumn(INPUT).getCaption());
    assertEquals(DATE, design.jobsGrid.getColumns().get(2).getId());
    assertEquals(resources.message(DATE), design.jobsGrid.getColumn(DATE).getCaption());
    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withLocale(locale);
    for (ExportJob job : allJobs) {
      assertEquals(formatter.format(job.getDate()),
          columnValue(design.jobsGrid.getColumn(DATE), job));
    }
    assertEquals(DONE_DATE, design.jobsGrid.getColumns().get(3).getId());
    assertEquals(resources.message(DONE_DATE), design.jobsGrid.getColumn(DONE_DATE).getCaption());
    for (ExportJob job : allJobs) {
      assertEquals(job.getDoneDate() != null ? formatter.format(job.getDoneDate()) : "",
          columnValue(design.jobsGrid.getColumn(DONE_DATE), job));
    }
    assertEquals(VIEW_PARAMETERS, design.jobsGrid.getColumns().get(4).getId());
    assertEquals(resources.message(VIEW_PARAMETERS),
        design.jobsGrid.getColumn(VIEW_PARAMETERS).getCaption());
    for (ExportJob job : allJobs) {
      assertEquals(resources.message(VIEW_PARAMETERS),
          columnValue(design.jobsGrid.getColumn(VIEW_PARAMETERS), job));
    }
    assertEquals(REMOVE_JOB, design.jobsGrid.getColumns().get(5).getId());
    assertEquals(resources.message(REMOVE_JOB), design.jobsGrid.getColumn(REMOVE_JOB).getCaption());
    for (ExportJob job : allJobs) {
      assertEquals(resources.message(REMOVE_JOB),
          columnValue(design.jobsGrid.getColumn(REMOVE_JOB), job));
    }
  }

  @Test
  @SuppressWarnings({ "unchecked", "serial" })
  public void viewParameters() {
    final ExportJob job = allJobs.get(0);
    Column<ExportJob, ?> viewParametersColumn = design.jobsGrid.getColumn(VIEW_PARAMETERS);
    ButtonRenderer<ExportJob> renderer = renderer(viewParametersColumn, ButtonRenderer.class);
    RendererClickListener<ExportJob> viewListener =
        ((Collection<RendererClickListener<ExportJob>>) renderer
            .getListeners(RendererClickEvent.class)).iterator().next();

    viewListener.click(new RendererClickEvent<ExportJob>(design.jobsGrid, allJobs.get(0),
        viewParametersColumn, null) {});

    verify(exportJobWindowProvider).get();
    verify(exportJobWindow).setValue(job);
    verify(exportJobWindow).center();
    verify(view).addWindow(exportJobWindow);
  }

  @Test
  @SuppressWarnings({ "unchecked", "serial" })
  public void removeJob() {
    final ExportJob job = allJobs.get(0);
    Column<ExportJob, ?> removeJobColumn = design.jobsGrid.getColumn(REMOVE_JOB);
    List<ExportJob> remaniningJobs = new ArrayList<>(allJobs);
    remaniningJobs.remove(0);
    when(jobService.all()).thenReturn(remaniningJobs);
    ButtonRenderer<ExportJob> renderer = renderer(removeJobColumn, ButtonRenderer.class);
    RendererClickListener<ExportJob> viewListener =
        ((Collection<RendererClickListener<ExportJob>>) renderer
            .getListeners(RendererClickEvent.class)).iterator().next();

    viewListener.click(new RendererClickEvent<ExportJob>(design.jobsGrid, allJobs.get(0),
        removeJobColumn, null) {});

    verify(jobService).delete(job);
    Collection<?> containerJobs = gridJobs();
    assertEquals(remaniningJobs.size(), containerJobs.size());
    for (ExportJob testJob : remaniningJobs) {
      assertTrue(containerJobs.contains(testJob));
    }
  }

  @Test
  public void refresh() {
    allJobs.remove(0);

    design.refreshButton.click();

    verify(jobService, times(2)).all();
    Collection<?> containerJobs = gridJobs();
    assertEquals(allJobs.size(), containerJobs.size());
    for (ExportJob job : allJobs) {
      assertTrue(containerJobs.contains(job));
    }
  }

  @Test
  public void executeJobs() {
    design.jobsGrid.select(allJobs.get(0));
    design.jobsGrid.select(allJobs.get(2));

    design.executeJobsButton.click();

    verify(publisher).publishEvent(applicationEventCaptor.capture());
    assertTrue(applicationEventCaptor.getValue() instanceof StartProcessExportJobsEvent);
    StartProcessExportJobsEvent event =
        (StartProcessExportJobsEvent) applicationEventCaptor.getValue();
    Collection<ExportJob> jobs = event.getJobs();
    assertEquals(2, jobs.size());
    assertTrue(jobs.contains(allJobs.get(0)));
    assertTrue(jobs.contains(allJobs.get(2)));
  }

  @Test
  public void removeJobs() {
    design.jobsGrid.select(allJobs.get(0));
    design.jobsGrid.select(allJobs.get(2));
    List<ExportJob> remaniningJobs = new ArrayList<>(allJobs);
    remaniningJobs.remove(2);
    remaniningJobs.remove(0);
    when(jobService.all()).thenReturn(remaniningJobs);

    design.removeJobsButton.click();

    verify(jobService).delete(jobsCaptor.capture());
    Collection<ExportJob> jobs = jobsCaptor.getValue();
    assertEquals(2, jobs.size());
    assertTrue(jobs.contains(allJobs.get(0)));
    assertTrue(jobs.contains(allJobs.get(2)));
    verify(jobService, times(2)).all();
    Collection<?> containerJobs = gridJobs();
    assertEquals(remaniningJobs.size(), containerJobs.size());
    for (ExportJob job : remaniningJobs) {
      assertTrue(containerJobs.contains(job));
    }
  }

  @Test
  public void removeDoneJobs() {
    final List<ExportJob> doneJobs =
        allJobs.stream().filter(j -> j.getDoneDate() != null).collect(Collectors.toList());
    final List<ExportJob> nonDoneJobs =
        allJobs.stream().filter(j -> j.getDoneDate() == null).collect(Collectors.toList());
    when(jobService.all()).thenReturn(nonDoneJobs);

    design.removeDoneJobsButton.click();

    verify(jobService).delete(jobsCaptor.capture());
    Collection<ExportJob> jobs = jobsCaptor.getValue();
    assertEquals(doneJobs.size(), jobs.size());
    for (ExportJob job : doneJobs) {
      assertTrue(jobs.contains(job));
    }
    verify(jobService, times(2)).all();
    Collection<?> containerJobs = gridJobs();
    assertEquals(nonDoneJobs.size(), containerJobs.size());
    for (ExportJob job : nonDoneJobs) {
      assertTrue(containerJobs.contains(job));
    }
  }
}

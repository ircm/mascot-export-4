/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.className;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.test.config.RetryOnFail;
import ca.qc.ircm.mascotexport.test.config.RetryOnFailRule;
import ca.qc.ircm.mascotexport.test.config.Slow;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.testbench.elements.LabelElement;
import com.vaadin.testbench.elements.WindowElement;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class JobsViewTest extends JobsViewPageObject {
  @Inject
  private MongoTemplate mongoTemplate;
  @Inject
  private MascotConfiguration mascotConfiguration;
  @Rule
  public RetryOnFailRule retryOnFailRule = new RetryOnFailRule();
  private Path mascotFilesRoot;
  private String mascotFile1 = "20110330/F017513.dat";
  private MessageResource resources = new MessageResource(JobsView.class, Locale.getDefault());
  private MessageResource exportJobWindowResources =
      new MessageResource(ExportJobWindow.class, Locale.getDefault());

  /**
   * Before tests.
   */
  @Before
  public void beforeTest() throws Throwable {
    mascotFilesRoot = mascotConfiguration.getData();
    open();
  }

  @After
  public void afterTest() throws Throwable {
    FileUtils.deleteDirectory(mascotFilesRoot.toFile());
  }

  private List<ExportJob> allJobs() {
    return mongoTemplate.findAll(ExportJob.class);
  }

  private Optional<ExportJob> findJob(Collection<ExportJob> jobs, String filePath,
      LocalDateTime date) {
    return jobs.stream().filter(j -> filePath.equals(j.getFilePath()))
        .filter(j -> date.toLocalDate().equals(j.getDate().toLocalDate())).findAny();
  }

  private void copyMascotFile(Path mascotFile) throws URISyntaxException, IOException {
    Path sourceDatFile = Paths.get(this.getClass().getResource("/F011906.dat").toURI());
    Files.createDirectories(mascotFile.getParent());
    Files.copy(sourceDatFile, mascotFile);
  }

  @Test
  public void title() throws Throwable {
    assertEquals(resources.message("title"), getDriver().getTitle());
  }

  @Test
  public void viewParameters() throws Throwable {
    clickViewParametersByRow(3);

    WindowElement window =
        wrap(WindowElement.class, findElement(className(ExportJobWindowPresenter.STYLE)));
    assertNotNull(window);
    ExportJob job = allJobs().stream().filter(tjob -> tjob.getId().equals("2")).findFirst().get();
    assertEquals(
        exportJobWindowResources.message(ExportJobWindowPresenter.TITLE, job.getFilePath()),
        window.getCaption());
    LabelElement fileLabel =
        wrap(LabelElement.class, window.findElement(className(ExportJobWindowPresenter.FILE)));
    assertEquals(job.getFilePath(), fileLabel.getText());
  }

  @Test
  public void removeJob() throws Throwable {
    List<ExportJob> allJobs = allJobs();
    List<ExportJob> keeptJobs =
        allJobs.stream().filter(job -> !job.getId().equals("2")).collect(Collectors.toList());

    clickRemoveJobByRow(3);

    List<ExportJob> jobs = getJobs();
    assertEquals(keeptJobs.size(), jobs.size());
    for (ExportJob job : keeptJobs) {
      assertTrue(findJob(jobs, job.getFilePath(), job.getDate()).isPresent());
    }
  }

  @Test
  public void executeJobs() throws Throwable {
    copyMascotFile(mascotFilesRoot.resolve(mascotFile1));
    selectJobsRows(Arrays.asList(1));

    clickExecuteJobsButton();

    assertNotNull(findElement(className(ExportProgressWindowPresenter.STYLE)));
  }

  @Test
  public void removeJobs() throws Throwable {
    List<ExportJob> allJobs = allJobs();
    selectJobsRows(Arrays.asList(0, 2));

    clickRemoveJobsButton();

    assertEquals(allJobs.size() - 2, getJobsRows().size());
  }

  @Test
  public void removeDoneJobs() throws Throwable {
    clickRemoveDoneJobsButton();

    List<ExportJob> allJobs = allJobs();
    assertEquals(allJobs.stream().filter(j -> j.getDoneDate() == null).count(),
        getJobsRows().size());
  }

  @Test
  @Slow
  @RetryOnFail(5)
  public void refresh() throws Throwable {
    copyMascotFile(mascotFilesRoot.resolve(mascotFile1));
    ExportJob job = getJobs().get(1);
    assertNull(job.getDoneDate());
    selectJobsRows(Arrays.asList(1));
    clickExecuteJobsButton();
    Thread.sleep(10000);

    clickRefreshButton();

    job = getJobs().get(1);
    assertNotNull(job.getDoneDate());
  }
}

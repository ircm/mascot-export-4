/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEPTIDE_MASTER;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_CALC_MR;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_DELTA;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_END;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_EXPECT;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_EXP_MR;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_EXP_Z;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_FRAME;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_HOMOL;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_IDENT;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_MISS;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_NUM_MATCH;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_SCAN_TITLE;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_SCORE;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_SEQ;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_START;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.PEP_VAR_MOD;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.SHOW_PEP_DUPES;
import static ca.qc.ircm.mascotexport.job.web.PeptideMatchInformationFormPresenter.SHOW_UNASSIGNED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class PeptideMatchInformationFormPresenterTest {
  private PeptideMatchInformationFormPresenter presenter;
  @Mock
  private PeptideMatchInformationForm view;
  private PeptideMatchInformationFormDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources =
      new MessageResource(PeptideMatchInformationForm.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new PeptideMatchInformationFormPresenter();
    design = new PeptideMatchInformationFormDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    presenter.proteinMasterProperty().setValue(true);
  }

  @Test
  public void defaultValues() {
    presenter.init(view);

    assertTrue(design.master.getValue());
    assertTrue(design.expMr.getValue());
    assertTrue(design.expZ.getValue());
    assertTrue(design.calcMr.getValue());
    assertTrue(design.delta.getValue());
    assertTrue(design.start.getValue());
    assertTrue(design.end.getValue());
    assertTrue(design.miss.getValue());
    assertTrue(design.score.getValue());
    assertFalse(design.homol.getValue());
    assertFalse(design.ident.getValue());
    assertTrue(design.expect.getValue());
    assertTrue(design.seq.getValue());
    assertFalse(design.frame.getValue());
    assertTrue(design.varMod.getValue());
    assertTrue(design.numMatch.getValue());
    assertTrue(design.scanTitle.getValue());
    assertFalse(design.showUnassigned.getValue());
    assertFalse(design.showPepDupes.getValue());
  }

  @Test
  public void captions() {
    presenter.init(view);

    assertEquals(resources.message(PEPTIDE_MASTER), design.master.getCaption());
    assertEquals(resources.message(PEP_EXP_MR), design.expMr.getCaption());
    assertEquals(resources.message(PEP_EXP_Z), design.expZ.getCaption());
    assertEquals(resources.message(PEP_CALC_MR), design.calcMr.getCaption());
    assertEquals(resources.message(PEP_DELTA), design.delta.getCaption());
    assertEquals(resources.message(PEP_START), design.start.getCaption());
    assertEquals(resources.message(PEP_END), design.end.getCaption());
    assertEquals(resources.message(PEP_MISS), design.miss.getCaption());
    assertEquals(resources.message(PEP_SCORE), design.score.getCaption());
    assertEquals(resources.message(PEP_HOMOL), design.homol.getCaption());
    assertEquals(resources.message(PEP_IDENT), design.ident.getCaption());
    assertEquals(resources.message(PEP_EXPECT), design.expect.getCaption());
    assertEquals(resources.message(PEP_SEQ), design.seq.getCaption());
    assertEquals(resources.message(PEP_FRAME), design.frame.getCaption());
    assertEquals(resources.message(PEP_VAR_MOD), design.varMod.getCaption());
    assertEquals(resources.message(PEP_NUM_MATCH), design.numMatch.getCaption());
    assertEquals(resources.message(PEP_SCAN_TITLE), design.scanTitle.getCaption());
    assertEquals(resources.message(SHOW_UNASSIGNED), design.showUnassigned.getCaption());
    assertEquals(resources.message(SHOW_PEP_DUPES), design.showPepDupes.getCaption());
  }

  @Test
  public void enableFields() {
    presenter.init(view);
    design.master.setValue(true);

    assertTrue(design.master.isEnabled());
    assertTrue(design.expMr.isEnabled());
    assertTrue(design.expZ.isEnabled());
    assertTrue(design.calcMr.isEnabled());
    assertTrue(design.delta.isEnabled());
    assertTrue(design.start.isEnabled());
    assertTrue(design.end.isEnabled());
    assertTrue(design.miss.isEnabled());
    assertTrue(design.score.isEnabled());
    assertTrue(design.homol.isEnabled());
    assertTrue(design.ident.isEnabled());
    assertTrue(design.expect.isEnabled());
    assertTrue(design.seq.isEnabled());
    assertTrue(design.frame.isEnabled());
    assertTrue(design.varMod.isEnabled());
    assertTrue(design.numMatch.isEnabled());
    assertTrue(design.scanTitle.isEnabled());
    assertTrue(design.showUnassigned.isEnabled());
    assertTrue(design.showPepDupes.isEnabled());
  }

  @Test
  public void disableFields_Master() {
    presenter.init(view);
    design.master.setValue(false);

    assertTrue(design.master.isEnabled());
    assertFalse(design.expMr.isEnabled());
    assertFalse(design.expZ.isEnabled());
    assertFalse(design.calcMr.isEnabled());
    assertFalse(design.delta.isEnabled());
    assertFalse(design.start.isEnabled());
    assertFalse(design.end.isEnabled());
    assertFalse(design.miss.isEnabled());
    assertFalse(design.score.isEnabled());
    assertFalse(design.homol.isEnabled());
    assertFalse(design.ident.isEnabled());
    assertFalse(design.expect.isEnabled());
    assertFalse(design.seq.isEnabled());
    assertFalse(design.frame.isEnabled());
    assertFalse(design.varMod.isEnabled());
    assertFalse(design.numMatch.isEnabled());
    assertFalse(design.scanTitle.isEnabled());
    assertFalse(design.showUnassigned.isEnabled());
    assertFalse(design.showPepDupes.isEnabled());
  }

  @Test
  public void disableFields_ProteinMaster() {
    presenter.init(view);
    presenter.proteinMasterProperty().setValue(false);

    assertFalse(design.master.isEnabled());
    assertFalse(design.expMr.isEnabled());
    assertFalse(design.expZ.isEnabled());
    assertFalse(design.calcMr.isEnabled());
    assertFalse(design.delta.isEnabled());
    assertFalse(design.start.isEnabled());
    assertFalse(design.end.isEnabled());
    assertFalse(design.miss.isEnabled());
    assertFalse(design.score.isEnabled());
    assertFalse(design.homol.isEnabled());
    assertFalse(design.ident.isEnabled());
    assertFalse(design.expect.isEnabled());
    assertFalse(design.seq.isEnabled());
    assertFalse(design.frame.isEnabled());
    assertFalse(design.varMod.isEnabled());
    assertFalse(design.numMatch.isEnabled());
    assertFalse(design.scanTitle.isEnabled());
    assertFalse(design.showUnassigned.isEnabled());
    assertFalse(design.showPepDupes.isEnabled());
  }

  @Test
  public void isReadOnly_Default() {
    presenter.init(view);
    assertFalse(presenter.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    presenter.init(view);
    presenter.setReadOnly(true);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.expMr.isReadOnly());
    assertTrue(design.expZ.isReadOnly());
    assertTrue(design.calcMr.isReadOnly());
    assertTrue(design.delta.isReadOnly());
    assertTrue(design.start.isReadOnly());
    assertTrue(design.end.isReadOnly());
    assertTrue(design.miss.isReadOnly());
    assertTrue(design.score.isReadOnly());
    assertTrue(design.homol.isReadOnly());
    assertTrue(design.ident.isReadOnly());
    assertTrue(design.expect.isReadOnly());
    assertTrue(design.seq.isReadOnly());
    assertTrue(design.frame.isReadOnly());
    assertTrue(design.varMod.isReadOnly());
    assertTrue(design.numMatch.isReadOnly());
    assertTrue(design.scanTitle.isReadOnly());
    assertTrue(design.showUnassigned.isReadOnly());
    assertTrue(design.showPepDupes.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    presenter.init(view);
    presenter.setReadOnly(false);

    assertFalse(presenter.isReadOnly());
    assertFalse(design.master.isReadOnly());
    assertFalse(design.expMr.isReadOnly());
    assertFalse(design.expZ.isReadOnly());
    assertFalse(design.calcMr.isReadOnly());
    assertFalse(design.delta.isReadOnly());
    assertFalse(design.start.isReadOnly());
    assertFalse(design.end.isReadOnly());
    assertFalse(design.miss.isReadOnly());
    assertFalse(design.score.isReadOnly());
    assertFalse(design.homol.isReadOnly());
    assertFalse(design.ident.isReadOnly());
    assertFalse(design.expect.isReadOnly());
    assertFalse(design.seq.isReadOnly());
    assertFalse(design.frame.isReadOnly());
    assertFalse(design.varMod.isReadOnly());
    assertFalse(design.numMatch.isReadOnly());
    assertFalse(design.scanTitle.isReadOnly());
    assertFalse(design.showUnassigned.isReadOnly());
    assertFalse(design.showPepDupes.isReadOnly());
  }

  @Test
  public void setReadOnly_BeforeInit() {
    presenter.setReadOnly(true);
    presenter.init(view);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.expMr.isReadOnly());
    assertTrue(design.expZ.isReadOnly());
    assertTrue(design.calcMr.isReadOnly());
    assertTrue(design.delta.isReadOnly());
    assertTrue(design.start.isReadOnly());
    assertTrue(design.end.isReadOnly());
    assertTrue(design.miss.isReadOnly());
    assertTrue(design.score.isReadOnly());
    assertTrue(design.homol.isReadOnly());
    assertTrue(design.ident.isReadOnly());
    assertTrue(design.expect.isReadOnly());
    assertTrue(design.seq.isReadOnly());
    assertTrue(design.frame.isReadOnly());
    assertTrue(design.varMod.isReadOnly());
    assertTrue(design.numMatch.isReadOnly());
    assertTrue(design.scanTitle.isReadOnly());
    assertTrue(design.showUnassigned.isReadOnly());
    assertTrue(design.showPepDupes.isReadOnly());
  }

  @Test
  public void isValid() {
    presenter.init(view);
    assertTrue(presenter.isValid());
  }

  @Test
  public void getBean_All() {
    presenter.init(view);
    design.master.setValue(true);
    design.expMr.setValue(true);
    design.expZ.setValue(true);
    design.calcMr.setValue(true);
    design.delta.setValue(true);
    design.start.setValue(true);
    design.end.setValue(true);
    design.miss.setValue(true);
    design.score.setValue(true);
    design.homol.setValue(true);
    design.ident.setValue(true);
    design.expect.setValue(true);
    design.seq.setValue(true);
    design.frame.setValue(true);
    design.varMod.setValue(true);
    design.numMatch.setValue(true);
    design.scanTitle.setValue(true);
    design.showUnassigned.setValue(true);
    design.showPepDupes.setValue(true);

    PeptideParameters parameters = presenter.getBean();

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isExpMr());
    assertTrue(parameters.isExpZ());
    assertTrue(parameters.isCalcMr());
    assertTrue(parameters.isDelta());
    assertTrue(parameters.isStart());
    assertTrue(parameters.isEnd());
    assertTrue(parameters.isMiss());
    assertTrue(parameters.isScore());
    assertTrue(parameters.isHomol());
    assertTrue(parameters.isIdent());
    assertTrue(parameters.isExpect());
    assertTrue(parameters.isSeq());
    assertTrue(parameters.isFrame());
    assertTrue(parameters.isVarMod());
    assertTrue(parameters.isNumMatch());
    assertTrue(parameters.isScanTitle());
    assertTrue(parameters.isShowUnassigned());
    assertTrue(parameters.isShowPepDupes());
  }

  @Test
  public void getBean_None() {
    presenter.init(view);
    design.master.setValue(false);
    design.expMr.setValue(false);
    design.expZ.setValue(false);
    design.calcMr.setValue(false);
    design.delta.setValue(false);
    design.start.setValue(false);
    design.end.setValue(false);
    design.miss.setValue(false);
    design.score.setValue(false);
    design.homol.setValue(false);
    design.ident.setValue(false);
    design.expect.setValue(false);
    design.seq.setValue(false);
    design.frame.setValue(false);
    design.varMod.setValue(false);
    design.numMatch.setValue(false);
    design.scanTitle.setValue(false);
    design.showUnassigned.setValue(false);
    design.showPepDupes.setValue(false);

    PeptideParameters parameters = presenter.getBean();

    assertFalse(parameters.isMaster());
    assertFalse(parameters.isExpMr());
    assertFalse(parameters.isExpZ());
    assertFalse(parameters.isCalcMr());
    assertFalse(parameters.isDelta());
    assertFalse(parameters.isStart());
    assertFalse(parameters.isEnd());
    assertFalse(parameters.isMiss());
    assertFalse(parameters.isScore());
    assertFalse(parameters.isHomol());
    assertFalse(parameters.isIdent());
    assertFalse(parameters.isExpect());
    assertFalse(parameters.isSeq());
    assertFalse(parameters.isFrame());
    assertFalse(parameters.isVarMod());
    assertFalse(parameters.isNumMatch());
    assertFalse(parameters.isScanTitle());
    assertFalse(parameters.isShowUnassigned());
    assertFalse(parameters.isShowPepDupes());
  }

  @Test
  public void setBean() {
    presenter.init(view);
    PeptideParameters parameters = new PeptideParameters();
    parameters.setMaster(false);
    parameters.setExpMr(false);
    parameters.setExpZ(false);
    parameters.setCalcMr(false);
    parameters.setDelta(false);
    parameters.setStart(false);
    parameters.setEnd(false);
    parameters.setMiss(false);
    parameters.setScore(false);
    parameters.setHomol(false);
    parameters.setIdent(false);
    parameters.setExpect(false);
    parameters.setSeq(false);
    parameters.setFrame(false);
    parameters.setVarMod(false);
    parameters.setNumMatch(false);
    parameters.setScanTitle(false);
    parameters.setShowUnassigned(true);
    parameters.setShowPepDupes(true);

    presenter.setBean(parameters);

    assertFalse(design.master.getValue());
    assertFalse(design.expMr.getValue());
    assertFalse(design.expZ.getValue());
    assertFalse(design.calcMr.getValue());
    assertFalse(design.delta.getValue());
    assertFalse(design.start.getValue());
    assertFalse(design.end.getValue());
    assertFalse(design.miss.getValue());
    assertFalse(design.score.getValue());
    assertFalse(design.homol.getValue());
    assertFalse(design.ident.getValue());
    assertFalse(design.expect.getValue());
    assertFalse(design.seq.getValue());
    assertFalse(design.frame.getValue());
    assertFalse(design.varMod.getValue());
    assertFalse(design.numMatch.getValue());
    assertFalse(design.scanTitle.getValue());
    assertTrue(design.showUnassigned.getValue());
    assertTrue(design.showPepDupes.getValue());
  }

  @Test
  public void setBean_BeforeInit() {
    PeptideParameters parameters = new PeptideParameters();
    parameters.setMaster(false);
    parameters.setExpMr(false);
    parameters.setExpZ(false);
    parameters.setCalcMr(false);
    parameters.setDelta(false);
    parameters.setStart(false);
    parameters.setEnd(false);
    parameters.setMiss(false);
    parameters.setScore(false);
    parameters.setHomol(false);
    parameters.setIdent(false);
    parameters.setExpect(false);
    parameters.setSeq(false);
    parameters.setFrame(false);
    parameters.setVarMod(false);
    parameters.setNumMatch(false);
    parameters.setScanTitle(false);
    parameters.setShowUnassigned(true);
    parameters.setShowPepDupes(true);

    presenter.setBean(parameters);
    presenter.init(view);

    assertFalse(design.master.getValue());
    assertFalse(design.expMr.getValue());
    assertFalse(design.expZ.getValue());
    assertFalse(design.calcMr.getValue());
    assertFalse(design.delta.getValue());
    assertFalse(design.start.getValue());
    assertFalse(design.end.getValue());
    assertFalse(design.miss.getValue());
    assertFalse(design.score.getValue());
    assertFalse(design.homol.getValue());
    assertFalse(design.ident.getValue());
    assertFalse(design.expect.getValue());
    assertFalse(design.seq.getValue());
    assertFalse(design.frame.getValue());
    assertFalse(design.varMod.getValue());
    assertFalse(design.numMatch.getValue());
    assertFalse(design.scanTitle.getValue());
    assertTrue(design.showUnassigned.getValue());
    assertTrue(design.showPepDupes.getValue());
  }

  @Test
  public void writeBean() throws Throwable {
    presenter.init(view);
    PeptideParameters parameters = new PeptideParameters();

    presenter.writeBean(parameters);

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isExpMr());
    assertTrue(parameters.isExpZ());
    assertTrue(parameters.isCalcMr());
    assertTrue(parameters.isDelta());
    assertTrue(parameters.isStart());
    assertTrue(parameters.isEnd());
    assertTrue(parameters.isMiss());
    assertTrue(parameters.isScore());
    assertFalse(parameters.isHomol());
    assertFalse(parameters.isIdent());
    assertTrue(parameters.isExpect());
    assertTrue(parameters.isSeq());
    assertFalse(parameters.isFrame());
    assertTrue(parameters.isVarMod());
    assertTrue(parameters.isNumMatch());
    assertTrue(parameters.isScanTitle());
    assertFalse(parameters.isShowUnassigned());
    assertFalse(parameters.isShowPepDupes());
  }
}

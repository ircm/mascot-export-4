/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROTEIN_MASTER;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_COVER;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_DESC;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_EMPAI;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_LEN;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_MASS;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_MATCHES;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_PI;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_SCORE;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_SEQ;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_TAX_ID;
import static ca.qc.ircm.mascotexport.job.web.ProteinHitInformationFormPresenter.PROT_TAX_STR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class ProteinHitInformationFormPresenterTest {
  private ProteinHitInformationFormPresenter presenter;
  @Mock
  private ProteinHitInformationForm view;
  private ProteinHitInformationFormDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(ProteinHitInformationForm.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new ProteinHitInformationFormPresenter();
    design = new ProteinHitInformationFormDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
  }

  @Test
  public void defaultValues() {
    presenter.init(view);

    assertTrue(design.master.getValue());
    assertTrue(design.score.getValue());
    assertTrue(design.desc.getValue());
    assertTrue(design.mass.getValue());
    assertTrue(design.matches.getValue());
    assertTrue(design.cover.getValue());
    assertTrue(design.len.getValue());
    assertTrue(design.pi.getValue());
    assertTrue(design.taxStr.getValue());
    assertTrue(design.taxId.getValue());
    assertFalse(design.seq.getValue());
    assertTrue(design.empai.getValue());
  }

  @Test
  public void captions() {
    presenter.init(view);

    assertEquals(resources.message(PROTEIN_MASTER), design.master.getCaption());
    assertEquals(resources.message(PROT_SCORE), design.score.getCaption());
    assertEquals(resources.message(PROT_DESC), design.desc.getCaption());
    assertEquals(resources.message(PROT_MASS), design.mass.getCaption());
    assertEquals(resources.message(PROT_MATCHES), design.matches.getCaption());
    assertEquals(resources.message(PROT_COVER), design.cover.getCaption());
    assertEquals(resources.message(PROT_LEN), design.len.getCaption());
    assertEquals(resources.message(PROT_PI), design.pi.getCaption());
    assertEquals(resources.message(PROT_TAX_STR), design.taxStr.getCaption());
    assertEquals(resources.message(PROT_TAX_ID), design.taxId.getCaption());
    assertEquals(resources.message(PROT_SEQ), design.seq.getCaption());
    assertEquals(resources.message(PROT_EMPAI), design.empai.getCaption());
    assertEquals(resources.message("slow"), design.slow.getValue());
    assertEquals(resources.message("verySlow"), design.verySlow.getValue());
  }

  @Test
  public void enableFields() {
    presenter.init(view);
    design.master.setValue(true);

    assertTrue(design.master.isEnabled());
    assertTrue(design.score.isEnabled());
    assertTrue(design.desc.isEnabled());
    assertTrue(design.mass.isEnabled());
    assertTrue(design.matches.isEnabled());
    assertTrue(design.cover.isEnabled());
    assertTrue(design.len.isEnabled());
    assertTrue(design.pi.isEnabled());
    assertTrue(design.taxStr.isEnabled());
    assertTrue(design.taxId.isEnabled());
    assertTrue(design.seq.isEnabled());
    assertTrue(design.empai.isEnabled());
  }

  @Test
  public void disableFields() {
    presenter.init(view);
    design.master.setValue(false);

    assertTrue(design.master.isEnabled());
    assertFalse(design.score.isEnabled());
    assertFalse(design.desc.isEnabled());
    assertFalse(design.mass.isEnabled());
    assertFalse(design.matches.isEnabled());
    assertFalse(design.cover.isEnabled());
    assertFalse(design.len.isEnabled());
    assertFalse(design.pi.isEnabled());
    assertFalse(design.taxStr.isEnabled());
    assertFalse(design.taxId.isEnabled());
    assertFalse(design.seq.isEnabled());
    assertFalse(design.empai.isEnabled());
  }

  @Test
  public void isReadOnly_Default() {
    presenter.init(view);

    assertFalse(presenter.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    presenter.init(view);
    presenter.setReadOnly(true);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.score.isReadOnly());
    assertTrue(design.desc.isReadOnly());
    assertTrue(design.mass.isReadOnly());
    assertTrue(design.matches.isReadOnly());
    assertTrue(design.cover.isReadOnly());
    assertTrue(design.len.isReadOnly());
    assertTrue(design.pi.isReadOnly());
    assertTrue(design.taxStr.isReadOnly());
    assertTrue(design.taxId.isReadOnly());
    assertTrue(design.seq.isReadOnly());
    assertTrue(design.empai.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    presenter.init(view);
    presenter.setReadOnly(false);

    assertFalse(presenter.isReadOnly());
    assertFalse(design.master.isReadOnly());
    assertFalse(design.score.isReadOnly());
    assertFalse(design.desc.isReadOnly());
    assertFalse(design.mass.isReadOnly());
    assertFalse(design.matches.isReadOnly());
    assertFalse(design.cover.isReadOnly());
    assertFalse(design.len.isReadOnly());
    assertFalse(design.pi.isReadOnly());
    assertFalse(design.taxStr.isReadOnly());
    assertFalse(design.taxId.isReadOnly());
    assertFalse(design.seq.isReadOnly());
    assertFalse(design.empai.isReadOnly());
  }

  @Test
  public void setReadOnly_BeforeInit() {
    presenter.setReadOnly(true);
    presenter.init(view);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.score.isReadOnly());
    assertTrue(design.desc.isReadOnly());
    assertTrue(design.mass.isReadOnly());
    assertTrue(design.matches.isReadOnly());
    assertTrue(design.cover.isReadOnly());
    assertTrue(design.len.isReadOnly());
    assertTrue(design.pi.isReadOnly());
    assertTrue(design.taxStr.isReadOnly());
    assertTrue(design.taxId.isReadOnly());
    assertTrue(design.seq.isReadOnly());
    assertTrue(design.empai.isReadOnly());
  }

  @Test
  public void isValid() {
    presenter.init(view);

    assertTrue(presenter.isValid());
  }

  @Test
  public void getBean_All() {
    presenter.init(view);
    design.master.setValue(true);
    design.score.setValue(true);
    design.desc.setValue(true);
    design.mass.setValue(true);
    design.matches.setValue(true);
    design.cover.setValue(true);
    design.len.setValue(true);
    design.pi.setValue(true);
    design.taxStr.setValue(true);
    design.taxId.setValue(true);
    design.seq.setValue(true);
    design.empai.setValue(true);

    ProteinParameters parameters = presenter.getBean();

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isScore());
    assertTrue(parameters.isDesc());
    assertTrue(parameters.isMass());
    assertTrue(parameters.isMatches());
    assertTrue(parameters.isCover());
    assertTrue(parameters.isLen());
    assertTrue(parameters.isPi());
    assertTrue(parameters.isTaxStr());
    assertTrue(parameters.isTaxId());
    assertTrue(parameters.isSeq());
    assertTrue(parameters.isEmpai());
  }

  @Test
  public void getBean_None() {
    presenter.init(view);
    design.master.setValue(false);
    design.score.setValue(false);
    design.desc.setValue(false);
    design.mass.setValue(false);
    design.matches.setValue(false);
    design.cover.setValue(false);
    design.len.setValue(false);
    design.pi.setValue(false);
    design.taxStr.setValue(false);
    design.taxId.setValue(false);
    design.seq.setValue(false);
    design.empai.setValue(false);

    ProteinParameters parameters = presenter.getBean();

    assertFalse(parameters.isMaster());
    assertFalse(parameters.isScore());
    assertFalse(parameters.isDesc());
    assertFalse(parameters.isMass());
    assertFalse(parameters.isMatches());
    assertFalse(parameters.isCover());
    assertFalse(parameters.isLen());
    assertFalse(parameters.isPi());
    assertFalse(parameters.isTaxStr());
    assertFalse(parameters.isTaxId());
    assertFalse(parameters.isSeq());
    assertFalse(parameters.isEmpai());
  }

  @Test
  public void setBean() {
    presenter.init(view);
    ProteinParameters parameters = new ProteinParameters();
    parameters.setMaster(false);
    parameters.setScore(false);
    parameters.setDesc(false);
    parameters.setMass(false);
    parameters.setMatches(false);
    parameters.setCover(false);
    parameters.setLen(false);
    parameters.setPi(false);
    parameters.setTaxStr(false);
    parameters.setTaxId(false);
    parameters.setSeq(true);
    parameters.setEmpai(true);

    presenter.setBean(parameters);

    assertFalse(design.master.getValue());
    assertFalse(design.score.getValue());
    assertFalse(design.desc.getValue());
    assertFalse(design.mass.getValue());
    assertFalse(design.matches.getValue());
    assertFalse(design.cover.getValue());
    assertFalse(design.len.getValue());
    assertFalse(design.pi.getValue());
    assertFalse(design.taxStr.getValue());
    assertFalse(design.taxId.getValue());
    assertTrue(design.seq.getValue());
    assertTrue(design.empai.getValue());
  }

  @Test
  public void setBean_BeforeInit() {
    ProteinParameters parameters = new ProteinParameters();
    parameters.setMaster(false);
    parameters.setScore(false);
    parameters.setDesc(false);
    parameters.setMass(false);
    parameters.setMatches(false);
    parameters.setCover(false);
    parameters.setLen(false);
    parameters.setPi(false);
    parameters.setTaxStr(false);
    parameters.setTaxId(false);
    parameters.setSeq(true);
    parameters.setEmpai(true);

    presenter.setBean(parameters);
    presenter.init(view);

    assertFalse(design.master.getValue());
    assertFalse(design.score.getValue());
    assertFalse(design.desc.getValue());
    assertFalse(design.mass.getValue());
    assertFalse(design.matches.getValue());
    assertFalse(design.cover.getValue());
    assertFalse(design.len.getValue());
    assertFalse(design.pi.getValue());
    assertFalse(design.taxStr.getValue());
    assertFalse(design.taxId.getValue());
    assertTrue(design.seq.getValue());
    assertTrue(design.empai.getValue());
  }

  @Test
  public void writeBean() throws Throwable {
    presenter.init(view);
    ProteinParameters parameters = new ProteinParameters();

    presenter.writeBean(parameters);

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isScore());
    assertTrue(parameters.isDesc());
    assertTrue(parameters.isMass());
    assertTrue(parameters.isMatches());
    assertTrue(parameters.isCover());
    assertTrue(parameters.isLen());
    assertTrue(parameters.isPi());
    assertTrue(parameters.isTaxStr());
    assertTrue(parameters.isTaxId());
    assertFalse(parameters.isSeq());
    assertTrue(parameters.isEmpai());
  }
}

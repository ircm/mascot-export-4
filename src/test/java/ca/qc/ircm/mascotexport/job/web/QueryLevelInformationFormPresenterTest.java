/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_MASTER;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_PARAMS;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_PEAKS;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_QUALIFIERS;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_RAW;
import static ca.qc.ircm.mascotexport.job.web.QueryLevelInformationFormPresenter.QUERY_TITLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class QueryLevelInformationFormPresenterTest {
  private QueryLevelInformationFormPresenter presenter;
  @Mock
  private QueryLevelInformationForm view;
  private QueryLevelInformationFormDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(QueryLevelInformationForm.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new QueryLevelInformationFormPresenter();
    design = new QueryLevelInformationFormDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
  }

  @Test
  public void defaultValues() {
    presenter.init(view);

    assertFalse(design.master.getValue());
    assertFalse(design.title.getValue());
    assertFalse(design.qualifiers.getValue());
    assertFalse(design.params.getValue());
    assertFalse(design.peaks.getValue());
    assertFalse(design.raw.getValue());
  }

  @Test
  public void captions() {
    presenter.init(view);

    assertEquals(resources.message(QUERY_MASTER), design.master.getCaption());
    assertEquals(resources.message(QUERY_TITLE), design.title.getCaption());
    assertEquals(resources.message(QUERY_QUALIFIERS), design.qualifiers.getCaption());
    assertEquals(resources.message(QUERY_PARAMS), design.params.getCaption());
    assertEquals(resources.message(QUERY_PEAKS), design.peaks.getCaption());
    assertEquals(resources.message(QUERY_RAW), design.raw.getCaption());
  }

  @Test
  public void enableFields() {
    presenter.init(view);
    design.master.setValue(true);

    assertTrue(design.master.isEnabled());
    assertTrue(design.title.isEnabled());
    assertTrue(design.qualifiers.isEnabled());
    assertTrue(design.params.isEnabled());
    assertTrue(design.peaks.isEnabled());
    assertTrue(design.raw.isEnabled());
  }

  @Test
  public void disableFields() {
    presenter.init(view);
    design.master.setValue(false);

    assertTrue(design.master.isEnabled());
    assertFalse(design.title.isEnabled());
    assertFalse(design.qualifiers.isEnabled());
    assertFalse(design.params.isEnabled());
    assertFalse(design.peaks.isEnabled());
    assertFalse(design.raw.isEnabled());
  }

  @Test
  public void isReadOnly_Default() {
    presenter.init(view);

    assertFalse(presenter.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    presenter.init(view);
    presenter.setReadOnly(true);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.title.isReadOnly());
    assertTrue(design.qualifiers.isReadOnly());
    assertTrue(design.params.isReadOnly());
    assertTrue(design.peaks.isReadOnly());
    assertTrue(design.raw.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    presenter.init(view);
    presenter.setReadOnly(false);

    assertFalse(presenter.isReadOnly());
    assertFalse(design.master.isReadOnly());
    assertFalse(design.title.isReadOnly());
    assertFalse(design.qualifiers.isReadOnly());
    assertFalse(design.params.isReadOnly());
    assertFalse(design.peaks.isReadOnly());
    assertFalse(design.raw.isReadOnly());
  }

  @Test
  public void setReadOnly_BeforeInit() {
    presenter.setReadOnly(true);
    presenter.init(view);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.title.isReadOnly());
    assertTrue(design.qualifiers.isReadOnly());
    assertTrue(design.params.isReadOnly());
    assertTrue(design.peaks.isReadOnly());
    assertTrue(design.raw.isReadOnly());
  }

  @Test
  public void isValid() {
    presenter.init(view);
    assertTrue(presenter.isValid());
  }

  @Test
  public void getBean_All() {
    presenter.init(view);
    design.master.setValue(true);
    design.title.setValue(true);
    design.qualifiers.setValue(true);
    design.params.setValue(true);
    design.peaks.setValue(true);
    design.raw.setValue(true);

    QueryParameters parameters = presenter.getBean();

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isTitle());
    assertTrue(parameters.isQualifiers());
    assertTrue(parameters.isParams());
    assertTrue(parameters.isPeaks());
    assertTrue(parameters.isRaw());
  }

  @Test
  public void getBean_None() {
    presenter.init(view);
    design.master.setValue(false);
    design.title.setValue(false);
    design.qualifiers.setValue(false);
    design.params.setValue(false);
    design.peaks.setValue(false);
    design.raw.setValue(false);

    QueryParameters parameters = presenter.getBean();

    assertFalse(parameters.isMaster());
    assertFalse(parameters.isTitle());
    assertFalse(parameters.isQualifiers());
    assertFalse(parameters.isParams());
    assertFalse(parameters.isPeaks());
    assertFalse(parameters.isRaw());
  }

  @Test
  public void setBean() {
    presenter.init(view);
    QueryParameters parameters = new QueryParameters();
    parameters.setMaster(false);
    parameters.setTitle(false);
    parameters.setQualifiers(false);
    parameters.setParams(false);
    parameters.setPeaks(true);
    parameters.setRaw(true);

    presenter.setBean(parameters);

    assertFalse(design.master.getValue());
    assertFalse(design.title.getValue());
    assertFalse(design.qualifiers.getValue());
    assertFalse(design.params.getValue());
    assertTrue(design.peaks.getValue());
    assertTrue(design.raw.getValue());
  }

  @Test
  public void setBean_BeforeInit() {
    QueryParameters parameters = new QueryParameters();
    parameters.setMaster(false);
    parameters.setTitle(false);
    parameters.setQualifiers(false);
    parameters.setParams(false);
    parameters.setPeaks(true);
    parameters.setRaw(true);

    presenter.setBean(parameters);
    presenter.init(view);

    assertFalse(design.master.getValue());
    assertFalse(design.title.getValue());
    assertFalse(design.qualifiers.getValue());
    assertFalse(design.params.getValue());
    assertTrue(design.peaks.getValue());
    assertTrue(design.raw.getValue());
  }

  @Test
  public void writeBean() throws Throwable {
    presenter.init(view);
    QueryParameters parameters = new QueryParameters();

    presenter.writeBean(parameters);

    assertFalse(parameters.isMaster());
    assertFalse(parameters.isTitle());
    assertFalse(parameters.isQualifiers());
    assertFalse(parameters.isParams());
    assertFalse(parameters.isPeaks());
    assertFalse(parameters.isRaw());
  }
}

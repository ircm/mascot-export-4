/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SEARCH_MASTER;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_FORMAT;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_HEADER;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_MASSES;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_MODS;
import static ca.qc.ircm.mascotexport.job.web.SearchInformationFormPresenter.SHOW_PARAMS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class SearchInformationFormPresenterTest {
  private SearchInformationFormPresenter presenter;
  @Mock
  private SearchInformationForm view;
  private SearchInformationFormDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(SearchInformationForm.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new SearchInformationFormPresenter();
    design = new SearchInformationFormDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
  }

  @Test
  public void defaultValues() {
    presenter.init(view);

    assertTrue(design.master.getValue());
    assertTrue(design.showHeader.getValue());
    assertTrue(design.showMods.getValue());
    assertTrue(design.showParams.getValue());
    assertTrue(design.showFormat.getValue());
    assertFalse(design.showMasses.getValue());
  }

  @Test
  public void captions() {
    presenter.init(view);

    assertEquals(resources.message(SEARCH_MASTER), design.master.getCaption());
    assertEquals(resources.message(SHOW_HEADER), design.showHeader.getCaption());
    assertEquals(resources.message(SHOW_MODS), design.showMods.getCaption());
    assertEquals(resources.message(SHOW_PARAMS), design.showParams.getCaption());
    assertEquals(resources.message(SHOW_FORMAT), design.showFormat.getCaption());
    assertEquals(resources.message(SHOW_MASSES), design.showMasses.getCaption());
  }

  @Test
  public void enableFields() {
    presenter.init(view);
    design.master.setValue(true);

    assertTrue(design.master.isEnabled());
    assertTrue(design.showHeader.isEnabled());
    assertTrue(design.showMods.isEnabled());
    assertTrue(design.showParams.isEnabled());
    assertTrue(design.showFormat.isEnabled());
    assertTrue(design.showMasses.isEnabled());
  }

  @Test
  public void disableFields() {
    presenter.init(view);
    design.master.setValue(false);

    assertTrue(design.master.isEnabled());
    assertFalse(design.showHeader.isEnabled());
    assertFalse(design.showMods.isEnabled());
    assertFalse(design.showParams.isEnabled());
    assertFalse(design.showFormat.isEnabled());
    assertFalse(design.showMasses.isEnabled());
  }

  @Test
  public void isReadOnly_Default() {
    presenter.init(view);

    assertFalse(presenter.isReadOnly());
  }

  @Test
  public void setReadOnly_True() {
    presenter.init(view);
    presenter.setReadOnly(true);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.showHeader.isReadOnly());
    assertTrue(design.showMods.isReadOnly());
    assertTrue(design.showParams.isReadOnly());
    assertTrue(design.showFormat.isReadOnly());
    assertTrue(design.showMasses.isReadOnly());
  }

  @Test
  public void setReadOnly_False() {
    presenter.init(view);
    presenter.setReadOnly(false);

    assertFalse(presenter.isReadOnly());
    assertFalse(design.master.isReadOnly());
    assertFalse(design.showHeader.isReadOnly());
    assertFalse(design.showMods.isReadOnly());
    assertFalse(design.showParams.isReadOnly());
    assertFalse(design.showFormat.isReadOnly());
    assertFalse(design.showMasses.isReadOnly());
  }

  @Test
  public void setReadOnly_BeforeInit() {
    presenter.setReadOnly(true);
    presenter.init(view);

    assertTrue(presenter.isReadOnly());
    assertTrue(design.master.isReadOnly());
    assertTrue(design.showHeader.isReadOnly());
    assertTrue(design.showMods.isReadOnly());
    assertTrue(design.showParams.isReadOnly());
    assertTrue(design.showFormat.isReadOnly());
    assertTrue(design.showMasses.isReadOnly());
  }

  @Test
  public void isValid() {
    presenter.init(view);

    assertTrue(presenter.isValid());
  }

  @Test
  public void getBean_All() {
    presenter.init(view);
    design.master.setValue(true);
    design.showHeader.setValue(true);
    design.showMods.setValue(true);
    design.showParams.setValue(true);
    design.showFormat.setValue(true);
    design.showMasses.setValue(true);

    SearchParameters parameters = presenter.getBean();

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isShowHeader());
    assertTrue(parameters.isShowMods());
    assertTrue(parameters.isShowParams());
    assertTrue(parameters.isShowFormat());
    assertTrue(parameters.isShowMasses());
  }

  @Test
  public void getBean_None() {
    presenter.init(view);
    design.master.setValue(false);
    design.showHeader.setValue(false);
    design.showMods.setValue(false);
    design.showParams.setValue(false);
    design.showFormat.setValue(false);
    design.showMasses.setValue(false);

    SearchParameters parameters = presenter.getBean();

    assertFalse(parameters.isMaster());
    assertFalse(parameters.isShowHeader());
    assertFalse(parameters.isShowMods());
    assertFalse(parameters.isShowParams());
    assertFalse(parameters.isShowFormat());
    assertFalse(parameters.isShowMasses());
  }

  @Test
  public void setBean() {
    presenter.init(view);
    SearchParameters parameters = new SearchParameters();
    parameters.setMaster(false);
    parameters.setShowHeader(false);
    parameters.setShowMods(false);
    parameters.setShowParams(false);
    parameters.setShowFormat(true);
    parameters.setShowMasses(true);

    presenter.setBean(parameters);

    assertFalse(design.master.getValue());
    assertFalse(design.showHeader.getValue());
    assertFalse(design.showMods.getValue());
    assertFalse(design.showParams.getValue());
    assertTrue(design.showFormat.getValue());
    assertTrue(design.showMasses.getValue());
  }

  @Test
  public void setBean_BeforeInit() {
    SearchParameters parameters = new SearchParameters();
    parameters.setMaster(false);
    parameters.setShowHeader(false);
    parameters.setShowMods(false);
    parameters.setShowParams(false);
    parameters.setShowFormat(true);
    parameters.setShowMasses(true);

    presenter.setBean(parameters);
    presenter.init(view);

    assertFalse(design.master.getValue());
    assertFalse(design.showHeader.getValue());
    assertFalse(design.showMods.getValue());
    assertFalse(design.showParams.getValue());
    assertTrue(design.showFormat.getValue());
    assertTrue(design.showMasses.getValue());
  }

  @Test
  public void writeBean() throws Throwable {
    presenter.init(view);
    SearchParameters parameters = new SearchParameters();

    presenter.writeBean(parameters);

    assertTrue(parameters.isMaster());
    assertTrue(parameters.isShowHeader());
    assertTrue(parameters.isShowMods());
    assertTrue(parameters.isShowParams());
    assertTrue(parameters.isShowFormat());
    assertFalse(parameters.isShowMasses());
  }
}

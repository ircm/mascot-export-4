/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.SearchParameters;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import com.vaadin.data.BinderValidationStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class SearchInformationFormTest {
  private SearchInformationForm view;
  @Mock
  private SearchInformationFormPresenter presenter;
  @Mock
  private SearchParameters bean;
  @Mock
  private BinderValidationStatus<SearchParameters> status;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    view = new SearchInformationForm(presenter);
  }

  @Test
  public void isReadOnly_False() {
    assertFalse(view.isReadOnly());

    verify(presenter).isReadOnly();
  }

  @Test
  public void isReadOnly_True() {
    when(presenter.isReadOnly()).thenReturn(true);

    assertTrue(view.isReadOnly());

    verify(presenter).isReadOnly();
  }

  @Test
  public void setReadOnly() {
    view.setReadOnly(true);

    verify(presenter).setReadOnly(true);
  }

  @Test
  public void getBean() {
    when(presenter.getBean()).thenReturn(bean);

    SearchParameters bean = view.getBean();

    verify(presenter).getBean();
    assertSame(this.bean, bean);
  }

  @Test
  public void setBean() {
    view.setBean(bean);

    verify(presenter).setBean(bean);
  }

  @Test
  public void writeBean() throws Throwable {
    view.writeBean(bean);

    verify(presenter).writeBean(bean);
  }

  @Test
  public void validate() {
    when(presenter.validate()).thenReturn(status);

    BinderValidationStatus<SearchParameters> status = view.validate();

    verify(presenter).validate();
    assertSame(this.status, status);
  }
}

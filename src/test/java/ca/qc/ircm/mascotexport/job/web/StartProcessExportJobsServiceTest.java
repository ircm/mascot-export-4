/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.job.web;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ProcessExportJobService;
import ca.qc.ircm.mascotexport.task.web.ShowTaskProgressEvent;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.progressbar.ProgressBarWithListeners;
import ca.qc.ircm.progressbar.Property;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskContext;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import java.util.Collection;
import java.util.Locale;
import java.util.function.BiConsumer;
import javax.inject.Provider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class StartProcessExportJobsServiceTest {
  private StartProcessExportJobsService startProcessExportJobsService;
  @Mock
  private UI ui;
  @Mock
  private ProcessExportJobService processJobService;
  @Mock
  private Provider<ExportProgressWindow> exportProgressWindowProvider;
  @Mock
  private ExportProgressWindow exportProgressWindow;
  @Mock
  private ApplicationEventPublisher publisher;
  @Mock
  private Task task;
  @Mock
  private TaskContext taskContext;
  @Mock
  private ProgressBarWithListeners progressBar;
  @Mock
  private Component component;
  @Mock
  private Collection<ExportJob> jobs;
  @Mock
  private Property<String> title;
  @Mock
  private Property<String> message;
  @Mock
  private Property<Double> progress;
  @Captor
  private ArgumentCaptor<ExportProgressWindow> windowCaptor;
  @Captor
  private ArgumentCaptor<ClickListener> clickListenerCaptor;
  private Locale locale = Locale.ENGLISH;

  /**
   * Configuration before test.
   */
  @Before
  public void beforeTest() {
    startProcessExportJobsService =
        new StartProcessExportJobsService(ui, processJobService, exportProgressWindowProvider);
    when(exportProgressWindowProvider.get()).thenReturn(exportProgressWindow);
    when(task.getContext()).thenReturn(taskContext);
    when(taskContext.getProgressBar()).thenReturn(progressBar);
    when(ui.getLocale()).thenReturn(locale);
    when(progressBar.title()).thenReturn(title);
    when(progressBar.message()).thenReturn(message);
    when(progressBar.progress()).thenReturn(progress);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void onStartProcessJobsEvent() {
    StartProcessExportJobsEvent event = new StartProcessExportJobsEvent(component, jobs);
    when(processJobService.process(any(), any(), any())).thenReturn(task);

    startProcessExportJobsService.onStartProcessJobsEvent(event);

    verify(processJobService).process(eq(jobs), any(ProgressBarWithListeners.class), eq(locale));
    verify(exportProgressWindowProvider).get();
    verify(exportProgressWindow).addCancelledClickListener(clickListenerCaptor.capture());
    ClickListener cancelListener = clickListenerCaptor.getValue();
    cancelListener.buttonClick(new ClickEvent(new Button()));
    verify(task).cancel();
    verify(progressBar).title();
    verify(title).addListener(any(BiConsumer.class));
    verify(progressBar).message();
    verify(message).addListener(any(BiConsumer.class));
    verify(progressBar).progress();
    verify(progress).addListener(any(BiConsumer.class));
    verify(ui).addWindow(windowCaptor.capture());
    assertTrue(windowCaptor.getValue() instanceof ExportProgressWindow);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void onShowTaskProgressEvent() {
    ShowTaskProgressEvent event = new ShowTaskProgressEvent(component, task);

    startProcessExportJobsService.onShowTaskProgressEvent(event);

    verify(exportProgressWindowProvider).get();
    verify(exportProgressWindow).addCancelledClickListener(clickListenerCaptor.capture());
    ClickListener cancelListener = clickListenerCaptor.getValue();
    cancelListener.buttonClick(new ClickEvent(new Button()));
    verify(task).cancel();
    verify(progressBar).title();
    verify(title).addListener(any(BiConsumer.class));
    verify(progressBar).message();
    verify(message).addListener(any(BiConsumer.class));
    verify(progressBar).progress();
    verify(progress).addListener(any(BiConsumer.class));
    verify(ui).addWindow(windowCaptor.capture());
    assertTrue(windowCaptor.getValue() instanceof ExportProgressWindow);
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.MascotExportConfiguration;
import ca.qc.ircm.mascotexport.exec.ExecutorProvider;
import ca.qc.ircm.mascotexport.mascot.parser.MascotParser;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotServiceTest {
  private MascotService mascotService;
  @Mock
  private MascotConfiguration mascotConfiguration;
  @Mock
  private MascotParser mascotParser;
  @Mock
  private MascotExportConfiguration mascotExportConfiguration;
  @Mock
  private ExecutorProvider executorProvider;
  @Mock
  private Executor executor;
  @Mock
  private Process process;
  @Captor
  private ArgumentCaptor<CommandLine> commandLineCaptor;
  @Captor
  private ArgumentCaptor<ExecuteStreamHandler> streamHandlerCaptor;
  private Map<String, List<String>> parameters;
  @Rule
  public TemporaryFolder temporaryFolder = new TemporaryFolder();
  private Path home;
  private Path datRoot;
  private Path working;
  private Path output;
  private Path executable;
  private List<String> arguments;

  /**
   * Before tests.
   */
  @Before
  public void beforeTest() throws Throwable {
    mascotService = new MascotService(mascotConfiguration, mascotParser, mascotExportConfiguration,
        executorProvider);
    parameters = new HashMap<>();
    parameters.put("param1", Arrays.asList("1", "2"));
    parameters.put("param2", Arrays.asList("abc"));
    when(executorProvider.get()).thenReturn(executor);
    home = temporaryFolder.getRoot().toPath();
    datRoot = Files.createDirectory(home.resolve("dat"));
    working = Files.createDirectory(home.resolve("working"));
    output = Files.createDirectory(home.resolve("output"));
    when(mascotConfiguration.getData()).thenReturn(datRoot);
    when(mascotExportConfiguration.getWorking()).thenReturn(working);
    when(mascotExportConfiguration.getOutput()).thenReturn(output);
    executable = Paths.get(getClass().getResource("/export.pl").toURI());
    when(mascotExportConfiguration.getExecutable()).thenReturn(executable);
    arguments = new ArrayList<>();
    arguments.add("test1");
    arguments.add("test2");
    when(mascotExportConfiguration.getArgs()).thenReturn(arguments);
  }

  @Test
  public void allFiles() throws Throwable {
    Path file1 = Files.createFile(datRoot.resolve("1.dat"));
    Path folder1 = Files.createDirectory(datRoot.resolve("a"));
    Path file11 = Files.createFile(folder1.resolve("1.dat"));
    Path folder2 = Files.createDirectory(datRoot.resolve("b"));
    final Path file21 = Files.createFile(folder2.resolve("2.dat"));
    final Path file22 = Files.createFile(folder2.resolve("21.dat"));

    Collection<Path> files = mascotService.allFiles();

    assertEquals(4, files.size());
    assertEquals(true, files.contains(datRoot.relativize(file1)));
    assertEquals(true, files.contains(datRoot.relativize(file11)));
    assertEquals(true, files.contains(datRoot.relativize(file21)));
    assertEquals(true, files.contains(datRoot.relativize(file22)));
  }

  @Test
  public void allFiles_DataDirectoryNotExists() throws Throwable {
    Files.delete(datRoot);

    Collection<Path> files = mascotService.allFiles();

    assertTrue(files.isEmpty());
  }

  @Test
  public void allFilesWithInput() throws Throwable {
    final Path file1 = Files.createFile(datRoot.resolve("1.dat"));
    final Path folder1 = Files.createDirectory(datRoot.resolve("a"));
    final Path file11 = Files.createFile(folder1.resolve("1.dat"));
    final Path folder2 = Files.createDirectory(datRoot.resolve("b"));
    final Path file21 = Files.createFile(folder2.resolve("2.dat"));
    final Path file22 = Files.createFile(folder2.resolve("21.dat"));
    when(mascotParser.parseFilename(any()))
        .thenAnswer(i -> i.getArgumentAt(0, Path.class).getFileName().toString() + ".raw");

    Collection<MascotFile> files = mascotService.allFilesWithInput();

    assertEquals(4, files.size());
    BiFunction<MascotFile, Path, Boolean> filter = (mfile, file) -> mfile.file.equals(file);
    Optional<MascotFile> optionalMascotFile =
        files.stream().filter(mfile -> filter.apply(mfile, datRoot.relativize(file1))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("1.dat.raw", optionalMascotFile.get().inputFile);
    optionalMascotFile =
        files.stream().filter(file -> filter.apply(file, datRoot.relativize(file11))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("1.dat.raw", optionalMascotFile.get().inputFile);
    optionalMascotFile =
        files.stream().filter(file -> filter.apply(file, datRoot.relativize(file21))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("2.dat.raw", optionalMascotFile.get().inputFile);
    optionalMascotFile =
        files.stream().filter(file -> filter.apply(file, datRoot.relativize(file22))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("21.dat.raw", optionalMascotFile.get().inputFile);
  }

  @Test
  public void allFilesWithInput_DataDirectoryNotExists() throws Throwable {
    Files.delete(datRoot);

    Collection<MascotFile> files = mascotService.allFilesWithInput();

    assertTrue(files.isEmpty());
  }

  @Test
  public void allFilesWithInput_SomeInvalid() throws Throwable {
    final Path file1 = Files.createFile(datRoot.resolve("1.dat"));
    final Path folder1 = Files.createDirectory(datRoot.resolve("a"));
    final Path file11 = Files.createFile(folder1.resolve("1.dat"));
    final Path folder2 = Files.createDirectory(datRoot.resolve("b"));
    final Path file21 = Files.createFile(folder2.resolve("2.dat"));
    final Path file22 = Files.createFile(folder2.resolve("21.dat"));
    when(mascotParser.parseFilename(any())).thenAnswer(i -> {
      Path input = i.getArgumentAt(0, Path.class);
      if (input.equals(file21)) {
        throw new IOException("Could not parse file " + input);
      }
      return input.getFileName().toString() + ".raw";
    });

    Collection<MascotFile> files = mascotService.allFilesWithInput();

    assertEquals(3, files.size());
    BiFunction<MascotFile, Path, Boolean> filter = (mfile, file) -> mfile.file.equals(file);
    Optional<MascotFile> optionalMascotFile =
        files.stream().filter(mfile -> filter.apply(mfile, datRoot.relativize(file1))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("1.dat.raw", optionalMascotFile.get().inputFile);
    optionalMascotFile =
        files.stream().filter(file -> filter.apply(file, datRoot.relativize(file11))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("1.dat.raw", optionalMascotFile.get().inputFile);
    optionalMascotFile =
        files.stream().filter(file -> filter.apply(file, datRoot.relativize(file21))).findFirst();
    assertEquals(false, optionalMascotFile.isPresent());
    optionalMascotFile =
        files.stream().filter(file -> filter.apply(file, datRoot.relativize(file22))).findFirst();
    assertEquals(true, optionalMascotFile.isPresent());
    assertEquals("21.dat.raw", optionalMascotFile.get().inputFile);
  }

  @Test
  public void allFilesPath() throws Throwable {
    Files.createFile(datRoot.resolve("1.dat"));
    Path folder1 = Files.createDirectory(datRoot.resolve("a"));
    Files.createFile(folder1.resolve("1.dat"));
    Path folder2 = Files.createDirectory(datRoot.resolve("b"));
    Files.createFile(folder2.resolve("2.dat"));
    Files.createFile(folder2.resolve("21.dat"));

    Collection<String> paths = mascotService.allFilesPath();

    assertEquals(4, paths.size());
    assertEquals(true, paths.contains("1.dat"));
    assertEquals(true, paths.contains("a/1.dat"));
    assertEquals(true, paths.contains("b/2.dat"));
    assertEquals(true, paths.contains("b/21.dat"));
  }

  @Test
  public void allFilesPath_DataDirectoryNotExists() throws Throwable {
    Files.delete(datRoot);

    Collection<String> files = mascotService.allFilesPath();

    assertTrue(files.isEmpty());
  }

  @Test
  public void export() throws Throwable {
    Path file = Paths.get(this.getClass().getResource("/F011906.dat").toURI());
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ExecuteResultHandler handler = (ExecuteResultHandler) invocation.getArguments()[1];
        handler.onProcessComplete(23);
        return null;
      }
    }).when(executor).execute(any(CommandLine.class), any(ExecuteResultHandler.class));

    mascotService.export(file, parameters, output);

    verify(executor).setWorkingDirectory(mascotExportConfiguration.getWorking().toFile());
    verify(executor).setWatchdog(any(ExecuteWatchdog.class));
    verify(executor).setStreamHandler(streamHandlerCaptor.capture());
    verify(executor).execute(commandLineCaptor.capture(), any(ExecuteResultHandler.class));
    verify(executor).isFailure(23);
    CommandLine commandLine = commandLineCaptor.getValue();
    assertEquals(mascotExportConfiguration.getExecutable(), Paths.get(commandLine.getExecutable()));
    String[] nonCommandArguments = commandLine.getArguments();
    List<String> expectedNonCommandArguments = new ArrayList<>(mascotExportConfiguration.getArgs());
    expectedNonCommandArguments.add("file=" + FilenameUtils.separatorsToUnix(file.toString()));
    parameters.forEach((key, values) -> values
        .forEach(value -> expectedNonCommandArguments.add(key + "=" + value)));
    assertArrayEquals(expectedNonCommandArguments.toArray(new String[0]), nonCommandArguments);
    ExecuteStreamHandler streamHandler = streamHandlerCaptor.getValue();
    assertTrue(streamHandler instanceof PumpStreamHandler);
    Method streamHandlerOutputMethod = PumpStreamHandler.class.getDeclaredMethod("getOut");
    streamHandlerOutputMethod.setAccessible(true);
    assertEquals(output, streamHandlerOutputMethod.invoke(streamHandler));
  }

  @Test(expected = IOException.class)
  public void export_Fail() throws Throwable {
    Path file = Paths.get(this.getClass().getResource("/F011906.dat").toURI());
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ExecuteResultHandler handler = (ExecuteResultHandler) invocation.getArguments()[1];
        handler.onProcessComplete(23);
        return null;
      }
    }).when(executor).execute(any(CommandLine.class), any(ExecuteResultHandler.class));
    when(executor.isFailure(23)).thenReturn(true);

    mascotService.export(file, parameters, output);
  }

  @Test
  public void export_Interrupted() throws Throwable {
    Path file = Paths.get(this.getClass().getResource("/F011906.dat").toURI());
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Thread.currentThread().interrupt();
        return null;
      }
    }).when(executor).execute(any(CommandLine.class), any(ExecuteResultHandler.class));
    doAnswer(new Answer<Void>() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        ExecuteWatchdog watchdog = (ExecuteWatchdog) invocation.getArguments()[0];
        watchdog.start(process);
        when(executor.getWatchdog()).thenReturn(watchdog);
        return null;
      }
    }).when(executor).setWatchdog(any(ExecuteWatchdog.class));
    when(process.exitValue()).thenThrow(new IllegalThreadStateException());

    try {
      mascotService.export(file, parameters, output);
      fail("Excepted InterruptedException");
    } catch (InterruptedException exception) {
      // Ignore.
    }

    verify(executor).setWorkingDirectory(mascotExportConfiguration.getWorking().toFile());
    verify(executor).setWatchdog(any(ExecuteWatchdog.class));
    verify(executor).setStreamHandler(streamHandlerCaptor.capture());
    verify(executor).execute(commandLineCaptor.capture(), any(ExecuteResultHandler.class));
    verify(process).destroy();
  }
}

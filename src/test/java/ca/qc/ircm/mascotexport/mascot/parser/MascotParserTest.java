/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test {@link MascotParser}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotParserTest {
  @Inject
  private MascotParser parser;

  @Test
  public void parseFilename() throws Exception {
    Path datFile = Paths.get(MascotParserTest.class.getResource("/F011906.dat").toURI());
    assertEquals(true, Files.exists(datFile));

    String filename = parser.parseFilename(datFile);

    assertNotNull(filename);
    assertEquals("LO_20100705_COU_01.RAW", filename);
  }

  @Test
  public void parseFilename_Mgf() throws Exception {
    Path datFile = Paths.get(MascotParserTest.class.getResource("/F011906_mgf.dat").toURI());
    assertEquals(true, Files.exists(datFile));

    String filename = parser.parseFilename(datFile);

    assertNotNull(filename);
    assertEquals("LO_20100705_COU_02.mgf", filename);
  }

  @Test
  public void parseFilename_Windows() throws Exception {
    Path datFile = Paths.get(MascotParserTest.class.getResource("/F011906_windows.dat").toURI());
    assertEquals(true, Files.exists(datFile));

    String filename = parser.parseFilename(datFile);

    assertNotNull(filename);
    assertEquals("LO_20100705_COU_01.RAW", filename);
  }

  @Test
  public void parseFilename_FileName() throws Exception {
    Path datFile = Paths.get(MascotParserTest.class.getResource("/F011906_FileName.dat").toURI());
    assertEquals(true, Files.exists(datFile));

    String filename = parser.parseFilename(datFile);

    assertNotNull(filename);
    assertEquals("LO_20100705_COU_02.RAW", filename);
  }

  @Test
  public void parseFilename_Null() throws Throwable {
    try {
      parser.parseFilename(null);
      fail("Expected NullPointerException");
    } catch (NullPointerException exception) {
      // Ignore.
    }
  }

  @Test
  public void parseFilename_NotExists() throws Throwable {
    Path datFile = Paths.get(MascotParserTest.class.getResource("/F011906.dat").toURI()).getParent()
        .resolve("abc.dat");

    try {
      parser.parseFilename(datFile);
      fail("Expected FileNotFoundException");
    } catch (FileNotFoundException exception) {
      // Ignore.
    }
  }

  @Test
  public void parseFilename_InvalidMascotFile() throws Throwable {
    Path datFile = Paths.get(MascotParserTest.class.getResource("/F011906_invalid.dat").toURI());

    try {
      parser.parseFilename(datFile);
      fail("Expected IOException");
    } catch (IOException exception) {
      // Ignore.
    }
  }
}

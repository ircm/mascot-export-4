/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.web;

import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionPresenter.FILES;
import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionPresenter.INPUT_FILE;
import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionPresenter.NAME;
import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionPresenter.PARENT;
import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionPresenter.SELECT;
import static ca.qc.ircm.mascotexport.test.utils.VaadinTestUtils.items;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.MascotConfiguration;
import ca.qc.ircm.mascotexport.mascot.MascotFile;
import ca.qc.ircm.mascotexport.mascot.MascotService;
import ca.qc.ircm.mascotexport.mascot.parser.MascotParser;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.TextField;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotFilesSelectionPresenterTest {
  private MascotFilesSelectionPresenter presenter;
  @Mock
  private MascotService mascotService;
  @Mock
  private MascotParser mascotParser;
  @Mock
  private MascotConfiguration mascotConfiguration;
  @Mock
  private MascotFilesSelection view;
  @Captor
  private ArgumentCaptor<List<Path>> filesCaptor;
  private MascotFilesSelectionDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(MascotFilesSelection.class, locale);
  private Path data = Paths.get("data");
  private List<MascotFile> mascotFiles = new ArrayList<>();

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    presenter = new MascotFilesSelectionPresenter(mascotService);
    design = new MascotFilesSelectionDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    mascotFiles.add(new MascotFile(Paths.get("file0_1.dat"), "file0_1.raw"));
    mascotFiles.add(new MascotFile(Paths.get("folder1/file1_1.dat"), "file1_1.raw"));
    mascotFiles.add(new MascotFile(Paths.get("folder1/file1_2.dat"), "file1_2.raw"));
    mascotFiles.add(new MascotFile(Paths.get("folder2/file2_1.dat"), "file2_1.raw"));
    mascotFiles.add(new MascotFile(Paths.get("folder2/file2_2.dat"), "file2_2.raw"));
    when(mascotService.allFilesWithInput()).thenReturn(mascotFiles);
    when(mascotParser.parseFilename(any()))
        .thenAnswer(i -> FilenameUtils.getBaseName(i.getArgumentAt(0, Path.class).toString()));
    when(mascotConfiguration.getData()).thenReturn(data);
    presenter.init(view);
  }

  @Test
  public void styles() {
    assertTrue(design.files.getStyleName().contains(FILES));
    assertTrue(design.select.getStyleName().contains(SELECT));
  }

  @Test
  public void captions() {
    assertEquals(resources.message(SELECT), design.select.getCaption());
  }

  @Test
  public void files() throws Throwable {
    assertEquals(3, design.files.getColumns().size());
    assertEquals(NAME, design.files.getColumns().get(0).getId());
    assertEquals(resources.message(NAME), design.files.getColumn(NAME).getCaption());
    for (MascotFile file : mascotFiles) {
      assertEquals(file.file.getFileName().toString(),
          design.files.getColumn(NAME).getValueProvider().apply(file));
      assertEquals(file.file.toString(),
          design.files.getColumn(NAME).getDescriptionGenerator().apply(file));
    }
    assertTrue(design.files.getColumn(NAME).isSortable());
    assertEquals(PARENT, design.files.getColumns().get(1).getId());
    assertEquals(resources.message(PARENT), design.files.getColumn(PARENT).getCaption());
    for (MascotFile file : mascotFiles) {
      assertEquals(
          file.file.getParent() != null ? file.file.getParent().getFileName().toString() : "",
          design.files.getColumn(PARENT).getValueProvider().apply(file));
    }
    assertTrue(design.files.getColumn(PARENT).isSortable());
    assertEquals(INPUT_FILE, design.files.getColumns().get(2).getId());
    assertEquals(resources.message(INPUT_FILE), design.files.getColumn(INPUT_FILE).getCaption());
    for (MascotFile file : mascotFiles) {
      assertEquals(file.inputFile,
          design.files.getColumn(INPUT_FILE).getValueProvider().apply(file));
    }
    assertTrue(design.files.getColumn(INPUT_FILE).isSortable());
    List<GridSortOrder<MascotFile>> sortOrders = design.files.getSortOrder();
    assertEquals(2, sortOrders.size());
    GridSortOrder<MascotFile> sortOrder = sortOrders.get(0);
    assertEquals(design.files.getColumn(PARENT), sortOrder.getSorted());
    assertEquals(SortDirection.DESCENDING, sortOrder.getDirection());
    sortOrder = sortOrders.get(1);
    assertEquals(design.files.getColumn(NAME), sortOrder.getSorted());
    assertEquals(SortDirection.ASCENDING, sortOrder.getDirection());
  }

  @Test
  public void filesChoices() {
    List<MascotFile> items = items(design.files);

    assertEquals(mascotFiles.size(), items.size());
    for (MascotFile file : mascotFiles) {
      assertTrue(items.contains(file));
    }
  }

  @Test
  @SuppressWarnings("unchecked")
  public void nameFilter() {
    DataProvider<MascotFile, Object> dataProvider = mock(DataProvider.class);
    design.files.setDataProvider(dataProvider);
    String filterValue = "test";
    TextField filter = (TextField) design.files.getHeaderRow(1).getCell(NAME).getComponent();
    filter.setValue(filterValue);

    assertEquals(filterValue, presenter.getFilter().nameContains);
    verify(dataProvider).refreshAll();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void parentFilter() {
    DataProvider<MascotFile, Object> dataProvider = mock(DataProvider.class);
    design.files.setDataProvider(dataProvider);
    String filterValue = "test";
    TextField filter = (TextField) design.files.getHeaderRow(1).getCell(PARENT).getComponent();
    filter.setValue(filterValue);

    assertEquals(filterValue, presenter.getFilter().parentContains);
    verify(dataProvider).refreshAll();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void inputFileFilter() {
    DataProvider<MascotFile, Object> dataProvider = mock(DataProvider.class);
    design.files.setDataProvider(dataProvider);
    String filterValue = "test";
    TextField filter = (TextField) design.files.getHeaderRow(1).getCell(INPUT_FILE).getComponent();
    filter.setValue(filterValue);

    assertEquals(filterValue, presenter.getFilter().inputFileContains);
    verify(dataProvider).refreshAll();
  }

  @Test
  public void select_File() {
    design.files.select(mascotFiles.get(1));

    design.select.click();

    verify(view).fireSaveEvent(filesCaptor.capture());
    List<Path> files = filesCaptor.getValue();
    assertEquals(1, files.size());
    assertTrue(files.contains(mascotFiles.get(1).file));
  }

  @Test
  public void select_MultipleFiles() {
    design.files.select(mascotFiles.get(1));
    design.files.select(mascotFiles.get(4));

    design.select.click();

    verify(view).fireSaveEvent(filesCaptor.capture());
    List<Path> files = filesCaptor.getValue();
    assertEquals(2, files.size());
    assertTrue(files.contains(mascotFiles.get(1).file));
    assertTrue(files.contains(mascotFiles.get(4).file));
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.mascot.web;

import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionWindowPresenter.STYLE;
import static ca.qc.ircm.mascotexport.mascot.web.MascotFilesSelectionWindowPresenter.TITLE;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.mascotexport.web.SaveListener;
import ca.qc.ircm.utils.MessageResource;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MascotFilesSelectionWindowPresenterTest {
  private MascotFilesSelectionWindowPresenter presenter;
  @Mock
  private MascotFilesSelectionPresenter mascotFilesSelectionPresenter;
  @Mock
  private MascotFilesSelectionWindow view;
  @Mock
  private SaveListener<List<String>> listener;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(MascotFilesSelectionWindow.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() throws Throwable {
    presenter = new MascotFilesSelectionWindowPresenter();
    view.mascotFilesSelection = mock(MascotFilesSelection.class);
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    presenter.init(view);
  }

  @Test
  public void styles() {
    verify(view).addStyleName(STYLE);
  }

  @Test
  public void captions() {
    verify(view).setCaption(resources.message(TITLE));
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.task.TaskState;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests for {@link TaskService}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class TaskServiceTest {
  @Inject
  @InjectMocks
  private TaskService taskServiceDefault;
  @Mock
  private Tasks tasks;
  @Mock
  private Task task;

  @Before
  public void beforeTest() {
    taskServiceDefault = new TaskService(tasks);
  }

  @Test
  public void get() {
    UUID id = UUID.randomUUID();
    when(tasks.get(any(UUID.class))).thenReturn(task);

    Task task = taskServiceDefault.get(id);

    verify(tasks).get(id);
    assertEquals(this.task, task);
  }

  @Test
  public void get_Null() {
    when(tasks.get(any(UUID.class))).thenReturn(task);

    assertEquals(task, taskServiceDefault.get(null));
  }

  @Test
  public void tasks() {
    UUID id1 = UUID.randomUUID();
    Task task1 = mock(Task.class);
    when(task1.getId()).thenReturn(id1);
    when(task1.getState()).thenReturn(mock(TaskState.class));
    when(task1.getState().isDone()).thenReturn(true);
    UUID id2 = UUID.randomUUID();
    Task task2 = mock(Task.class);
    when(task2.getId()).thenReturn(id2);
    when(task2.getState()).thenReturn(mock(TaskState.class));
    when(task2.getState().isDone()).thenReturn(false);
    List<Task> iterateTasks = Arrays.asList(task1, task2);
    when(tasks.values()).thenReturn(iterateTasks);

    List<Task> tasks = taskServiceDefault.tasks();

    assertEquals(iterateTasks, tasks);
  }

  @Test
  public void add() {
    UUID id = UUID.randomUUID();
    when(task.getId()).thenReturn(id);

    taskServiceDefault.add(task);

    verify(tasks).put(id, task);
  }

  @Test
  public void add_Null() {
    taskServiceDefault.add(null);

    verifyZeroInteractions(tasks);
  }

  @Test
  public void remove() {
    UUID id = UUID.randomUUID();
    when(task.getId()).thenReturn(id);

    taskServiceDefault.remove(task);

    verify(tasks).remove(id);
  }

  @Test
  public void remove_Null() {
    taskServiceDefault.remove(null);

    verifyZeroInteractions(tasks);
  }

  @Test
  public void removeAllDone() {
    UUID id1 = UUID.randomUUID();
    Task task1 = mock(Task.class);
    when(task1.getId()).thenReturn(id1);
    when(task1.getState()).thenReturn(mock(TaskState.class));
    when(task1.getState().isDone()).thenReturn(true);
    UUID id2 = UUID.randomUUID();
    Task task2 = mock(Task.class);
    when(task2.getId()).thenReturn(id2);
    when(task2.getState()).thenReturn(mock(TaskState.class));
    when(task2.getState().isDone()).thenReturn(false);
    Collection<Task> iterateTasks = Arrays.asList(task1, task2);
    when(tasks.values()).thenReturn(iterateTasks);

    taskServiceDefault.removeAllDone();

    verify(tasks).remove(id1);
    verify(tasks, never()).remove(id2);
  }
}

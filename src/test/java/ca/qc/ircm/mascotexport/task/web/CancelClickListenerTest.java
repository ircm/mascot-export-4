/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task.web;

import static org.mockito.Mockito.verify;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.task.Task;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.TextField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class CancelClickListenerTest {
  private CancelClickListener cancelClickListener;
  @Mock
  private Task task;

  @Before
  public void beforeTest() {
    cancelClickListener = new CancelClickListener(task);
  }

  @Test
  public void buttonClick() {
    TextField field = new TextField();

    cancelClickListener.buttonClick(new ClickEvent(field));

    verify(task).cancel();
  }
}

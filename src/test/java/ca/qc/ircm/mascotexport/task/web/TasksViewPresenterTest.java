/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.task.web;

import static ca.qc.ircm.mascotexport.task.web.TasksViewPresenter.CANCEL;
import static ca.qc.ircm.mascotexport.task.web.TasksViewPresenter.ID;
import static ca.qc.ircm.mascotexport.task.web.TasksViewPresenter.JOBS;
import static ca.qc.ircm.mascotexport.task.web.TasksViewPresenter.PROGRESS;
import static ca.qc.ircm.mascotexport.task.web.TasksViewPresenter.REFRESH;
import static ca.qc.ircm.mascotexport.test.utils.VaadinGridUtils.columnValue;
import static ca.qc.ircm.mascotexport.test.utils.VaadinGridUtils.renderer;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportJobTaskContext;
import ca.qc.ircm.mascotexport.job.RunningExportJobs;
import ca.qc.ircm.mascotexport.test.config.ServiceTestAnnotations;
import ca.qc.ircm.task.Task;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ServiceTestAnnotations
public class TasksViewPresenterTest {
  private TasksViewPresenter presenter;
  @Inject
  private MongoTemplate mongoTemplate;
  @Mock
  private TasksView view;
  @Mock
  private RunningExportJobs runningJobs;
  @Mock
  private ApplicationEventPublisher publisher;
  @Captor
  private ArgumentCaptor<ApplicationEvent> applicationEventCaptor;
  private TasksViewDesign design;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(TasksView.class, locale);
  private List<ExportJob> allJobs;
  private List<Task> allTasks;
  private Map<ExportJob, Task> tasksMap;

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new TasksViewPresenter(runningJobs, publisher);
    design = new TasksViewDesign();
    view.design = design;
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    allJobs();
    allTasks(allJobs);
    when(runningJobs.all()).thenReturn(allJobs);
    when(runningJobs.get(any())).thenAnswer(i -> {
      ExportJob job = (ExportJob) i.getArguments()[0];
      return tasksMap.get(job);
    });
    presenter.attach(view);
  }

  private void allJobs() {
    allJobs = mongoTemplate.findAll(ExportJob.class).stream().limit(3).collect(Collectors.toList());
  }

  private void allTasks(List<ExportJob> jobs) {
    allTasks = new ArrayList<>();
    tasksMap = new HashMap<>();
    Task task1 = mock(Task.class);
    when(task1.getId()).thenReturn(UUID.randomUUID());
    ExportJobTaskContext context = mock(ExportJobTaskContext.class);
    when(task1.getContext()).thenReturn(context);
    List<ExportJob> taskJobs = new ArrayList<>();
    taskJobs.add(jobs.get(0));
    taskJobs.add(jobs.get(1));
    when(context.getJobs()).thenReturn(taskJobs);
    tasksMap.put(jobs.get(0), task1);
    tasksMap.put(jobs.get(1), task1);
    allTasks.add(task1);
    Task task2 = mock(Task.class);
    when(task2.getId()).thenReturn(UUID.randomUUID());
    context = mock(ExportJobTaskContext.class);
    when(task2.getContext()).thenReturn(context);
    taskJobs = new ArrayList<>();
    taskJobs.add(jobs.get(2));
    when(context.getJobs()).thenReturn(taskJobs);
    tasksMap.put(jobs.get(2), task2);
    allTasks.add(task2);
  }

  @SuppressWarnings("unchecked")
  private List<Task> gridTasks() {
    return new ArrayList<>(
        ((ListDataProvider<Task>) design.tasksGrid.getDataProvider()).getItems());
  }

  @Test
  public void defaultValues() throws Throwable {
    List<Task> tasks = gridTasks();

    assertEquals(2, tasks.size());
    Task task = tasks.get(0);
    assertEquals(allTasks.get(0), task);
    assertEquals(resources.message("task." + ID, task.getId().toString()),
        columnValue(design.tasksGrid.getColumn(ID), task));
    assertEquals(2, columnValue(design.tasksGrid.getColumn(JOBS), task));
    assertEquals(resources.message(PROGRESS),
        columnValue(design.tasksGrid.getColumn(PROGRESS), task));
    assertEquals(resources.message(CANCEL), columnValue(design.tasksGrid.getColumn(CANCEL), task));
  }

  @Test
  public void styles() {
  }

  @Test
  public void captions() {
    verify(view).setTitle(resources.message("title"));
    assertEquals(resources.message(ID), design.tasksGrid.getColumn(ID).getCaption());
    assertEquals(resources.message(JOBS), design.tasksGrid.getColumn(JOBS).getCaption());
    assertEquals(resources.message(PROGRESS), design.tasksGrid.getColumn(PROGRESS).getCaption());
    assertEquals(resources.message(CANCEL), design.tasksGrid.getColumn(CANCEL).getCaption());
    assertEquals(resources.message(REFRESH), design.refreshButton.getCaption());
  }

  @Test
  @SuppressWarnings({ "unchecked", "serial" })
  public void progress() {
    Task task = allTasks.get(0);
    Column<Task, ?> progressColumn = design.tasksGrid.getColumn(PROGRESS);
    ButtonRenderer<Task> renderer = renderer(progressColumn, ButtonRenderer.class);
    RendererClickListener<Task> viewListener =
        ((Collection<RendererClickListener<Task>>) renderer.getListeners(RendererClickEvent.class))
            .iterator().next();

    viewListener
        .click(new RendererClickEvent<Task>(design.tasksGrid, task, progressColumn, null) {});

    verify(publisher).publishEvent(applicationEventCaptor.capture());
    ApplicationEvent event = applicationEventCaptor.getValue();
    assertTrue(event instanceof ShowTaskProgressEvent);
    ShowTaskProgressEvent showTaskProgressEvent = (ShowTaskProgressEvent) event;
    assertEquals(task, showTaskProgressEvent.getTask());
  }

  @Test
  @SuppressWarnings({ "unchecked", "serial" })
  public void cancel() {
    allTasks.remove(0);
    tasksMap.remove(allJobs.get(0));
    tasksMap.remove(allJobs.get(1));
    allJobs.remove(0);
    allJobs.remove(0);
    Task task = allTasks.get(0);
    Column<Task, ?> cancelColumn = design.tasksGrid.getColumn(CANCEL);
    ButtonRenderer<Task> renderer = renderer(cancelColumn, ButtonRenderer.class);
    RendererClickListener<Task> viewListener =
        ((Collection<RendererClickListener<Task>>) renderer.getListeners(RendererClickEvent.class))
            .iterator().next();

    viewListener.click(new RendererClickEvent<Task>(design.tasksGrid, task, cancelColumn, null) {});

    verify(task).cancel();
    List<Task> tasks = gridTasks();
    assertEquals(1, tasks.size());
    assertEquals(task, tasks.get(0));
  }

  @Test
  public void refresh() {
    allTasks.remove(0);
    tasksMap.remove(allJobs.get(0));
    tasksMap.remove(allJobs.get(1));
    allJobs.remove(0);
    allJobs.remove(0);

    design.refreshButton.click();

    List<Task> tasks = gridTasks();
    assertEquals(1, tasks.size());
    Task task = allTasks.get(0);
    assertEquals(task, tasks.get(0));
  }
}

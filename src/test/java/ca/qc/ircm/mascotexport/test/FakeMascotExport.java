/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.test;

import java.util.Random;

public class FakeMascotExport {
  private static final int STRING_LENGTH = 2048;
  private static final int ROUNDS = 5;
  private static final char[] chars =
      ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" + "0123456789").toCharArray();

  /**
   * Writes random strings to output.
   *
   * @param args
   *          not used
   */
  public static void main(String[] args) {
    int rounds = ROUNDS;
    System.out.println(randomString());
    for (int i = 0; i < rounds; i++) {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException exception) {
        return;
      }
      System.out.println(randomString());
    }
    System.out.println(randomString());
  }

  private static String randomString() {
    Random random = new Random();
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < STRING_LENGTH; i++) {
      builder.append(chars[random.nextInt(chars.length)]);
    }
    return builder.toString();
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.test.config;

import static ca.qc.ircm.mascotexport.job.ExportFormat.CSV;
import static ca.qc.ircm.mascotexport.job.ExportFormat.XML;

import ca.qc.ircm.mascotexport.ProteinScoring;
import ca.qc.ircm.mascotexport.job.ExportJob;
import ca.qc.ircm.mascotexport.job.ExportParameters;
import ca.qc.ircm.mascotexport.job.PeptideParameters;
import ca.qc.ircm.mascotexport.job.ProteinParameters;
import ca.qc.ircm.mascotexport.job.QueryParameters;
import ca.qc.ircm.mascotexport.job.SearchParameters;
import java.time.LocalDateTime;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.TestContext;

/**
 * Initialized Mongo database.
 */
public class InitializeMongoExecutionListener extends InjectIntoTestExecutionListener {
  private static final Logger logger =
      LoggerFactory.getLogger(InitializeMongoExecutionListener.class);
  @Inject
  private MongoTemplate mongoTemplate;

  @Override
  public void beforeTestMethod(TestContext testContext) throws Exception {
    logger.debug("Initializes database");

    ExportParameters exportParameters = new ExportParameters();
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(false);
    exportParameters.setRequireBoldRed(true);
    exportParameters.setPreferTaxonomy(null);
    SearchParameters searchParameters = new SearchParameters();
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(false);
    ProteinParameters proteinParameters = new ProteinParameters();
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(false);
    proteinParameters.setPi(false);
    proteinParameters.setTaxStr(false);
    proteinParameters.setTaxId(false);
    proteinParameters.setSeq(false);
    proteinParameters.setEmpai(true);
    PeptideParameters peptideParameters = new PeptideParameters();
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(false);
    peptideParameters.setIdent(false);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(false);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(false);
    peptideParameters.setShowPepDupes(false);
    QueryParameters queryParameters = new QueryParameters();
    queryParameters.setMaster(false);
    queryParameters.setTitle(false);
    queryParameters.setQualifiers(false);
    queryParameters.setParams(false);
    queryParameters.setPeaks(false);
    queryParameters.setRaw(false);
    ExportJob job = new ExportJob();
    job.setId("1");
    job.setFilePath("20110330/F017513.dat");
    job.setDate(LocalDateTime.of(2012, 5, 7, 10, 11, 27));
    job.setDoneDate(null);
    job.setExportParameters(exportParameters);
    job.setSearchParameters(searchParameters);
    job.setProteinParameters(proteinParameters);
    job.setPeptideParameters(peptideParameters);
    job.setQueryParameters(queryParameters);
    mongoTemplate.save(job);

    exportParameters = new ExportParameters();
    exportParameters.setExportFormat(CSV);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(false);
    exportParameters.setRequireBoldRed(true);
    exportParameters.setPreferTaxonomy(null);
    searchParameters = new SearchParameters();
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(false);
    proteinParameters = new ProteinParameters();
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(false);
    proteinParameters.setPi(false);
    proteinParameters.setTaxStr(false);
    proteinParameters.setTaxId(false);
    proteinParameters.setSeq(false);
    proteinParameters.setEmpai(true);
    peptideParameters = new PeptideParameters();
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(false);
    peptideParameters.setIdent(false);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(false);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(false);
    peptideParameters.setShowPepDupes(false);
    queryParameters = new QueryParameters();
    queryParameters.setMaster(false);
    queryParameters.setTitle(false);
    queryParameters.setQualifiers(false);
    queryParameters.setParams(false);
    queryParameters.setPeaks(false);
    queryParameters.setRaw(false);
    job = new ExportJob();
    job.setId("2");
    job.setFilePath("20120507/F025704.dat");
    job.setDate(LocalDateTime.of(2012, 5, 9, 9, 30, 15));
    job.setDoneDate(null);
    job.setExportParameters(exportParameters);
    job.setSearchParameters(searchParameters);
    job.setProteinParameters(proteinParameters);
    job.setPeptideParameters(peptideParameters);
    job.setQueryParameters(queryParameters);
    mongoTemplate.save(job);

    exportParameters = new ExportParameters();
    exportParameters.setExportFormat(XML);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(false);
    exportParameters.setRequireBoldRed(true);
    exportParameters.setPreferTaxonomy(null);
    searchParameters = new SearchParameters();
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(false);
    proteinParameters = new ProteinParameters();
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(false);
    proteinParameters.setPi(false);
    proteinParameters.setTaxStr(false);
    proteinParameters.setTaxId(false);
    proteinParameters.setSeq(false);
    proteinParameters.setEmpai(true);
    peptideParameters = new PeptideParameters();
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(false);
    peptideParameters.setIdent(false);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(false);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(false);
    peptideParameters.setShowPepDupes(false);
    queryParameters = new QueryParameters();
    queryParameters.setMaster(false);
    queryParameters.setTitle(false);
    queryParameters.setQualifiers(false);
    queryParameters.setParams(false);
    queryParameters.setPeaks(false);
    queryParameters.setRaw(false);
    job = new ExportJob();
    job.setId("3");
    job.setFilePath("20110330/F017514.dat");
    job.setDate(LocalDateTime.of(2012, 5, 8, 15, 21, 7));
    job.setDoneDate(null);
    job.setExportParameters(exportParameters);
    job.setSearchParameters(searchParameters);
    job.setProteinParameters(proteinParameters);
    job.setPeptideParameters(peptideParameters);
    job.setQueryParameters(queryParameters);
    mongoTemplate.save(job);

    exportParameters = new ExportParameters();
    exportParameters.setExportFormat(XML);
    exportParameters.setSigThreshold(0.05);
    exportParameters.setIgnoreIonsScoreBelow(15);
    exportParameters.setUseHomology(false);
    exportParameters.setReport(200);
    exportParameters.setServerMudpitSwitch(ProteinScoring.STANDARD);
    exportParameters.setShowSameSets(true);
    exportParameters.setShowSubsets(1);
    exportParameters.setGroupFamily(false);
    exportParameters.setRequireBoldRed(true);
    exportParameters.setPreferTaxonomy(null);
    searchParameters = new SearchParameters();
    searchParameters.setMaster(true);
    searchParameters.setShowHeader(true);
    searchParameters.setShowMods(true);
    searchParameters.setShowParams(true);
    searchParameters.setShowFormat(true);
    searchParameters.setShowMasses(false);
    proteinParameters = new ProteinParameters();
    proteinParameters.setMaster(true);
    proteinParameters.setScore(true);
    proteinParameters.setDesc(true);
    proteinParameters.setMass(true);
    proteinParameters.setMatches(true);
    proteinParameters.setCover(true);
    proteinParameters.setLen(false);
    proteinParameters.setPi(false);
    proteinParameters.setTaxStr(false);
    proteinParameters.setTaxId(false);
    proteinParameters.setSeq(false);
    proteinParameters.setEmpai(true);
    peptideParameters = new PeptideParameters();
    peptideParameters.setMaster(true);
    peptideParameters.setExpMr(true);
    peptideParameters.setExpZ(true);
    peptideParameters.setCalcMr(true);
    peptideParameters.setDelta(true);
    peptideParameters.setStart(true);
    peptideParameters.setEnd(true);
    peptideParameters.setMiss(true);
    peptideParameters.setScore(true);
    peptideParameters.setHomol(false);
    peptideParameters.setIdent(false);
    peptideParameters.setExpect(true);
    peptideParameters.setSeq(true);
    peptideParameters.setFrame(false);
    peptideParameters.setVarMod(true);
    peptideParameters.setNumMatch(true);
    peptideParameters.setScanTitle(true);
    peptideParameters.setShowUnassigned(false);
    peptideParameters.setShowPepDupes(false);
    queryParameters = new QueryParameters();
    queryParameters.setMaster(false);
    queryParameters.setTitle(false);
    queryParameters.setQualifiers(false);
    queryParameters.setParams(false);
    queryParameters.setPeaks(false);
    queryParameters.setRaw(false);
    job = new ExportJob();
    job.setId("4");
    job.setFilePath("20110330/F017513.dat");
    job.setDate(LocalDateTime.of(2012, 5, 6, 14, 42, 37));
    job.setDoneDate(LocalDateTime.of(2012, 5, 8, 6, 30, 45));
    job.setExportParameters(exportParameters);
    job.setSearchParameters(searchParameters);
    job.setProteinParameters(proteinParameters);
    job.setPeptideParameters(peptideParameters);
    job.setQueryParameters(queryParameters);
    mongoTemplate.save(job);
  }

  @Override
  public void afterTestMethod(TestContext testContext) throws Exception {
    mongoTemplate.dropCollection(ExportJob.class);
  }
}

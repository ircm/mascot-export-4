/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.test.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

/**
 * Utilities for test.
 */
public class TestUtils {
  /**
   * Compares two maps for equality.
   *
   * @param expected
   *          expected map
   * @param actual
   *          actual map
   */
  public static <K, V> void compareMaps(Map<K, V> expected, Map<K, V> actual) {
    if (expected == null || actual == null) {
      if (expected != null && !expected.isEmpty()) {
        fail("Expected " + expected.size() + " elements but found 0");
      }
      if (actual != null && !actual.isEmpty()) {
        fail("Expected 0 elements but found " + +actual.size());
      }
    } else {
      for (Map.Entry<K, V> expectedEntry : expected.entrySet()) {
        assertTrue("Expected key " + expectedEntry.getKey() + " not found in acutal map",
            actual.containsKey(expectedEntry.getKey()));
        assertEquals(
            "Expected value " + expectedEntry.getValue() + " for key " + expectedEntry.getKey()
                + " but found " + actual.get(expectedEntry.getKey()),
            expectedEntry.getValue(), actual.get(expectedEntry.getKey()));
      }
      for (Map.Entry<K, V> actualEntry : actual.entrySet()) {
        assertTrue("Key " + actualEntry.getKey() + " not expected",
            expected.containsKey(actualEntry.getKey()));
      }
    }
  }
}

/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.test.utils;

import com.vaadin.server.Extension;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.renderers.AbstractRenderer;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.function.Function;

/**
 * Vaadin utilities for Grid.
 */
public class VaadinGridUtils {
  /**
   * Returns column's value for specified item.
   *
   * @param column
   *          column
   * @param item
   *          item
   * @return column's value for specified item
   */
  @SuppressWarnings("unchecked")
  public static <T, V> V columnValue(Column<T, V> column, T item) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException {
    Field valueProviderField = column.getClass().getDeclaredField("valueProvider");
    valueProviderField.setAccessible(true);
    Function<T, ? extends V> valueProvider =
        (Function<T, ? extends V>) valueProviderField.get(column);
    return valueProvider.apply(item);
  }

  /**
   * Returns column's renderer of specified type, if any.
   *
   * @param column
   *          column
   * @param clazz
   *          renderer type
   * @return column's renderer of specified type, if any
   */
  @SuppressWarnings("unchecked")
  public static <T, V, R extends AbstractRenderer<V, ?>> R renderer(Column<T, V> column,
      Class<R> clazz) {
    Collection<Extension> extensions = column.getExtensions();
    return (R) extensions.stream().filter(extension -> clazz.isAssignableFrom(extension.getClass()))
        .findFirst().orElse(null);
  }
}

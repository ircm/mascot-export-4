/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import static ca.qc.ircm.mascotexport.web.AboutWindowPresenter.LABEL;
import static ca.qc.ircm.mascotexport.web.AboutWindowPresenter.STYLE;
import static ca.qc.ircm.mascotexport.web.AboutWindowPresenter.TITLE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.ui.Label;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class AboutWindowPresenterTest {
  private AboutWindowPresenter presenter;
  @Mock
  private AboutWindow view;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(AboutWindow.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    presenter = new AboutWindowPresenter();
    view.label = new Label();
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    presenter.init(view);
  }

  @Test
  public void styles() {
    verify(view).addStyleName(STYLE);
    assertTrue(view.label.getStyleName().contains(LABEL));
  }

  @Test
  public void captions() {
    verify(view).setCaption(resources.message(TITLE));
    assertEquals(resources.message(LABEL), view.label.getValue());
  }
}

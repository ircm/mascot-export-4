/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import static ca.qc.ircm.mascotexport.web.AboutWindowPresenter.LABEL;
import static ca.qc.ircm.mascotexport.web.AboutWindowPresenter.STYLE;
import static ca.qc.ircm.mascotexport.web.AboutWindowPresenter.TITLE;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.ABOUT;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.HELP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.openqa.selenium.By.className;

import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.testbench.elements.LabelElement;
import com.vaadin.testbench.elements.MenuBarElement;
import com.vaadin.testbench.elements.WindowElement;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class AboutWindowTest extends AbstractTestBenchTestCase {
  private MessageResource resources;
  private MessageResource menuResources;

  @Before
  public void beforeTest() {
    resources = new MessageResource(AboutWindow.class, Locale.getDefault());
    menuResources = new MessageResource(Menu.class, Locale.getDefault());
  }

  @Test
  public void about() throws Throwable {
    openView(MainView.VIEW_NAME);
    MenuBarElement menu = $(MenuBarElement.class).first();

    menu.clickItem(menuResources.message(HELP), menuResources.message(ABOUT));

    assertNotNull(findElement(className(STYLE)));
    WindowElement about = wrap(WindowElement.class, findElement(className(STYLE)));
    assertEquals(resources.message(TITLE), about.getCaption());
    assertEquals(resources.message(LABEL), about.$(LabelElement.class).first().getText());
  }
}

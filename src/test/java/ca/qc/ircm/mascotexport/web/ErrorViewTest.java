/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import static org.junit.Assert.assertEquals;

import ca.qc.ircm.mascotexport.job.web.ExportView;
import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import com.vaadin.testbench.elements.ButtonElement;
import com.vaadin.testbench.elements.LabelElement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class ErrorViewTest extends AbstractTestBenchTestCase {
  @Test
  public void errorView() throws Throwable {
    openView(ErrorView.VIEW_NAME);

    assertEquals("Unfortunately, the page you've requested does not exists.",
        $(LabelElement.class).first().getText());
    ButtonElement back = $(ButtonElement.class).first();
    assertEquals("Back to the main page", back.getText());
    back.click();

    assertEquals(viewUrl(ExportView.VIEW_NAME), getDriver().getCurrentUrl());
  }
}

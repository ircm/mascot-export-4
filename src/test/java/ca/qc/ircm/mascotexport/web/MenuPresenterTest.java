/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import static ca.qc.ircm.mascotexport.web.MenuPresenter.ABOUT;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.EXPORT;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.HELP;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.JOBS;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.TASKS;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.job.web.ExportView;
import ca.qc.ircm.mascotexport.job.web.JobsView;
import ca.qc.ircm.mascotexport.task.web.TasksView;
import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Window;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEvent;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MenuPresenterTest {
  @Inject
  @InjectMocks
  private MenuPresenter presenter;
  @Mock
  private Menu view;
  @Mock
  private ClickListener listener;
  @Captor
  private ArgumentCaptor<ApplicationEvent> eventCaptor;
  @Captor
  private ArgumentCaptor<Window> windowCaptor;
  private Locale locale = Locale.getDefault();
  private MessageResource resources = new MessageResource(Menu.class, locale);

  /**
   * Before test.
   */
  @Before
  public void beforeTest() {
    view.menuBar = new MenuBar();
    when(view.getLocale()).thenReturn(locale);
    when(view.getResources()).thenReturn(resources);
    presenter.init(view);
  }

  private MenuItem findMenuItem(Collection<MenuItem> menuItems, String text) {
    for (MenuItem menuItem : menuItems) {
      if (text.equals(menuItem.getText())) {
        return menuItem;
      }
    }
    return null;
  }

  @Test
  public void captions() {
    List<MenuItem> menuItems = view.menuBar.getItems();

    assertNotNull(findMenuItem(menuItems, resources.message(EXPORT)));
    assertNotNull(findMenuItem(menuItems, resources.message(JOBS)));
    assertNotNull(findMenuItem(menuItems, resources.message(TASKS)));
    MenuItem menuItem = findMenuItem(menuItems, resources.message(HELP));
    assertNotNull(menuItem);
    assertNotNull(findMenuItem(menuItem.getChildren(), resources.message(ABOUT)));
  }

  @Test
  public void export() {
    MenuItem menuItem = findMenuItem(view.menuBar.getItems(), resources.message(EXPORT));

    menuItem.getCommand().menuSelected(menuItem);

    verify(view).navigateTo(ExportView.VIEW_NAME);
  }

  @Test
  public void jobs() {
    MenuItem menuItem = findMenuItem(view.menuBar.getItems(), resources.message(JOBS));

    menuItem.getCommand().menuSelected(menuItem);

    verify(view).navigateTo(JobsView.VIEW_NAME);
  }

  @Test
  public void tasks() {
    MenuItem menuItem = findMenuItem(view.menuBar.getItems(), resources.message(TASKS));

    menuItem.getCommand().menuSelected(menuItem);

    verify(view).navigateTo(TasksView.VIEW_NAME);
  }

  @Test
  public void help() {
    MenuItem menuItem = findMenuItem(view.menuBar.getItems(), resources.message(HELP));

    assertNull(menuItem.getCommand());
  }

  @Test
  public void about() {
    MenuItem helpItem = findMenuItem(view.menuBar.getItems(), resources.message(HELP));
    MenuItem aboutItem = findMenuItem(helpItem.getChildren(), resources.message(ABOUT));

    aboutItem.getCommand().menuSelected(aboutItem);

    verify(view).addWindow(windowCaptor.capture());
    assertTrue(windowCaptor.getValue() instanceof AboutWindow);
    AboutWindow window = (AboutWindow) windowCaptor.getValue();
    assertTrue(window.isModal());
  }
}

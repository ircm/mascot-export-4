/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web;

import static ca.qc.ircm.mascotexport.web.MenuPresenter.ABOUT;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.EXPORT;
import static ca.qc.ircm.mascotexport.web.MenuPresenter.HELP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.openqa.selenium.By.className;

import ca.qc.ircm.mascotexport.job.web.ExportView;
import ca.qc.ircm.mascotexport.job.web.JobsView;
import ca.qc.ircm.mascotexport.test.config.AbstractTestBenchTestCase;
import ca.qc.ircm.mascotexport.test.config.TestBenchTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.testbench.elements.MenuBarElement;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.StaleElementReferenceException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestBenchTestAnnotations
public class MenuTest extends AbstractTestBenchTestCase {
  private MessageResource resources;

  @Before
  public void beforeTest() {
    resources = new MessageResource(Menu.class, Locale.getDefault());
  }

  private String message(String key, Object... replacements) {
    return resources.message(key, replacements);
  }

  @Test
  public void changeView() throws Throwable {
    openView(JobsView.VIEW_NAME);
    MenuBarElement menu = $(MenuBarElement.class).first();

    try {
      menu.clickItem(message(EXPORT));
    } catch (StaleElementReferenceException exception) {
      // Thrown because page changes.
    }

    assertEquals(viewUrl(ExportView.VIEW_NAME), getDriver().getCurrentUrl());
  }

  @Test
  public void addWindow() throws Throwable {
    openView(ExportView.VIEW_NAME);
    MenuBarElement menu = $(MenuBarElement.class).first();

    menu.clickItem(message(HELP), message(ABOUT));

    assertNotNull(findElement(className(AboutWindowPresenter.STYLE)));
  }
}

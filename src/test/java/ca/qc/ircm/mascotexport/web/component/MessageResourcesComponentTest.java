/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.mascotexport.web.component;

import static org.junit.Assert.assertEquals;

import ca.qc.ircm.mascotexport.test.config.NonTransactionalTestAnnotations;
import ca.qc.ircm.utils.MessageResource;
import com.vaadin.ui.TextField;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@NonTransactionalTestAnnotations
public class MessageResourcesComponentTest {
  private TestMessageResourcesComponent messageResourcesComponent;
  private Locale locale = Locale.getDefault();
  private Locale frenchLocale = Locale.FRENCH;
  private String message = "This is a test";
  private String frenchMessage = "Ceci est un test";

  @Before
  public void beforeTest() {
    messageResourcesComponent = new TestMessageResourcesComponent();
  }

  @Test
  public void getResources() {
    messageResourcesComponent.setLocale(locale);

    MessageResource messageResource = messageResourcesComponent.getResources();

    assertEquals(message, messageResource.message("message"));
  }

  @Test
  public void getResources_French() {
    messageResourcesComponent.setLocale(frenchLocale);

    MessageResource messageResource = messageResourcesComponent.getResources();

    assertEquals(frenchMessage, messageResource.message("message"));
  }

  private static class TestMessageResourcesComponent extends TextField implements BaseComponent {
    private static final long serialVersionUID = 1640742296797655653L;
  }
}

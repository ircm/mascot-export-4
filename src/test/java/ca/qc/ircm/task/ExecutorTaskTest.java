/*
 * Copyright (c) 2010 Institut de recherches cliniques de Montreal (IRCM)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.qc.ircm.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.qc.ircm.mascotexport.test.config.RetryOnFail;
import ca.qc.ircm.mascotexport.test.config.RetryOnFailRule;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ExecutorTaskTest {
  private ExecutorTask executorTask;
  private ExecutorService realExecutor;
  @Mock
  private Command command;
  @Mock
  private ExecutorService executor;
  @Mock
  private TaskContext context;
  @Mock
  @SuppressWarnings("rawtypes")
  private Future future;
  @Captor
  private ArgumentCaptor<Runnable> runnableCaptor;
  @Rule
  public RetryOnFailRule retryOnFailRule = new RetryOnFailRule();

  /**
   * Before tests.
   */
  @Before
  @SuppressWarnings("unchecked")
  public void beforeTest() {
    MockitoAnnotations.initMocks(this);
    executorTask = new ExecutorTask(command, executor, context);
    realExecutor = Executors.newFixedThreadPool(2);

    when(executor.submit(any(Runnable.class))).thenReturn(future);
    when(command.getDescription(any(Locale.class))).thenReturn("test_description");
    when(command.getErrorDescription(any(Throwable.class), any(Locale.class)))
        .thenReturn("test_error_description");
  }

  @After
  public void afterTest() {
    realExecutor.shutdownNow();
    Mockito.validateMockitoUsage();
  }

  private void realExecutorTask(Command command) {
    executorTask = new ExecutorTask(command, realExecutor, context);
  }

  @Test
  public void new_SetTaskInContext() {
    verify(context).setTask(executorTask);
  }

  @Test
  public void getId() {
    assertNotNull(executorTask.getId());
  }

  @Test
  public void execute() throws Throwable {
    executorTask.execute();

    verify(executor).submit(runnableCaptor.capture());
    verify(command, never()).execute();
    runnableCaptor.getValue().run();
    verify(command).execute();
    verify(command, never()).undo();
  }

  @Test
  public void canUndo() {
    assertEquals(false, executorTask.canUndo());

    verify(command).canUndo();
  }

  @Test
  public void canUndo_True() {
    when(command.canUndo()).thenReturn(true);

    assertEquals(true, executorTask.canUndo());

    verify(command).canUndo();
  }

  @Test
  public void undo() throws Throwable {
    when(command.canUndo()).thenReturn(true);

    executorTask.undo();

    verify(executor).submit(runnableCaptor.capture());
    verify(command, never()).undo();
    runnableCaptor.getValue().run();
    verify(command).undo();
    verify(command, never()).execute();
  }

  @Test
  public void undo_Interrupt() throws Throwable {
    when(command.canUndo()).thenReturn(true);

    executorTask.execute();
    executorTask.undo();

    verify(future).cancel(true);
    verify(future).get();
  }

  @Test
  @RetryOnFail(3)
  public void cancel() throws Throwable {
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        while (true) {
          Thread.sleep(1000);
        }
      }
    });
    executorTask.execute();

    executorTask.cancel();
    Thread.sleep(100);

    assertEquals(true, executorTask.getState().isCancelled());
  }

  @Test
  @RetryOnFail(3)
  public void cancel_Wait() throws Throwable {
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        while (true) {
          Thread.sleep(1000);
        }
      }
    });
    executorTask.execute();
    Thread.sleep(100);

    executorTask.cancel();
    Thread.sleep(100);

    assertEquals(true, executorTask.getState().isCancelled());
  }

  @Test
  public void cancel_NotStarted() throws Throwable {
    executorTask.cancel();

    assertEquals(true, executorTask.getState().isCancelled());
  }

  @Test
  @RetryOnFail(3)
  public void join() throws Throwable {
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        Thread.sleep(100);
      }
    });
    executorTask.execute();
    GregorianCalendar beforeCalendar = new GregorianCalendar();
    beforeCalendar.add(Calendar.MILLISECOND, -50);
    Date before = beforeCalendar.getTime();
    GregorianCalendar afterCalendar = new GregorianCalendar();
    afterCalendar.add(Calendar.MILLISECOND, 150);
    Date after = afterCalendar.getTime();
    executorTask.join();

    assertEquals(true, before.before(executorTask.getState().getCompletionMoment()));
    assertEquals(true, after.after(executorTask.getState().getCompletionMoment()));
  }

  @Test
  public void join_NotStarted() throws Throwable {
    executorTask.join();
  }

  @Test
  public void getState_New() {
    TaskState state = executorTask.getState();

    assertEquals(false, state.isRunning());
    assertEquals(false, state.isDone());
    assertEquals(false, state.isCancelled());
    assertEquals(null, state.getException());
    assertEquals(null, state.getCompletionMoment());
  }

  @Test
  @RetryOnFail(3)
  public void getState_Running() throws Throwable {
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        while (true) {
          Thread.sleep(1000);
        }
      }
    });
    executorTask.execute();
    Thread.sleep(100);
    TaskState state = executorTask.getState();

    assertEquals(true, state.isRunning());
    assertEquals(false, state.isDone());
    assertEquals(false, state.isCancelled());
    assertEquals(null, state.getException());
    assertEquals(null, state.getCompletionMoment());
  }

  @Test
  @RetryOnFail(3)
  public void getState_Cancelled() throws Throwable {
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        throw new InterruptedException();
      }
    });
    executorTask.execute();
    Thread.sleep(100);
    TaskState state = executorTask.getState();

    assertEquals(false, state.isRunning());
    assertEquals(true, state.isDone());
    assertEquals(true, state.isCancelled());
    assertEquals(null, state.getException());
    GregorianCalendar beforeCalendar = new GregorianCalendar();
    beforeCalendar.add(Calendar.MINUTE, -2);
    Date before = beforeCalendar.getTime();
    assertEquals(true,
        before.before(state.getCompletionMoment()) || before.equals(state.getCompletionMoment()));
    Date after = new Date();
    assertEquals(true,
        after.after(state.getCompletionMoment()) || after.equals(state.getCompletionMoment()));
  }

  @Test
  @RetryOnFail(3)
  public void getState_Completed() throws Throwable {
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
      }
    });
    executorTask.execute();
    Thread.sleep(100);
    TaskState state = executorTask.getState();

    assertEquals(false, state.isRunning());
    assertEquals(true, state.isDone());
    assertEquals(false, state.isCancelled());
    assertEquals(null, state.getException());
    GregorianCalendar beforeCalendar = new GregorianCalendar();
    beforeCalendar.add(Calendar.MINUTE, -2);
    Date before = beforeCalendar.getTime();
    assertEquals(true,
        before.before(state.getCompletionMoment()) || before.equals(state.getCompletionMoment()));
    Date after = new Date();
    assertEquals(true,
        after.after(state.getCompletionMoment()) || after.equals(state.getCompletionMoment()));
  }

  @Test
  @RetryOnFail(3)
  public void getState_Exception() throws Throwable {
    final Exception exception = new IllegalStateException("unit_test");
    realExecutorTask(new AbstractCommand() {
      @Override
      public void execute() throws InterruptedException, Exception {
        throw exception;
      }
    });
    executorTask.execute();
    Thread.sleep(100);
    TaskState state = executorTask.getState();

    assertEquals(false, state.isRunning());
    assertEquals(true, state.isDone());
    assertEquals(false, state.isCancelled());
    assertEquals(exception, state.getException());
    GregorianCalendar beforeCalendar = new GregorianCalendar();
    beforeCalendar.add(Calendar.MINUTE, -2);
    Date before = beforeCalendar.getTime();
    assertEquals(true,
        before.before(state.getCompletionMoment()) || before.equals(state.getCompletionMoment()));
    Date after = new Date();
    assertEquals(true,
        after.after(state.getCompletionMoment()) || after.equals(state.getCompletionMoment()));
  }

  @Test
  public void getContext() {
    assertSame(context, executorTask.getContext());
  }

  @Test
  public void getDescription() {
    String description = executorTask.getDescription(Locale.getDefault());

    verify(command).getDescription(Locale.getDefault());
    assertEquals("test_description", description);
  }

  @Test
  public void getErrorDescription() throws Throwable {
    final Exception exception = new IllegalStateException("unit_test");
    doThrow(exception).when(command).execute();
    executorTask.execute();
    verify(executor).submit(runnableCaptor.capture());
    runnableCaptor.getValue().run();

    String description = executorTask.getErrorDescription(Locale.getDefault());

    assertEquals("test_error_description", description);
    verify(command).execute();
    assertEquals(exception, executorTask.getState().getException());
    verify(command).getErrorDescription(any(Throwable.class), eq(Locale.getDefault()));
    verify(command).getErrorDescription(eq(exception), eq(Locale.getDefault()));
  }
}
